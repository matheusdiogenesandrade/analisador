/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Map;

import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.github.shyiko.dotenv.DotEnv;

/**
 *
 * @author matheus
 */
public class Constants {

	private Constants() {
		//
	}

	static {
		Map<String, String> dotEnv = DotEnv.load();
		VALID_ROUTES_NUMBER_BY_SERVED_CUSTOMER = 1;
		MINIMUM_VEHICLE_AVERAGE_SPEED = 20d;
		MAXIMUM_VEHICLE_AVERAGE_SPEED = 80d;
		ERROR_SHEET_COLOR = IndexedColors.RED.getIndex();
		WARNING_SHEET_COLOR = IndexedColors.YELLOW.getIndex();
		WARNING_LUNCH_SHEET_COLOR = IndexedColors.GREEN.getIndex();
		ERROR_SHEET_FILL_PATTERN_TYPE = FillPatternType.THIN_BACKWARD_DIAG;
		BASE_DIR_RESOURCES = "/";
		MAX_COLUMNS_SHEET_SIZE = 15;
		SHEET_EXTENSION = ".xlsx";
		SHEET_NAME = "RESULT";
		INSTANCE_EXTENSION = ".json";
		URL_TXT_HISTORY_DIR = Constants.BASE_DIR_RESOURCES + "url_txt_history" + Constants.INSTANCE_EXTENSION;
		URL_TXT_HISTORY_NUMBER = 10;
		ORIGIN_DIR_HISTORY_DIR = Constants.BASE_DIR_RESOURCES + "origin_dir_history" + Constants.INSTANCE_EXTENSION;
		ORIGIN_DIR_HISTORY_NUMBER = 5;
		DESTINY_DIR_HISTORY_DIR = Constants.BASE_DIR_RESOURCES + "destiny_dir_history" + Constants.INSTANCE_EXTENSION;
		DESTINY_DIR_HISTORY_NUMBER = 5;
		INSTANCE_CONF_FILE_EXTENSION = "-conf" + Constants.INSTANCE_EXTENSION;
		RESULT_EXTENSION = "-result" + Constants.SHEET_EXTENSION;
		INSTANCE_PARAMS_NAME = new String[] { "prmMap" };
		MAP_IMAGE_FORMAT = "jpg";
		MAP_IMAGE_EXTENSION = "." + Constants.MAP_IMAGE_FORMAT;
		MAP_IMAGE_SIZE = "640x640";
		MAP_DEPOT_MARKER_COLOR = "blue";
		GOOGLE_STATIC_MAPS_API_KEY = dotEnv.get("GOOGLE_STATIC_MAPS_API_KEY");
		PLANNER_DEPOT_ID = 1;
		MONTH_DAYS_NUM = 28;
	}

	public static final Integer VALID_ROUTES_NUMBER_BY_SERVED_CUSTOMER;
	public static final Integer PLANNER_DEPOT_ID;
	public static final Double MINIMUM_VEHICLE_AVERAGE_SPEED;
	public static final Double MAXIMUM_VEHICLE_AVERAGE_SPEED;
	public static final Short ERROR_SHEET_COLOR;
	public static final FillPatternType ERROR_SHEET_FILL_PATTERN_TYPE;
	public static final String BASE_DIR_RESOURCES;
	public static final Integer MAX_COLUMNS_SHEET_SIZE;
	public static final String SHEET_EXTENSION;
	public static final String URL_TXT_HISTORY_DIR;
	public static final Integer URL_TXT_HISTORY_NUMBER;
	public static final String ORIGIN_DIR_HISTORY_DIR;
	public static final Integer ORIGIN_DIR_HISTORY_NUMBER;
	public static final String DESTINY_DIR_HISTORY_DIR;
	public static final Integer DESTINY_DIR_HISTORY_NUMBER;
	public static final String INSTANCE_EXTENSION;
	public static final String INSTANCE_CONF_FILE_EXTENSION;
	public static final String RESULT_EXTENSION;
	public static final String SHEET_NAME;
	public static final String[] INSTANCE_PARAMS_NAME;
	public static final Short WARNING_SHEET_COLOR;
	public static final Short WARNING_LUNCH_SHEET_COLOR;
	public static final String GOOGLE_STATIC_MAPS_API_KEY;
	public static final String MAP_IMAGE_FORMAT;
	public static final String MAP_IMAGE_EXTENSION;
	public static final String MAP_IMAGE_SIZE;
	public static final String MAP_DEPOT_MARKER_COLOR;
	public static final Integer MONTH_DAYS_NUM;
}
