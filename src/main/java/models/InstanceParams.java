/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author matheus
 */
public class InstanceParams {
    private Integer prmMap;
    private Instance instance;
 
    /**
     * @return the prmMap
     */
    public Integer getPrmMap() {
        return prmMap;
    }

    /**
     * @param prmMap the prmMap to set
     */
    public void setPrmMap(Integer prmMap) {
        this.prmMap = prmMap;
    }

    /**
     * @return the instance
     */
    public Instance getInstance() {
        return instance;
    }

    /**
     * @param instance the instance to set
     */
    public void setInstance(Instance instance) {
        this.instance = instance;
    }
    
}
