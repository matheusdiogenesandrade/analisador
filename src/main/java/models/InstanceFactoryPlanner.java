/**
 * 
 */
package models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import models.Instance.VelocityPerDistance;

/**
 * @author projeto
 *
 */
public class InstanceFactoryPlanner extends InstanceFactory {

	public InstanceFactoryPlanner(JsonObject instanceJsonObject) {
		super(instanceJsonObject, InstanceType.PLANNER);
	}

	private InstanceFactoryPlanner(JsonObject instanceJsonObject, InstanceType instanceType) {
		super(instanceJsonObject, instanceType);
	}

	@Override
	public Instance createInstance() {
		Instance instance = super.createInstance();
		JsonArray parametros = super.getInstanceJsonObject().getAsJsonArray("parametros");
		Integer timeHorizon = null;
		Boolean almocoObrigatorio = false;
		Boolean inicioCD = true;
		Boolean returnCD = true;
		Boolean viradaDia = true;
		Boolean funcionaSabado = true;
		for (int i = 0; i < parametros.size(); i++) {
			JsonObject param = parametros.get(i).getAsJsonObject();
			if (param.getAsJsonPrimitive("chave").getAsString().equals("Horizonte de Planejamento")) {
				timeHorizon = param.getAsJsonPrimitive("valor").getAsInt();
			} else if (param.getAsJsonPrimitive("chave").getAsString().equals("Utiliza Tempo Para Refeição?")) {
				almocoObrigatorio = param.getAsJsonPrimitive("valor").getAsString().equals("SIM");
			} else if (param.getAsJsonPrimitive("chave").getAsString().equals("inicioCD")) {
				inicioCD = param.getAsJsonPrimitive("valor").getAsBoolean();
			} else if (param.getAsJsonPrimitive("chave").getAsString().equals("returnCD")) {
				returnCD = param.getAsJsonPrimitive("valor").getAsBoolean();
			} else if (param.getAsJsonPrimitive("chave").getAsString().equals("viradaDia")) {
				viradaDia = param.getAsJsonPrimitive("valor").getAsBoolean();
			} else if (param.getAsJsonPrimitive("chave").getAsString().equals("Funciona no sábado?")
					&& param.getAsJsonPrimitive("valor").getAsString().equals("NAO")) {
				funcionaSabado = false;
			}
		}
		instance.setTimeHorizon(timeHorizon);
		instance.setMandatoryLunch(almocoObrigatorio == null ? false : almocoObrigatorio);
		instance.setStartInDepot(inicioCD);
		instance.setEndsInDepot(returnCD);
		instance.setDayShift(viradaDia);
		instance.setWorksInSaturday(funcionaSabado);
		return instance;
	}

	@Override
	public List<Customer> createDepots() {
		JsonArray vendedores = super.getInstanceJsonObject().getAsJsonArray("vendedores");
		List<Customer> depots = new ArrayList<Customer>();
		for (int i = 0; i < vendedores.size(); i++) {
			JsonObject vendedor = vendedores.get(i).getAsJsonObject();
			double x = vendedor.getAsJsonPrimitive("longitude").getAsDouble();
			double y = vendedor.getAsJsonPrimitive("lat").getAsDouble();
			boolean newDepot = true;
			for (Customer depot : depots) {
				if (depot.getX() == x && depot.getY() == y) {
					newDepot = false;
					break;
				}
			}
			if (newDepot) {
				Customer newDepotObj = new Customer();
				newDepotObj.setCoordinates(new Coordinates(x, y));
				newDepotObj.setId(Constants.PLANNER_DEPOT_ID);
				newDepotObj.setExpedient(
						new Expedient(getTimeFromString(vendedor.getAsJsonPrimitive("expedienteIni").getAsString()),
								getTimeFromString(vendedor.getAsJsonPrimitive("expedienteFim").getAsString())));
				newDepotObj.setCubing(0d);
				newDepotObj.setWeight(0d);
				newDepotObj
						.setServiceTime(newDepotObj.getExpedient().getFinish() - newDepotObj.getExpedient().getStart());
				depots.add(newDepotObj);
			}
		}
		return depots;
	}

	@Override
	public List<Customer> createCustomers() {
		JsonArray clientes = super.getInstanceJsonObject().getAsJsonArray("clientes");
		List<Customer> customers = new ArrayList<Customer>(clientes.size());
		for (int i = 0; i < clientes.size(); i++) {
			JsonObject cliente = clientes.get(i).getAsJsonObject();
			Customer customer = new Customer();
			customer.setId(Integer.valueOf(cliente.getAsJsonPrimitive("codigoCliente").getAsString()));
			customer.setCoordinates(new Coordinates(cliente.getAsJsonPrimitive("longitude").getAsDouble(),
					cliente.getAsJsonPrimitive("lat").getAsDouble()));
			customer.setExpedient(new Expedient(getTimeFromString(cliente.getAsJsonPrimitive("horaCedo").getAsString()),
					getTimeFromString(cliente.getAsJsonPrimitive("horaTarde").getAsString())));
			customer.setCubing(0d);
			customer.setWeight(cliente.getAsJsonPrimitive("mediaPeso").getAsDouble());
			customer.setVisitsLimit(1);
			customer.setServiceTime(cliente.getAsJsonPrimitive("mediaTempoVisita").getAsDouble());
			// dias obrigatorios
			List<WeekDay> weekDays = new ArrayList<WeekDay>();
			char[] diasObrigatorios = cliente.getAsJsonPrimitive("diasObrigatorios").getAsString().toCharArray();
			if (diasObrigatorios[0] == 'S') {
				weekDays.add(WeekDay.MONDAY);
			}
			if (diasObrigatorios[1] == 'S') {
				weekDays.add(WeekDay.TUESDAY);
			}
			if (diasObrigatorios[2] == 'S') {
				weekDays.add(WeekDay.WEDNESDAY);
			}
			if (diasObrigatorios[3] == 'S') {
				weekDays.add(WeekDay.THURSDAY);
			}
			if (diasObrigatorios[4] == 'S') {
				weekDays.add(WeekDay.FRIDAY);
			}
			if (diasObrigatorios[5] == 'S') {
				weekDays.add(WeekDay.SATURDAY);
			}
			if (diasObrigatorios[6] == 'S') {
				weekDays.add(WeekDay.SUNDAY);
			}
			customer.setRequiredWeekDays(weekDays.toArray(new WeekDay[weekDays.size()]));
			// semanas possiveis
			char[] semanasPossiveis = cliente.getAsJsonPrimitive("semanas").getAsString().toCharArray();
			Boolean[] possibleWeeks = new Boolean[semanasPossiveis.length];
			for (int j = 0; j < semanasPossiveis.length; j++) {
				possibleWeeks[j] = semanasPossiveis[j] == 'X';
			}
			customer.setPossibleVisitWeeks(possibleWeeks);
			// frequencia de visita
			Integer frequenciaVisita = cliente.getAsJsonPrimitive("frequenciaDeVisita").getAsInt();
			if (frequenciaVisita == 100) {
				customer.setVisitFrequency(VisitFrequency.MONTHLY);
			} else if (frequenciaVisita == 200) {
				customer.setVisitFrequency(VisitFrequency.BIWEEKLY);
			} else if (frequenciaVisita == 300) {
				customer.setVisitFrequency(VisitFrequency.WEEKLY);
			} else if (frequenciaVisita == 400) {
				customer.setVisitFrequency(VisitFrequency.TWO_TIMES_PER_WEEK);
			} else if (frequenciaVisita == 500) {
				customer.setVisitFrequency(VisitFrequency.DAILY);
			}
			customers.add(customer);
		}
		return customers;
	}

	@Override
	public List<VelocityPerDistance> createVelocitiesPerDistance() {
		List<VelocityPerDistance> velocitiesPerDistance = new ArrayList<VelocityPerDistance>();
		JsonArray params = super.getInstanceJsonObject().getAsJsonArray("parametros");
		for (int i = 0; i < params.size(); i++) {
			JsonObject param = params.get(i).getAsJsonObject();
			String key = param.getAsJsonPrimitive("chave").getAsString();
			String value = (param.getAsJsonPrimitive("valor").getAsString());
			if (key.equals("Velocidade Média E (10000ms a 999999ms)")) {
				velocitiesPerDistance.add(new VelocityPerDistance(Double.valueOf(value), 10000d));
			} else if (key.equals("Velocidade Média C (1000ms a 3000ms)")) {
				velocitiesPerDistance.add(new VelocityPerDistance(Double.valueOf(value), 1000d));
			} else if (key.equals("Velocidade Média A (0ms a 300ms)")) {
				velocitiesPerDistance.add(new VelocityPerDistance(Double.valueOf(value), 0d));
			} else if (key.equals("Velocidade Média B (300ms a 1000ms)")) {
				velocitiesPerDistance.add(new VelocityPerDistance(Double.valueOf(value), 300d));
			} else if (key.equals("Velocidade Média D (3000ms a 10000ms)")) {
				velocitiesPerDistance.add(new VelocityPerDistance(Double.valueOf(value), 3000d));
			}
		}
		return velocitiesPerDistance;
	}

	@Override
	public List<Vehicle> createVehicles() {
		JsonArray vendedores = super.getInstanceJsonObject().getAsJsonArray("vendedores");
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		for (int i = 0; i < vendedores.size(); i++) {
			JsonObject vendedor = vendedores.get(i).getAsJsonObject();
			Vehicle vehicle = new Vehicle();
			vehicle.setBoard(vendedor.getAsJsonPrimitive("codigo").getAsString());
			VehicleRestrictions vehicleRestrictions = vehicle.getVehicleRestrictions();
			vehicleRestrictions.setExpedient(
					new Expedient(getTimeFromString(vendedor.getAsJsonPrimitive("expedienteIni").getAsString()),
							getTimeFromString(vendedor.getAsJsonPrimitive("expedienteFim").getAsString())));
			vehicleRestrictions.setLunchExpedient(
					new Expedient(getTimeFromString(vendedor.getAsJsonPrimitive("Inicio Almoco").getAsString()),
							getTimeFromString(vendedor.getAsJsonPrimitive("Fim Almoco").getAsString())));
			vehicleRestrictions.setLunchDelay(getTimeFromString(vendedor.getAsJsonPrimitive("Atrasar Almoco").getAsString()));
			vehicleRestrictions.getLunchExpedient().setDelayLunch(getTimeFromString(vendedor.getAsJsonPrimitive("Atrasar Almoco").getAsString()));
			vehicleRestrictions.setLunchAntecipate(getTimeFromString(vendedor.getAsJsonPrimitive("Antecipar Almoco").getAsString()));
			vehicleRestrictions.getLunchExpedient().setAntecipateLunch(getTimeFromString(vendedor.getAsJsonPrimitive("Antecipar Almoco").getAsString()));
			vehicle.setWeightLimit(vendedor.getAsJsonPrimitive("Peso Limite").getAsDouble());
			vehicle.setCubingLimit(0d);
			vehicle.setVisitsLimit(vendedor.getAsJsonPrimitive("numVisitas").getAsInt());
			vehicle.setServiceTimeLimit(
					(vehicleRestrictions.getExpedient().getFinish() - vehicleRestrictions.getExpedient().getStart())
							- (vehicleRestrictions.getLunchExpedient().getFinish()
									- vehicleRestrictions.getExpedient().getStart()));
			vehicles.add(vehicle);
		}
		return vehicles;
	}

	private Double getTimeFromString(String string) {
		String[] parts = string.split(":");
		return Double.valueOf(parts[0]) + (Double.valueOf(parts[1]) / 60);
	}
}
