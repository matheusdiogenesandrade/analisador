/**
 *
 */
package models;

import java.util.ArrayList;
import java.util.List;

/**
 * @author matheus
 *
 */
public class Route {

    private Integer id;
    private Vehicle vehicle;
    private List<Customer> customers;
    private Customer depot;
    private Integer day;

    public Route() {
        this.setCustomers(new ArrayList<Customer>());
    }

    /**
     * @return the vehicle
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * @param vehicle the vehicle to set
     */
    public void setVehicle(Vehicle vehicle) {
        if (vehicle != null) {
            this.vehicle = vehicle;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro vehicle não pode ser nulo.");
        }
    }

    /**
     * @return the customers
     */
    public List<Customer> getCustomers() {
        return customers;
    }

    /**
     * @param customers the customers to set
     */
    public void setCustomers(List<Customer> customers) {
        if (customers != null) {
            this.customers = customers;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro customers não pode ser nulo.");
        }        
    }

    /**
     * @return the depot
     */
    public Customer getDepot() {
        return depot;
    }

    /**
     * @param depot the depot to set
     */
    public void setDepot(Customer depot) {
        if (depot != null) {
            this.depot = depot;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro depot não pode ser nulo.");
        }
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        if (id != null) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro id não pode ser nulo.");
        }        
    }

	/**
	 * @return the day
	 */
	public Integer getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(Integer day) {		
		if (day != null && day >= 0) {
			this.day = day;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro day não pode ser nulo e deve ser natural.");
        }    
	}
	
}
