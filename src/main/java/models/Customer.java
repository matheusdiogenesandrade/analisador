/**
 *
 */
package models;

/**
 * @author matheus
 *
 */
public class Customer {

    private Integer id;
    private Coordinates coordinates;
    private Double cubing;
    private Double weight;
    private Integer visitsLimit;
    private Double serviceTime;
    private Expedient expedient;
    private CustomerRouteDatas customerRouteDatas;    
    private WeekDay[] requiredWeekDays;
    private Boolean[] possibleVisitWeeks;
    private VisitFrequency visitFrequency;

    public Customer() {
        this.setCoordinates(new Coordinates());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Customer customer = new Customer();
        customer.setId(this.getId());
        customer.setCoordinates((Coordinates) this.getCoordinates().clone());
        customer.setExpedient((Expedient) this.getExpedient().clone());
        if (this.getRequiredWeekDays() != null) {
        	customer.setRequiredWeekDays(this.getRequiredWeekDays());
        }
        if (this.getPossibleVisitWeeks() != null) {
        	customer.setPossibleVisitWeeks(this.getPossibleVisitWeeks());
        }
        if (this.getVisitFrequency() != null) {
        	customer.setVisitFrequency(this.getVisitFrequency());
        }
        if (this.getServiceTime() != null) {
        	customer.setServiceTime(this.getServiceTime());
        }        
        return customer;
    }

    /* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Customer other = (Customer) obj;
        if (this.getCubing() == null) {
            if (other.getCubing() != null) {
                return false;
            }
        } else if (!this.getCubing().equals(other.getCubing())) {
            return false;
        }
        if (this.getExpedient() == null) {
            if (other.getExpedient() != null) {
                return false;
            }
        } else if (!this.getExpedient().equals(other.getExpedient())) {
            return false;
        }
        if (this.getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!this.getId().equals(other.getId())) {
            return false;
        }
        if (this.getServiceTime() == null) {
            if (other.getServiceTime() != null) {
                return false;
            }
        } else if (!this.getServiceTime().equals(other.getServiceTime())) {
            return false;
        }
        if (this.getVisitsLimit() == null) {
            if (other.getVisitsLimit() != null) {
                return false;
            }
        } else if (!this.getVisitsLimit().equals(other.getVisitsLimit())) {
            return false;
        }
        if (this.getWeight() == null) {
            if (other.getWeight() != null) {
                return false;
            }
        } else if (!this.getWeight().equals(other.getWeight())) {
            return false;
        }
        if (this.getX() == null) {
            if (other.getX() != null) {
                return false;
            }
        } else if (!this.getX().equals(other.getX())) {
            return false;
        }
        if (this.getY() == null) {
            if (other.getY() != null) {
                return false;
            }
        } else if (!this.getY().equals(other.getY())) {
            return false;
        }
        return true;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        if (id != null) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro id deve ser diferente de nulo.");
        }
    }

    /**
     * @return the x
     */
    public Double getX() {
        return this.getCoordinates().getX();
    }

    /**
     * @param x the x to set
     */
    public void setX(Double x) {
        this.getCoordinates().setX(x);
    }

    /**
     * @return the y
     */
    public Double getY() {
        return this.getCoordinates().getY();
    }

    /**
     * @param y the y to set
     */
    public void setY(Double y) {
        this.getCoordinates().setY(y);
    }

    /**
     * @return the cubing
     */
    public Double getCubing() {
        return cubing;
    }

    /**
     * @param cubing the cubing to set
     */
    public void setCubing(Double cubing) {
        if (cubing != null && cubing >= 0) {
            this.cubing = cubing;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro cubing deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

    /**
     * @return the weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Double weight) {
        if (weight != null && weight >= 0) {
            this.weight = weight;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro weight deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

    /**
     * @return the visitsLimit
     */
    public Integer getVisitsLimit() {
        return visitsLimit;
    }

    /**
     * @param visitsLimit the visitsLimit to set
     */
    public void setVisitsLimit(Integer visitsLimit) {
        if (visitsLimit != null && visitsLimit >= 0) {
            this.visitsLimit = visitsLimit;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro visitsLimit deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

    /**
     * @return the serviceTime
     */
    public Double getServiceTime() {
        return serviceTime;
    }

    /**
     * @param serviceTime the serviceTime to set
     */
    public void setServiceTime(Double serviceTime) {
        if (serviceTime != null && serviceTime >= 0) {
            this.serviceTime = serviceTime;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro serviceTime deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

    /**
     * @return the expedient
     */
    public Expedient getExpedient() {
        return expedient;
    }

    /**
     * @param expedient the expedient to set
     */
    public void setExpedient(Expedient expedient) {
        if (expedient != null) {
            this.expedient = expedient;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro expedient não pode ser nulo");
        }
    }

    /**
     * @return the coordinates
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
     * @param coordinates the coordinates to set
     */
    public void setCoordinates(Coordinates coordinates) {
        if (coordinates != null) {
            this.coordinates = coordinates;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro coordinates não pode ser nulo");
        }
    }

    /**
     * @return the customerRouteDatas
     */
    public CustomerRouteDatas getCustomerRouteDatas() {
        return customerRouteDatas;
    }

    /**
     * @param customerRouteDatas the routeDatas to set
     */
    public void setCustomerRouteDatas(CustomerRouteDatas customerRouteDatas) {
        if (customerRouteDatas != null) {
            this.customerRouteDatas = customerRouteDatas;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro customerRouteDatas não pode ser nulo");
        }
    }

	/**
	 * @return the requiredWeekDays
	 */
	public WeekDay[] getRequiredWeekDays() {
		return requiredWeekDays;
	}

	/**
	 * @param requiredWeekDays the requiredWeekDays to set
	 */
	public void setRequiredWeekDays(WeekDay[] requiredWeekDays) {
		if (requiredWeekDays != null) {
			this.requiredWeekDays = requiredWeekDays;
		}else {
			throw new IllegalArgumentException("Erro: O parametro requiredWeekDays não pode ser nulo");
		}
	}

	/**
	 * @return the possibleVisitWeeks
	 */
	public Boolean[] getPossibleVisitWeeks() {
		return possibleVisitWeeks;
	}

	/**
	 * @param possibleVisitWeeks the possibleVisitWeeks to set
	 */
	public void setPossibleVisitWeeks(Boolean[] possibleVisitWeeks) {
		if (possibleVisitWeeks != null) {
			this.possibleVisitWeeks = possibleVisitWeeks;
		}else {
			throw new IllegalArgumentException("Erro: O parametro possibleVisitWeeks não pode ser nulo");
		}		
	}

	/**
	 * @return the visitFrequency
	 */
	public VisitFrequency getVisitFrequency() {
		return visitFrequency;
	}

	/**
	 * @param visitFrequency the visitFrequency to set
	 */
	public void setVisitFrequency(VisitFrequency visitFrequency) {		
		if (visitFrequency != null) {
			this.visitFrequency = visitFrequency;
		}else {
			throw new IllegalArgumentException("Erro: O parametro visitFrequency não pode ser nulo");
		}	
	}
    
}
