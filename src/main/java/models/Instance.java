/**
 *
 */
package models;

import java.util.List;

/**
 * @author matheus
 *
 */
public class Instance {

    public static class VelocityPerDistance {

        private Double velocity;
        private Double distance;
        
        public VelocityPerDistance(Double velocity, Double distance){
            this.setDistance(distance);
            this.setVelocity(velocity);
        }
        
        /**
         * @return the velocity
         */
        public Double getVelocity() {
            return velocity;
        }

        /**
         * @param velocity the velocity to set
         */
        public void setVelocity(Double velocity) {
            if (velocity != null && velocity >= 0) {
                this.velocity = velocity;
            } else {
                throw new IllegalArgumentException("Erro: O paramêtro velocity deve ser diferente de nulo e maior ou igual a zero.");
            }                        
        }

        /**
         * @return the distance
         */
        public Double getDistance() {
            return distance;
        }

        /**
         * @param distance the distance to set
         */
        public void setDistance(Double distance) {
            if (distance != null && distance >= 0) {
                this.distance = distance;
            } else {
                throw new IllegalArgumentException("Erro: O paramêtro distance deve ser diferente de nulo e maior ou igual a zero.");
            }            
        }

    }

    private List<Customer> depots;
    private List<Customer> customers;
    private Double[][] distanceMatrix;
    private Double[][] timeMatrix;
    private List<VelocityPerDistance> velocitiesPerDistance;
    private List<Vehicle> vehicles;    
    private InstanceType instanceType;
    private Integer timeHorizon;
    private Boolean mandatoryLunch;
    private Boolean startInDepot;
    private Boolean endsInDepot;
    private Boolean dayShift;
    private Boolean worksInSaturday;
    
    private void checkCostMatrix(Double[][] matrix){
        if (matrix != null && matrix.length > 1d && matrix[0].length > 1d) {                                                
            for (int i = 0; i < matrix.length; i++) {
                //square format
                if (matrix[i].length != matrix.length){
                    throw new IllegalArgumentException("Erro: A matriz deve ser quadrada.");
                }
                for (Double item : matrix[i]) {
                    //null values
                    if (item == null) {
                        throw new IllegalArgumentException("Erro: A matriz não deve ter valores nulo.");
                    }                                        
                }
                //diagonal values                    
                if (matrix[i][i] != 0d){
                    throw new IllegalArgumentException("Erro: A matriz deve ter 0 para pontos iguais (matrix[i][i] == 0).");
                }
            }
        } else {
            throw new IllegalArgumentException("Erro: A matriz deve ser diferente de nulo e deve ter pelo menos pelo menos duas linhas e duas colunas.");
        }             
    }
    
    /**
     * @return the depot
     */
    public List<Customer> getDepots() {
        return depots;
    }

    /**
     * @param depots the depot to set
     */
    public void setDepots(List<Customer> depots) {
        if (depots != null && depots.size() > 0) {
            this.depots = depots;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro depots deve ser diferente de nulo e maior que 0.");
        }
    }

    /**
     * @return the customers
     */
    public List<Customer> getCustomers() {
        return customers;
    }

    /**
     * @param customers the customers to set
     */
    public void setCustomers(List<Customer> customers) {
        if (customers != null && customers.size() > 0) {
            this.customers = customers;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro customers deve ser diferente de nulo e deve ter pelo menos um customer na lista.");
        }
    }

    /**
     * @return the vehicles
     */
    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    /**
     * @param vehicles the vehicles to set
     */
    public void setVehicles(List<Vehicle> vehicles) {
        if (vehicles != null && vehicles.size() > 0) {
            this.vehicles = vehicles;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro vehicles deve ser diferente de nulo e deve ter pelo menos um vehicle na lista.");
        }
    }

    /**
     * @return the distanceMatrix
     */
    public Double[][] getDistanceMatrix() {
        return distanceMatrix;
    }

    /**
     * @param distanceMatrix the distanceMatrix to set
     */
    public void setDistanceMatrix(Double[][] distanceMatrix) {
        this.checkCostMatrix(distanceMatrix);
        this.distanceMatrix = distanceMatrix;        
    }

    /**
     * @return the timeMatrix
     */
    public Double[][] getTimeMatrix() {
        return timeMatrix;
    }

    /**
     * @param timeMatrix the timeMatrix to set
     */
    public void setTimeMatrix(Double[][] timeMatrix) {
        this.checkCostMatrix(timeMatrix);
        this.timeMatrix = timeMatrix;
    }

    /**
     * @return the velocitiesPerDistance
     */
    public List<VelocityPerDistance> getVelocitiesPerDistance() {
        return velocitiesPerDistance;
    }

    /**
     * @param velocitiesPerDistance the velocitiesPerDistance to set
     */
    public void setVelocitiesPerDistance(List<VelocityPerDistance> velocitiesPerDistance) {
        if (velocitiesPerDistance != null && velocitiesPerDistance.size() > 0) {
            for (VelocityPerDistance velocityPerDistance : velocitiesPerDistance) {
                if (velocityPerDistance == null){
                    throw new IllegalArgumentException("Erro: O paramêtro velocitiesPerDistance não deve ter elementos nulo.");
                }
            }
            this.velocitiesPerDistance = velocitiesPerDistance;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro velocitiesPerDistance deve ser diferente de nulo e deve ter pelo menos um velocityPerDistance na lista.");
        }        
    }

	/**
	 * @return the instanceType
	 */
	public InstanceType getInstanceType() {
		return instanceType;
	}

	/**
	 * @param instanceType the instanceType to set
	 */
	public void setInstanceType(InstanceType instanceType) {		
		if (instanceType != null) {
			this.instanceType = instanceType;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro instanceType deve ser diferente de nulo.");
        }
	}

	/**
	 * @return the timeHorizon
	 */
	public Integer getTimeHorizon() {
		return timeHorizon;
	}

	/**
	 * @param timeHorizon the timeHorizon to set
	 */
	public void setTimeHorizon(Integer timeHorizon) {		
		if (timeHorizon != null && timeHorizon > 0) {
			this.timeHorizon = timeHorizon;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro timeHorizon deve ser diferente de nulo e maior que 0.");
        }
	}

	/**
	 * @return the mandatoryLunch
	 */
	public Boolean getMandatoryLunch() {
		return mandatoryLunch;
	}

	/**
	 * @param mandatoryLunch the mandatoryLunch to set
	 */
	public void setMandatoryLunch(Boolean mandatoryLunch) {
		if (mandatoryLunch != null) {
			this.mandatoryLunch = mandatoryLunch;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro mandatoryLunch deve ser diferente de nulo.");
        }		
	}

	/**
	 * @return the startInDepot
	 */
	public Boolean getStartInDepot() {
		return startInDepot;
	}

	/**
	 * @param startInDepot the startInDepot to set
	 */
	public void setStartInDepot(Boolean startInDepot) {		
		if (startInDepot != null) {
			this.startInDepot = startInDepot;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro startInDepot deve ser diferente de nulo.");
        }	
	}

	/**
	 * @return the endsInDepot
	 */
	public Boolean getEndsInDepot() {
		return endsInDepot;
	}

	/**
	 * @param endsInDepot the endsInDepot to set
	 */
	public void setEndsInDepot(Boolean endsInDepot) {		
		if (endsInDepot != null) {
			this.endsInDepot = endsInDepot;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro endsInDepot deve ser diferente de nulo.");
        }	
	}

	/**
	 * @return the dayShift
	 */
	public Boolean getDayShift() {
		return dayShift;
	}

	/**
	 * @param dayShift the dayShift to set
	 */
	public void setDayShift(Boolean dayShift) {		
		if (dayShift != null) {
			this.dayShift = dayShift;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro dayShift deve ser diferente de nulo.");
        }	
	}

	/**
	 * @return the worksInSaturday
	 */
	public Boolean getWorksInSaturday() {
		return worksInSaturday;
	}

	/**
	 * @param worksInSaturday the worksInSaturday to set
	 */
	public void setWorksInSaturday(Boolean worksInSaturday) {		
		if (worksInSaturday != null) {
			this.worksInSaturday = worksInSaturday;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro worksInSaturday deve ser diferente de nulo.");
        }	
	}
	
}
