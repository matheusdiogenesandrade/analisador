/**
 *
 */
package models;

/**
 * @author matheus
 *
 */
public class Expedient {

    private Double start;
    private Double antecipateLunch;
    private Double lunchStart;
    private Double lunchFinish;
    private Double delayLunch;
    private Double finish;
    private Double startRest;
    private Double finishRest;

    public Expedient(Double start, Double finish) {
        this.setStart(start);
        this.setFinish(finish);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Expedient(this.getStart(), this.getFinish());
    }
    
    /* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Expedient other = (Expedient) obj;
        if (finish == null) {
            if (other.finish != null) {
                return false;
            }
        } else if (!finish.equals(other.finish)) {
            return false;
        }
        if (start == null) {
            if (other.start != null) {
                return false;
            }
        } else if (!start.equals(other.start)) {
            return false;
        }
        return true;
    }

    /**
     * @return the start
     */
    public Double getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(Double start) {
        if (start != null && start >= 0) {
            this.start = start;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro start deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

    /**
     * @return the finish
     */
    public Double getFinish() {
        return finish;
    }

    /**
     * @param finish the finish to set
     */
    public void setFinish(Double finish) {
        if (finish != null && finish >= 0) {
            this.finish = finish;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro finish deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

	/**
	 * @return the antecipateLunch
	 */
	public Double getAntecipateLunch() {
		return antecipateLunch;
	}

	/**
	 * @param antecipateLunch the antecipateLunch to set
	 */
	public void setAntecipateLunch(Double antecipateLunch) {
		if (antecipateLunch != null && antecipateLunch >= 0) {
			this.antecipateLunch = antecipateLunch;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro antecipateLunch deve ser diferente de nulo e maior ou igual a zero.");
        }		
	}

	/**
	 * @return the lunchStart
	 */
	public Double getLunchStart() {
		return lunchStart;
	}

	/**
	 * @param lunchStart the lunchStart to set
	 */
	public void setLunchStart(Double lunchStart) {		
		if (lunchStart != null && lunchStart >= 0) {
			this.lunchStart = lunchStart;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro lunchStart deve ser diferente de nulo e maior ou igual a zero.");
        }	
	}

	/**
	 * @return the lunchFinish
	 */
	public Double getLunchFinish() {
		return lunchFinish;
	}

	/**
	 * @param lunchFinish the lunchFinish to set
	 */
	public void setLunchFinish(Double lunchFinish) {		
		if (lunchFinish != null && lunchFinish >= 0) {
			this.lunchFinish = lunchFinish;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro lunchFinish deve ser diferente de nulo e maior ou igual a zero.");
        }	
	}

	/**
	 * @return the delayLunch
	 */
	public Double getDelayLunch() {
		return delayLunch;
	}

	/**
	 * @param delayLunch the delayLunch to set
	 */
	public void setDelayLunch(Double delayLunch) {		
		if (delayLunch != null && delayLunch >= 0) {
			this.delayLunch = delayLunch;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro delayLunch deve ser diferente de nulo e maior ou igual a zero.");
        }	
	}

	/**
	 * @return the startRest
	 */
	public Double getStartRest() {
		return startRest;
	}

	/**
	 * @param startRest the startRest to set
	 */
	public void setStartRest(Double startRest) {		
		if (startRest != null && startRest >= 0) {
			this.startRest = startRest;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro startRest deve ser diferente de nulo e maior ou igual a zero.");
        }	
	}

	/**
	 * @return the finishRest
	 */
	public Double getFinishRest() {
		return finishRest;
	}

	/**
	 * @param finishRest the finishRest to set
	 */
	public void setFinishRest(Double finishRest) {		
		if (finishRest != null && finishRest >= 0) {
			this.finishRest = finishRest;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro finishRest deve ser diferente de nulo e maior ou igual a zero.");
        }	
	}
    
}
