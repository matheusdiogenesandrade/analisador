/**
 * 
 */
package models;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * @author projeto
 *
 */
public abstract class OutputFactory {
	private JsonObject outputJsonObject;
	private Instance instance;
	
	/**
	 * @param outputJsonObject
	 */
	public OutputFactory(JsonObject outputJsonObject, Instance instance) {
		super();
		this.setOutputJsonObject(outputJsonObject);
		this.setInstance(instance);
	}

	protected abstract List<Route> createRoutes();
	protected abstract List<Customer> createNotServed();
	protected abstract Double createAlgorithmTime();	
	
	protected List<Route> getRoutesFromJsonObject(JsonObject jsonObject, Instance instance) {
		List<Vehicle> instanceVehicles = instance.getVehicles();
		List<Customer> instanceDepots = instance.getDepots();
		List<Customer> instanceCustomers = instance.getCustomers();
		// getting routes
		JsonArray grupos = jsonObject.getAsJsonArray("grupos");
		List<Route> routes = new ArrayList<Route>(grupos.size());
		for (int i = 0; i < grupos.size(); i++) {
			JsonObject grupo = grupos.get(i).getAsJsonObject();
			Route route = new Route();
			route.setId(i);
			// getting vehicle
			JsonObject veiculo = grupo.getAsJsonObject("veiculo");
			// search vehicle in instance
			String board = veiculo.getAsJsonPrimitive("id").getAsString();
			Boolean find = false;
			for (Vehicle instanceVehicle : instanceVehicles) {
				if (board.equals(instanceVehicle.getBoard())) {
					JsonArray totais = grupo.getAsJsonArray("totais");
					// vehicle already used
					VehicleRouteDatas vehicleRouteDatas;
					if (instanceVehicle.getVehicleRouteDatas() != null) {
						vehicleRouteDatas = instanceVehicle.getVehicleRouteDatas();
						vehicleRouteDatas
								.setWeightFilled(vehicleRouteDatas.getWeightFilled() + totais.get(0).getAsDouble());
						vehicleRouteDatas.setServiceTimeFilled(
								vehicleRouteDatas.getServiceTimeFilled() + totais.get(1).getAsDouble());
						vehicleRouteDatas.setVisitsMade(vehicleRouteDatas.getVisitsMade() + totais.get(2).getAsInt());
						vehicleRouteDatas
								.setCubingFilled(vehicleRouteDatas.getCubingFilled() + totais.get(3).getAsDouble());
					} else {
						vehicleRouteDatas = new VehicleRouteDatas();
						vehicleRouteDatas.setWeightFilled(totais.get(0).getAsDouble());
						vehicleRouteDatas.setServiceTimeFilled(totais.get(1).getAsDouble());
						vehicleRouteDatas.setVisitsMade(totais.get(2).getAsInt());
						vehicleRouteDatas.setCubingFilled(totais.get(3).getAsDouble());
					}
					vehicleRouteDatas.getServedRoutes().add(route);
					instanceVehicle.setVehicleRouteDatas(vehicleRouteDatas);
					route.setVehicle(instanceVehicle);
					find = true;
					break;
				}
			}
			// if vehicle in route does not exists in instance
			if (!find) {
				JsonArray totais = grupo.getAsJsonArray("totais");
				JsonArray limites = veiculo.getAsJsonArray("limites");
				// getting vehicle
				Vehicle vehicle = new Vehicle();
				vehicle.setBoard(board);
				vehicle.setWeightLimit(limites.get(0).getAsDouble());
				vehicle.setServiceTimeLimit(limites.get(1).getAsDouble());
				vehicle.setVisitsLimit(limites.get(2).getAsInt());
				vehicle.setCubingLimit(limites.get(3).getAsDouble());
				// getting vehicle route datas
				VehicleRouteDatas vehicleRouteDatas = new VehicleRouteDatas();
				vehicleRouteDatas.setWeightFilled(totais.get(0).getAsDouble());
				vehicleRouteDatas.setServiceTimeFilled(totais.get(1).getAsDouble());
				vehicleRouteDatas.setVisitsMade(totais.get(2).getAsInt());
				vehicleRouteDatas.setCubingFilled(totais.get(3).getAsDouble());

				vehicle.setVehicleRouteDatas(vehicleRouteDatas);
				route.setVehicle(vehicle);
			}
			// getting customers
			JsonArray clientes = grupo.getAsJsonArray("clientes");
			// getting depot
			JsonObject cd = clientes.get(0).getAsJsonObject();
			Integer depotId = cd.getAsJsonPrimitive("id").getAsInt();			
			Customer depot = null;
			// searching depot in instance
			try {
				Customer findedDepot = null;
				for (Customer instanceDepot : instanceDepots) {
					if (depotId.equals(instanceDepot.getId())) {
						findedDepot = instanceDepot;
						break;
					}
				}
				if (findedDepot != null) {
					depot = (Customer) findedDepot.clone();
				} else {
					for (Customer instanceCustomer : instanceCustomers) {
						if (depotId.equals(instanceCustomer.getId())) {
							depot = (Customer) instanceCustomer.clone();
						}
					}
				}
			} catch (CloneNotSupportedException ex) {
				Logger.getLogger(VerifyFacade.class.getName()).log(Level.SEVERE, null, ex);
			}
			JsonObject jtrCd = cd.getAsJsonObject("jtr");
			JsonObject trajetoCd = cd.getAsJsonObject("trajeto");
			JsonArray locaisCd = trajetoCd.getAsJsonArray("locais");
			// getting route datas from depot
			CustomerRouteDatas depotCustomerRouteDatas = new CustomerRouteDatas();
			depotCustomerRouteDatas.setCurrentMileage(cd.getAsJsonPrimitive("km").getAsDouble());
			depotCustomerRouteDatas.setServedExpedient(new Expedient(jtrCd.getAsJsonPrimitive("ini").getAsDouble(),
					jtrCd.getAsJsonPrimitive("fim").getAsDouble()));
			depotCustomerRouteDatas.setRoute(route);
			Path depotPathToNextCustomer = depotCustomerRouteDatas.getPathToNextCustomer();
			depotPathToNextCustomer.setExitId(trajetoCd.getAsJsonPrimitive("id0").getAsInt());
			depotPathToNextCustomer.setArrivalId(trajetoCd.getAsJsonPrimitive("id1").getAsInt());
			depotPathToNextCustomer.setExitCoordinates(new Coordinates(trajetoCd.getAsJsonPrimitive("x0").getAsDouble(),
					trajetoCd.getAsJsonPrimitive("y0").getAsDouble()));
			depotPathToNextCustomer
					.setArrivalCoordinates(new Coordinates(trajetoCd.getAsJsonPrimitive("x1").getAsDouble(),
							trajetoCd.getAsJsonPrimitive("y1").getAsDouble()));
			for (int j = 0; j < locaisCd.size(); j++) {
				JsonObject localCd = locaisCd.get(j).getAsJsonObject();
				depotPathToNextCustomer.getLocals().add(new Coordinates(localCd.getAsJsonPrimitive("x").getAsDouble(),
						localCd.getAsJsonPrimitive("y").getAsDouble()));
			}
			depot.setCustomerRouteDatas(depotCustomerRouteDatas);
			route.setDepot(depot);
			// getting route datas from anothers customers
			for (int j = 1; j < clientes.size(); j++) {
				JsonObject cliente = clientes.get(j).getAsJsonObject();
				Customer customer = null;
				Integer customerId = Integer.valueOf(cliente.getAsJsonPrimitive("id").getAsString());
				// searching customer in instance
				try {
//					Customer findedDepot = null;
//					for (Customer instanceDepot : instanceDepots) {
//						if (depotId.equals(instanceDepot.getId())) {
//							findedDepot = instanceDepot;
//							break;
//						}
//					}
//					if (findedDepot != null) {
//						depot = (Customer) findedDepot.clone();
//					} else {
						for (Customer instanceCustomer : instanceCustomers) {
							if (customerId.equals(instanceCustomer.getId())) {
								customer = (Customer) instanceCustomer.clone();
							}
						}
//					}
				} catch (CloneNotSupportedException ex) {
					Logger.getLogger(VerifyFacade.class.getName()).log(Level.SEVERE, null, ex);
				}
				JsonObject jtr = cliente.getAsJsonObject("jtr");
				JsonObject trajeto = cliente.getAsJsonObject("trajeto");
				JsonArray locais = trajeto.getAsJsonArray("locais");
				// getting route datas from customer
				CustomerRouteDatas customerRouteDatas = new CustomerRouteDatas();
				customerRouteDatas.setCurrentMileage(cliente.getAsJsonPrimitive("km").getAsDouble());
				customerRouteDatas.setServedExpedient(new Expedient(jtr.getAsJsonPrimitive("ini").getAsDouble(),
						jtr.getAsJsonPrimitive("fim").getAsDouble()));
				customerRouteDatas.setRoute(route);
				Path pathToNextCustomer = customerRouteDatas.getPathToNextCustomer();
				pathToNextCustomer.setExitId(trajeto.getAsJsonPrimitive("id0").getAsInt());
				pathToNextCustomer.setArrivalId(trajeto.getAsJsonPrimitive("id1").getAsInt());
				pathToNextCustomer.setExitCoordinates(new Coordinates(trajeto.getAsJsonPrimitive("x0").getAsDouble(),
						trajeto.getAsJsonPrimitive("y0").getAsDouble()));
				pathToNextCustomer.setArrivalCoordinates(new Coordinates(trajeto.getAsJsonPrimitive("x1").getAsDouble(),
						trajeto.getAsJsonPrimitive("y1").getAsDouble()));
				for (int k = 0; k < locais.size(); k++) {
					JsonObject local = locais.get(k).getAsJsonObject();
					pathToNextCustomer.getLocals().add(new Coordinates(local.getAsJsonPrimitive("x").getAsDouble(),
							local.getAsJsonPrimitive("y").getAsDouble()));
				}
				customer.setCustomerRouteDatas(customerRouteDatas);
				route.getCustomers().add(customer);
			}
			routes.add(route);
		}
		return routes;
	}
	
	protected List<Customer> getNotServedFromJsonObject(JsonObject jsonObject, Instance instance){
		JsonArray naoAtendidos = jsonObject.getAsJsonArray("naoAtendidos");
		List<Customer> instanceDepots = instance.getDepots();
		List<Customer> instanceCustomers = instance.getCustomers();
		List<Customer> notServed = new ArrayList<Customer>(naoAtendidos.size());
		for (int i = 0; i < naoAtendidos.size(); i++) {
			JsonObject clienteNaoAtendido = naoAtendidos.get(i).getAsJsonObject();
			Customer notServedCustomer = null;
			Integer customerId = new Integer(clienteNaoAtendido.getAsJsonPrimitive("id").getAsString());
			// searching customer in instance

			Customer findedDepot = null;
			for (Customer instanceDepot : instanceDepots) {
				if (customerId.equals(instanceDepot.getId())) {
					findedDepot = instanceDepot;
					break;
				}
			}
			if (findedDepot != null) {
				try {
					notServedCustomer = (Customer) findedDepot.clone();
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
			} else {
				for (Customer instanceCustomer : instanceCustomers) {
					if (customerId.equals(instanceCustomer.getId())) {
						notServedCustomer = instanceCustomer;
					}
				}
			}
			notServed.add(notServedCustomer);
		}
		return notServed;
	}
	
	/**
	 * @return the outputJsonObject
	 */
	public JsonObject getOutputJsonObject() {
		return outputJsonObject;
	}

	/**
	 * @param outputJsonObject the outputJsonObject to set
	 */
	public void setOutputJsonObject(JsonObject outputJsonObject) {
		if (outputJsonObject != null) {
			this.outputJsonObject = outputJsonObject;
		}else {
			throw new IllegalArgumentException("Erro: O parâmetro outputJsonObject não pode ser nulo.");
		}		
	}

	/**
	 * @return the instance
	 */
	public Instance getInstance() {
		return instance;
	}

	/**
	 * @param instance the instance to set
	 */
	public void setInstance(Instance instance) {
		if (instance != null) {
			this.instance = instance;
		}else {
			throw new IllegalArgumentException("Erro: O parâmetro instance não pode ser nulo");
		}		
	}
	
}
