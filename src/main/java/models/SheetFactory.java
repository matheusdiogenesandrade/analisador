/**
 * 
 */
package models;

import java.util.List;

import javax.swing.JProgressBar;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import utils.WriteExcel;

/**
 * @author projeto
 *
 */
public abstract class SheetFactory {
	protected Instance instance;
	protected Result result;
	protected String sheetName;
	protected String fileName;
	protected WriteExcel writeExcel;
	protected XSSFSheet xssfSheet;
	protected CellStyle boldCellStyle;
	protected CellStyle redColorCellStyle;
	protected CellStyle greenColorCellStyle;
	protected CellStyle yellowColorCellStyle;
	protected CellStyle headerColorCellStyle;
	protected CellStyle colorCellStyle;
	protected List<Route> routes;
	protected List<Vehicle> vehicles;
	protected List<Customer> notServedCustomers;
	protected List<Customer> lostCustomers;
	protected int rowCount;
	
	/**
	 * @param instance
	 * @param result
	 * @param sheetName
	 * @param fileName
	 */
	public SheetFactory(Instance instance, Result result, String sheetName, String fileName) {
		super();
		this.setInstance(instance);
		this.setResult(result);
		this.setSheetName(sheetName);
		this.setFileName(fileName);		
		this.setWriteExcel(new WriteExcel(this.getSheetName()));
		WriteExcel writeExcel = this.getWriteExcel();
		this.setXssfSheet(writeExcel.getXssfSheet());
		this.setBoldCellStyle(writeExcel.getXssfWorkbook().createCellStyle());
		this.setRedColorCellStyle(writeExcel.createCellStyle(Constants.ERROR_SHEET_COLOR,
				Constants.ERROR_SHEET_FILL_PATTERN_TYPE));
		this.setYellowColorCellStyle(writeExcel.createCellStyle(Constants.WARNING_SHEET_COLOR,
				Constants.ERROR_SHEET_FILL_PATTERN_TYPE));
		this.setGreenColorCellStyle(writeExcel.createCellStyle(Constants.WARNING_LUNCH_SHEET_COLOR,
				Constants.ERROR_SHEET_FILL_PATTERN_TYPE));
		this.setHeaderColorCellStyle(writeExcel.createCellStyle(IndexedColors.GREY_25_PERCENT.getIndex(),
				FillPatternType.LEAST_DOTS));
		this.setColorCellStyle(writeExcel.createCellStyle(IndexedColors.LEMON_CHIFFON.getIndex(),
				FillPatternType.LEAST_DOTS));
		CellStyle redColorCellStyle = this.getRedColorCellStyle();
		CellStyle yellowColorCellStyle = this.getYellowColorCellStyle();
		CellStyle greenColorCellStyle = this.getGreenColorCellStyle();
		CellStyle headerColorCellStyle = this.getHeaderColorCellStyle();
		CellStyle colorCellStyle = this.getColorCellStyle();
		redColorCellStyle.setAlignment(HorizontalAlignment.CENTER);
		yellowColorCellStyle.setAlignment(HorizontalAlignment.CENTER);
		greenColorCellStyle.setAlignment(HorizontalAlignment.CENTER);
		headerColorCellStyle.setAlignment(HorizontalAlignment.CENTER);
		colorCellStyle.setAlignment(HorizontalAlignment.CENTER);
		redColorCellStyle.setBorderBottom(BorderStyle.MEDIUM);
		greenColorCellStyle.setBorderBottom(BorderStyle.MEDIUM);
		headerColorCellStyle.setBorderBottom(BorderStyle.MEDIUM);
		colorCellStyle.setBorderBottom(BorderStyle.MEDIUM);
		redColorCellStyle.setBorderLeft(BorderStyle.MEDIUM);
		greenColorCellStyle.setBorderLeft(BorderStyle.MEDIUM);
		headerColorCellStyle.setBorderLeft(BorderStyle.MEDIUM);
		colorCellStyle.setBorderLeft(BorderStyle.MEDIUM);
		redColorCellStyle.setBorderRight(BorderStyle.MEDIUM);
		greenColorCellStyle.setBorderRight(BorderStyle.MEDIUM);
		headerColorCellStyle.setBorderRight(BorderStyle.MEDIUM);
		colorCellStyle.setBorderRight(BorderStyle.MEDIUM);
		redColorCellStyle.setBorderTop(BorderStyle.MEDIUM);
		greenColorCellStyle.setBorderTop(BorderStyle.MEDIUM);
		headerColorCellStyle.setBorderTop(BorderStyle.MEDIUM);
		colorCellStyle.setBorderTop(BorderStyle.MEDIUM);
		XSSFFont fontBold = writeExcel.getXssfWorkbook().createFont();
		fontBold.setBold(true);
		this.getBoldCellStyle().setFont(fontBold);
		headerColorCellStyle.setFont(fontBold);
		this.setRoutes(this.getResult().getRoutes());
		this.setVehicles(this.getInstance().getVehicles());
		this.setNotServedCustomers(this.getResult().getNotServed());
		this.setLostCustomers(VerifyFacade.getLostCustomers(this.getInstance(), this.getResult()));
		rowCount = 0;
	}
	
	public abstract WriteExcel createSheet(JProgressBar progressBar, int qtdInstance);	
	/**
	 * @return the instance
	 */
	public Instance getInstance() {
		return instance;
	}
	/**
	 * @param instance the instance to set
	 */
	public void setInstance(Instance instance) {		
		if (instance != null) {			
			this.instance = instance;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento instance não pode ser nulo");
		}
	}
	/**
	 * @return the result
	 */
	public Result getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(Result result) {		
		if (result != null) {			
			this.result = result;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento result não pode ser nulo");
		}
	}
	/**
	 * @return the sheetName
	 */
	public String getSheetName() {
		return sheetName;
	}
	/**
	 * @param sheetName the sheetName to set
	 */
	public void setSheetName(String sheetName) {		
		if (sheetName != null && !sheetName.isEmpty()) {			
			this.sheetName = sheetName;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento sheetName não pode ser nulo e não pode ser vazio");
		}
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @return the writeExcel
	 */
	public WriteExcel getWriteExcel() {
		return writeExcel;
	}

	/**
	 * @param writeExcel the writeExcel to set
	 */
	public void setWriteExcel(WriteExcel writeExcel) {
		if (writeExcel != null) {			
			this.writeExcel = writeExcel;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento writeExcel não pode ser nulo");
		}		
	}

	/**
	 * @return the xssfSheet
	 */
	public XSSFSheet getXssfSheet() {
		return xssfSheet;
	}

	/**
	 * @param xssfSheet the xssfSheet to set
	 */
	public void setXssfSheet(XSSFSheet xssfSheet) {		
		if (xssfSheet != null) {			
			this.xssfSheet = xssfSheet;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento xssfSheet não pode ser nulo");
		}
	}

	/**
	 * @return the boldCellStyle
	 */
	public CellStyle getBoldCellStyle() {
		return boldCellStyle;
	}

	/**
	 * @param boldCellStyle the boldCellStyle to set
	 */
	public void setBoldCellStyle(CellStyle boldCellStyle) {		
		if (boldCellStyle != null) {			
			this.boldCellStyle = boldCellStyle;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento boldCellStyle não pode ser nulo");
		}
	}

	/**
	 * @return the redColorCellStyle
	 */
	public CellStyle getRedColorCellStyle() {
		return redColorCellStyle;
	}

	/**
	 * @param redColorCellStyle the redColorCellStyle to set
	 */
	public void setRedColorCellStyle(CellStyle redColorCellStyle) {		
		if (redColorCellStyle != null) {			
			this.redColorCellStyle = redColorCellStyle;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento redColorCellStyle não pode ser nulo");
		}
	}

	/**
	 * @return the yellowColorCellStyle
	 */
	public CellStyle getYellowColorCellStyle() {
		return yellowColorCellStyle;
	}

	/**
	 * @param yellowColorCellStyle the yellowColorCellStyle to set
	 */
	public void setYellowColorCellStyle(CellStyle yellowColorCellStyle) {		
		if (yellowColorCellStyle != null) {			
			this.yellowColorCellStyle = yellowColorCellStyle;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento yellowColorCellStyle não pode ser nulo");
		}
	}

	/**
	 * @return the headerColorCellStyle
	 */
	public CellStyle getHeaderColorCellStyle() {
		return headerColorCellStyle;
	}

	/**
	 * @param headerColorCellStyle the headerColorCellStyle to set
	 */
	public void setHeaderColorCellStyle(CellStyle headerColorCellStyle) {		
		if (headerColorCellStyle != null) {			
			this.headerColorCellStyle = headerColorCellStyle;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento headerColorCellStyle não pode ser nulo");
		}
	}

	/**
	 * @return the colorCellStyle
	 */
	public CellStyle getColorCellStyle() {
		return colorCellStyle;
	}

	/**
	 * @param colorCellStyle the colorCellStyle to set
	 */
	public void setColorCellStyle(CellStyle colorCellStyle) {		
		if (colorCellStyle != null) {			
			this.colorCellStyle = colorCellStyle;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento colorCellStyle não pode ser nulo");
		}
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {		
		if (fileName != null && !fileName.isEmpty()) {			
			this.fileName = fileName;
		}else {
			throw new IllegalArgumentException ("Erro: O argumento fileName não pode ser nulo e não pode ser vazio");
		}
	}
	
	/**
	 * @return the routes
	 */
	public List<Route> getRoutes() {
		return routes;
	}

	/**
	 * @param routes
	 *            the routes to set
	 */
	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

	/**
	 * @return the vehicles
	 */
	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	/**
	 * @param vehicles
	 *            the vehicles to set
	 */
	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	/**
	 * @return the notServedCustomers
	 */
	public List<Customer> getNotServedCustomers() {
		return notServedCustomers;
	}

	/**
	 * @param notServedCustomers
	 *            the notServedCustomers to set
	 */
	public void setNotServedCustomers(List<Customer> notServedCustomers) {
		this.notServedCustomers = notServedCustomers;
	}

	/**
	 * @return the lostCustomers
	 */
	public List<Customer> getLostCustomers() {
		return lostCustomers;
	}

	/**
	 * @param lostCustomers
	 *            the lostCustomers to set
	 */
	public void setLostCustomers(List<Customer> lostCustomers) {
		this.lostCustomers = lostCustomers;
	}

	/**
	 * @return the rowCount
	 */
	public int getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the greenColorCellStyle
	 */
	public CellStyle getGreenColorCellStyle() {
		return greenColorCellStyle;
	}

	/**
	 * @param greenColorCellStyle the greenColorCellStyle to set
	 */
	public void setGreenColorCellStyle(CellStyle greenColorCellStyle) {
		this.greenColorCellStyle = greenColorCellStyle;
	}
	
}
