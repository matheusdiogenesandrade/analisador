/**
 * 
 */
package models;

import java.util.List;

import com.google.gson.JsonObject;

/**
 * @author projeto
 *
 */
public class OutputFactoryRotas extends OutputFactory {

	private OutputFactoryGrupos outputFactoryGrupos;
	
	public OutputFactoryRotas(JsonObject outputJsonObject, Instance instance) {
		super(outputJsonObject, instance);
		this.setOutputFactoryGrupos(new OutputFactoryGrupos(outputJsonObject, instance));
	}

	@Override
	public List<Route> createRoutes() {
		return this.getOutputFactoryGrupos().createRoutes();
	}

	@Override
	public List<Customer> createNotServed() {
		return this.getOutputFactoryGrupos().createNotServed();
	}

	@Override
	public Double createAlgorithmTime() {		
		return super.getOutputJsonObject().getAsJsonObject("resultado").getAsJsonObject("resumo")
				.getAsJsonPrimitive("duracaoCalculo").getAsDouble();
	}

	/**
	 * @return the outputFactoryGrupos
	 */
	public OutputFactoryGrupos getOutputFactoryGrupos() {
		return outputFactoryGrupos;
	}

	/**
	 * @param outputFactoryGrupos the outputFactoryGrupos to set
	 */
	public void setOutputFactoryGrupos(OutputFactoryGrupos outputFactoryGrupos) {		
		if (outputFactoryGrupos != null) {
			this.outputFactoryGrupos = outputFactoryGrupos;
		}else {
			throw new IllegalArgumentException("Erro: O parâmetro outputFactoryGrupos não pode ser nulo.");
		}	
	}
	
}
