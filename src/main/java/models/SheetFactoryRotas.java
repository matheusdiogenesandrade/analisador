/**
 * 
 */
package models;

import java.util.List;

import javax.swing.JProgressBar;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import utils.WriteExcel;

/**
 * @author projeto
 *
 */
public class SheetFactoryRotas extends SheetFactory {

	public SheetFactoryRotas(Instance instance, Result result, String sheetName, String fileName) {
		super(instance, result, sheetName, fileName);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.SheetFactory#createSheet()
	 */
	@Override
	public WriteExcel createSheet(JProgressBar progressBar, int qtdInstance) {
		progressBar.setValue(progressBar.getValue()+5/qtdInstance);
		fillRoute();
		progressBar.setValue(progressBar.getValue()+10/qtdInstance);
		fillVehicles();
		progressBar.setValue(progressBar.getValue()+5/qtdInstance);
		fillCustomers();
		progressBar.setValue(progressBar.getValue()+20/qtdInstance);
		fillNotServedCustomers();
		progressBar.setValue(progressBar.getValue()+10/qtdInstance);
		fillLostedCustomers();
		progressBar.setValue(progressBar.getValue()+5/qtdInstance);
		fillInvalidPaths();
		progressBar.setValue(progressBar.getValue()+5/qtdInstance);
		fillEuclidianPaths();
		progressBar.setValue(progressBar.getValue()+15/qtdInstance);
		return writeExcel;
	}

	protected void fillRoute() {
		System.out.println("HEADER from routes");
		// fill routes
		// header
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellValue((String) "GRUPOS");
		cell.setCellStyle(boldCellStyle);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellValue((String) "NÃºm. de grupos: ");
		cell.setCellStyle(boldCellStyle);
		cell = row.createCell(2);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) routes.size());
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 1, 2));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 12));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Grupo");
		cell = row.createCell(1);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Peso ocupado");
		cell = row.createCell(3);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo ocupado");
		cell = row.createCell(5);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "NÃºm. de visitas feitas");
		cell = row.createCell(8);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Cubagem ocupada");
		cell = row.createCell(10);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Núm. de janelas quebradas");
		cell = row.createCell(13);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Placa do veÃ­culo");
		System.out.println("CONTENT from routes");
		// content
		for (Route route : routes) {
			Vehicle routeVehicle = route.getVehicle();
			VehicleRouteDatas vehicleRouteDatas = routeVehicle.getVehicleRouteDatas();
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 1, 2));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 12));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
			row = xssfSheet.createRow(rowCount++);
			cell = row.createCell(0);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) route.getId());
			cell = row.createCell(1);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) vehicleRouteDatas.getWeightFilled());
			cell = row.createCell(3);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) vehicleRouteDatas.getServiceTimeFilled());
			cell = row.createCell(5);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) vehicleRouteDatas.getVisitsMade());
			cell = row.createCell(8);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) vehicleRouteDatas.getCubingFilled());
			cell = row.createCell(10);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) VerifyFacade.getBrokenExpedientsNumber(route.getCustomers()));
			cell = row.createCell(13);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((String) routeVehicle.getBoard());
			if (!VerifyFacade.vehicleExistsInInstace(routeVehicle, super.getInstance())) {
				cell.setCellStyle(redColorCellStyle);
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 1, 2));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 12));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
		row = xssfSheet.createRow(rowCount++);
	}

	protected void fillVehicles() {
		System.out.println("HEADER from vehicles");
		// fill vehicles
		// header
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "VEÍCULOS");
		int usedVehiclesNumber = VerifyFacade.getUsedVehiclesNumber(vehicles);
		int unusedVehiclesNumber = vehicles.size() - usedVehiclesNumber;
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade de veí­culos usados: ");
		cell = row.createCell(4);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) usedVehiclesNumber);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade de veículos ociosos: ");
		cell = row.createCell(4);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) unusedVehiclesNumber);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 3));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 8));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 11));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Grupo");
		cell = row.createCell(1);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Placa");
		cell = row.createCell(2);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Peso limite");
		cell = row.createCell(4);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Peso ocupado");
		cell = row.createCell(6);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo limite de serviço");
		cell = row.createCell(9);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo serviço ocupado");
		cell = row.createCell(12);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Núm. limite de visitas");
		cell = row.createCell(14);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Visitas feitas");
		cell = row.createCell(16);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Cubagem limite");
		cell = row.createCell(18);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Cubagem ocupada");
		System.out.println("CONTENT from vehicles");
		// content
		for (Vehicle vehicle : vehicles) {
			VehicleRouteDatas vehicleRouteDatas = vehicle.getVehicleRouteDatas();
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 3));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 8));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 11));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
			row = xssfSheet.createRow(rowCount++);
			if (vehicleRouteDatas != null) {
				cell = row.createCell(0);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) VerifyFacade.formatRoutesIds(vehicleRouteDatas.getServedRoutes()));
				cell = row.createCell(1);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) vehicle.getBoard());
				cell = row.createCell(2);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getWeightLimit());
				cell = row.createCell(4);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRouteDatas.getWeightFilled());
				if (!VerifyFacade.isValidVehicleFilledWeight(vehicle)) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(6);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getServiceTimeLimit());
				cell = row.createCell(9);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRouteDatas.getServiceTimeFilled());
				if (!VerifyFacade.isValidVehicleFilledServiceTime(vehicle)) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(12);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) vehicle.getVisitsLimit());
				cell = row.createCell(14);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) vehicleRouteDatas.getVisitsMade());
				if (!VerifyFacade.isValidMadeVisitsNumberByVehicle(vehicle)) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(16);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getCubingLimit());
				cell = row.createCell(18);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRouteDatas.getCubingFilled());
				if (!VerifyFacade.isValidVehicleFilledCubing(vehicle)) {
					cell.setCellStyle(redColorCellStyle);
				}
			} else {
				cell = row.createCell(0);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) "");
				cell = row.createCell(1);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) vehicle.getBoard());
				cell = row.createCell(2);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getWeightLimit());
				cell = row.createCell(4);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) 0d);
				cell = row.createCell(6);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getServiceTimeLimit());
				cell = row.createCell(9);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) 0d);
				cell = row.createCell(12);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) vehicle.getVisitsLimit());
				cell = row.createCell(14);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) 0);
				cell = row.createCell(16);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getCubingLimit());
				cell = row.createCell(18);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) 0d);
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 3));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 8));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 11));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
		row = xssfSheet.createRow(rowCount++);
	}

	protected void fillCustomers() {
		System.out.println("HEADER from served customers");
		// fill served customers
		// header
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "CLIENTES ATENDIDOS");
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		row = xssfSheet.createRow(rowCount++);
		Integer servedCustomersNumber = 0;
		for (Route route : routes) {
			servedCustomersNumber += route.getCustomers().size();
		}
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade: ");
		cell = row.createCell(2);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) servedCustomersNumber);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 7));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 11));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Grupo");
		cell = row.createCell(1);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID");
		cell = row.createCell(2);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "X");
		cell = row.createCell(3);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Y");
		cell = row.createCell(4);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Demanda de peso");
		cell = row.createCell(6);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo de serviço");
		cell = row.createCell(8);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Núm. visitas limite");
		cell = row.createCell(10);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Kilometragem atual");
		cell = row.createCell(12);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Expediente de início");
		cell = row.createCell(14);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Iní­cio atendimento");
		cell = row.createCell(16);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Expediente de fim");
		cell = row.createCell(18);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Fim atendimento");
		cell = row.createCell(20);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Placa veí­culo");
		System.out.println("CONTENT from served customers");
		// content
		for (int i = 0; i < routes.size(); i++) {
			Route route = routes.get(i);
			Vehicle routeVehicle = route.getVehicle();
			// set customers route
			List<Customer> customers = route.getCustomers();
			servedCustomersNumber += customers.size();
			// depot
			Customer depot = route.getDepot();
			CustomerRouteDatas depotRouteDatas = depot.getCustomerRouteDatas();
			Expedient depotExpedient = depot.getExpedient();
			Expedient depotRouteExpedient = depotRouteDatas.getServedExpedient();
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 7));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 11));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
			row = xssfSheet.createRow(rowCount++);
			cell = row.createCell(0);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) route.getId());
			cell = row.createCell(1);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) depot.getId());
			cell = row.createCell(2);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depot.getX());
			cell = row.createCell(3);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depot.getY());
			cell = row.createCell(4);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue(depot.getWeight() == null ? 0 : (Double) depot.getWeight());
			cell = row.createCell(6);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue(depot.getServiceTime() == null ? 0 : (Double) depot.getServiceTime());
			cell = row.createCell(8);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue(depot.getVisitsLimit() == null ? 0 : (Integer) depot.getVisitsLimit());
			cell = row.createCell(10);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotRouteDatas.getCurrentMileage());
			cell = row.createCell(12);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotExpedient.getStart());
			cell = row.createCell(14);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotRouteExpedient.getStart());
			if (VerifyFacade.isStartExpedientBroken(depotExpedient.getStart(), depotRouteExpedient.getStart())) {
				cell.setCellStyle(redColorCellStyle);
			}
			cell = row.createCell(16);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotExpedient.getFinish());
			cell = row.createCell(18);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotRouteExpedient.getFinish());
			if (VerifyFacade.isFinishExpedientBroken(depotExpedient.getFinish(), depotRouteExpedient.getFinish())) {
				cell.setCellStyle(redColorCellStyle);
			}
			cell = row.createCell(20);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((String) routeVehicle.getBoard());
			// anothers customers
			for (int j = 0; j < customers.size(); j++) {
				Customer customer = customers.get(j);
				CustomerRouteDatas customerRouteDatas = customer.getCustomerRouteDatas();
				Expedient expedient = customer.getExpedient();
				Expedient routeExpedient = customerRouteDatas.getServedExpedient();
				// if customer is repeated in anothers routes
				Boolean isCustomerRepeatedInAnotherRoute = false;
				for (int k = i - 1; k >= 0; k--) {
					List<Customer> customersFromRouteK = routes.get(k).getCustomers();
					for (Customer routeCustomer : customersFromRouteK) {
						if (routeCustomer.getId().equals(customer.getId())) {
							isCustomerRepeatedInAnotherRoute = true;
							k = 0;
							break;
						}
					}
				}
				// if customer is repeated in route
				Boolean isCustomerRepeatedRoute = false;
				for (int k = j - 1; k >= 0; k--) {
					if (customers.get(k).getId().equals(customer.getId())) {
						isCustomerRepeatedRoute = true;
						k = 0;
						break;
					}
				}
				// alerts
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 7));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 11));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
				row = xssfSheet.createRow(rowCount++);
				cell = row.createCell(0);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) route.getId());
				if (isCustomerRepeatedInAnotherRoute) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(1);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) customer.getId());
				if (isCustomerRepeatedRoute) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(2);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customer.getX());
				cell = row.createCell(3);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customer.getY());
				cell = row.createCell(4);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue(customer.getWeight() == null ? 0 : (Double) customer.getWeight());
				cell = row.createCell(6);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue(customer.getServiceTime() == null ? 0 : (Double) customer.getServiceTime());
				cell = row.createCell(8);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue(customer.getVisitsLimit() == null ? 0 : (Integer) customer.getVisitsLimit());
				cell = row.createCell(10);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customerRouteDatas.getCurrentMileage());
				cell = row.createCell(12);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) expedient.getStart());
				cell = row.createCell(14);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) routeExpedient.getStart());
				if (VerifyFacade.isStartExpedientBroken(expedient.getStart(), routeExpedient.getStart())) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(16);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) expedient.getFinish());
				cell = row.createCell(18);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) routeExpedient.getFinish());
				if (VerifyFacade.isFinishExpedientBroken(expedient.getFinish(), routeExpedient.getFinish())) {
					cell.setCellStyle(yellowColorCellStyle);
				}
				cell = row.createCell(20);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) routeVehicle.getBoard());
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 7));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 11));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
		row = xssfSheet.createRow(rowCount++);
	}

	protected void fillNotServedCustomers() {
		System.out.println("HEADER from not served customers");
		// fill not served customers
		// header
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "CLIENTES NÃO ATENDIDOS");
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade: ");
		cell = row.createCell(2);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) notServedCustomers.size());
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 6));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 8));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 10));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID");
		cell = row.createCell(1);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "X");
		cell = row.createCell(2);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Y");
		cell = row.createCell(3);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Demanda de peso");
		cell = row.createCell(5);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo de serviço");
		cell = row.createCell(7);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Núm. visitas limite");
		cell = row.createCell(9);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Cubagem limite");
		cell = row.createCell(11);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Expediente de iní­cio");
		cell = row.createCell(13);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Expediente de fim");
		System.out.println("HEADER from not served customers");
		// content
		for (Customer customer : notServedCustomers) {
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 6));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 8));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 10));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
			row = xssfSheet.createRow(rowCount++);
			cell = row.createCell(0);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) customer.getId());
			cell = row.createCell(1);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getX());
			cell = row.createCell(2);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getY());
			cell = row.createCell(3);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getWeight());
			cell = row.createCell(5);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getServiceTime());
			cell = row.createCell(7);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) customer.getVisitsLimit());
			cell = row.createCell(9);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getCubing());
			if (customer.getExpedient() != null) {
				cell = row.createCell(11);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customer.getExpedient().getStart());
				cell = row.createCell(13);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customer.getExpedient().getFinish());
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 6));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 8));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 10));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
		row = xssfSheet.createRow(rowCount++);
	}

	protected void fillLostedCustomers() {
		System.out.println("HEADER from losted customers");
		// fill losted customers
		// header
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "CLIENTES PERDIDOS");
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade: ");
		cell = row.createCell(2);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) lostCustomers.size());
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 6));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 8));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 10));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID");
		cell = row.createCell(1);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "X");
		cell = row.createCell(2);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Y");
		cell = row.createCell(3);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Demanda de peso");
		cell = row.createCell(5);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo de serviço");
		cell = row.createCell(7);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Núm. visitas limite");
		cell = row.createCell(9);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Cubagem limite");
		cell = row.createCell(11);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Expediente de início");
		cell = row.createCell(13);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Expediente de fim");
		System.out.println("CONTENT from losted customers");
		// content
		for (Customer customer : lostCustomers) {
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 6));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 8));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 10));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
			row = xssfSheet.createRow(rowCount++);
			cell = row.createCell(0);
			cell.setCellStyle(redColorCellStyle);
			cell.setCellValue((Integer) customer.getId());
			cell = row.createCell(1);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getX());
			cell = row.createCell(2);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getY());
			cell = row.createCell(3);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getWeight());
			cell = row.createCell(5);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getServiceTime());
			cell = row.createCell(7);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) customer.getVisitsLimit());
			cell = row.createCell(9);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) customer.getCubing());
			if (customer.getExpedient() != null) {
				cell = row.createCell(11);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customer.getExpedient().getStart());
				cell = row.createCell(13);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customer.getExpedient().getFinish());
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 3, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 6));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 7, 8));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 10));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
		row = xssfSheet.createRow(rowCount++);
	}

	protected void fillInvalidPaths() {
		// invalid paths
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "CAMINHOS INVÁLIDOS");
		Integer wrongPathsNumber = 0;
		for (Route route : routes) {
			List<Customer> customers = route.getCustomers();
			if (customers.size() > 0) {
				Customer depot = route.getDepot();
				// verify wrong paths
				// depot
				Customer firstCustomer = customers.get(0);
				Double distance = firstCustomer.getCustomerRouteDatas().getCurrentMileage();
				Double travelTime = firstCustomer.getCustomerRouteDatas().getServedExpedient().getStart()
						- depot.getExpedient().getStart();
				Double speed = distance / travelTime;
				if (speed < Constants.MINIMUM_VEHICLE_AVERAGE_SPEED
						|| speed > Constants.MAXIMUM_VEHICLE_AVERAGE_SPEED) {
					wrongPathsNumber++;
				}
				for (int i = 0; i < customers.size() - 1; i++) {
					firstCustomer = customers.get(i);
					Customer secondCustomer = customers.get(i + 1);
					// alerts
					distance = VerifyFacade.getDistanceByRoutedCustomers(firstCustomer, secondCustomer);
					travelTime = VerifyFacade.getTravelTimeByRoutedCustomers(firstCustomer, secondCustomer);
					speed = VerifyFacade.getSpeedByCustomers(firstCustomer, secondCustomer);
					if (speed < Constants.MINIMUM_VEHICLE_AVERAGE_SPEED
							|| speed > Constants.MAXIMUM_VEHICLE_AVERAGE_SPEED) {
						wrongPathsNumber++;
					}
				}
				// depot
				Customer lastCustomer = customers.get(customers.size() - 1);
				distance = VerifyFacade.getDistanceByRoutedCustomers(lastCustomer, depot);
				travelTime = depot.getCustomerRouteDatas().getServedExpedient().getFinish() - depot.getServiceTime()
						- lastCustomer.getCustomerRouteDatas().getServedExpedient().getFinish();
				speed = distance / travelTime;
				if (speed < Constants.MINIMUM_VEHICLE_AVERAGE_SPEED
						|| speed > Constants.MAXIMUM_VEHICLE_AVERAGE_SPEED) {
					wrongPathsNumber++;
				}
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade: ");
		cell = row.createCell(2);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) wrongPathsNumber);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 10));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Rota");
		cell = row.createCell(2);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Cliente Origem");
		cell = row.createCell(5);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Cliente Destino");
		cell = row.createCell(8);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Distância Percorrida");
		cell = row.createCell(11);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo");
		cell = row.createCell(13);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Velocidade");
		for (Route route : routes) {
			List<Customer> customers = route.getCustomers();
			if (customers.size() > 0) {
				Customer depot = route.getDepot();
				// verify wrong paths
				// depot
				Customer firstCustomer = customers.get(0);
				Double distance = firstCustomer.getCustomerRouteDatas().getCurrentMileage();
				Double travelTime = firstCustomer.getCustomerRouteDatas().getServedExpedient().getStart()
						- depot.getExpedient().getStart();
				Double speed = distance / travelTime;
				if (speed < Constants.MINIMUM_VEHICLE_AVERAGE_SPEED
						|| speed > Constants.MAXIMUM_VEHICLE_AVERAGE_SPEED) {
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 10));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
					row = xssfSheet.createRow(rowCount++);
					cell = row.createCell(0);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) route.getId());
					cell = row.createCell(2);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) depot.getId());
					cell = row.createCell(5);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) firstCustomer.getId());
					cell = row.createCell(8);
					cell.setCellStyle(redColorCellStyle);
					cell.setCellValue((Double) distance);
					cell = row.createCell(11);
					cell.setCellStyle(redColorCellStyle);
					cell.setCellValue((Double) travelTime);
					cell = row.createCell(13);
					cell.setCellStyle(redColorCellStyle);
					cell.setCellValue((Double) speed);
				}
				for (int i = 0; i < customers.size() - 1; i++) {
					firstCustomer = customers.get(i);
					Customer secondCustomer = customers.get(i + 1);
					// alerts
					distance = VerifyFacade.getDistanceByRoutedCustomers(firstCustomer, secondCustomer);
					travelTime = VerifyFacade.getTravelTimeByRoutedCustomers(firstCustomer, secondCustomer);
					speed = VerifyFacade.getSpeedByCustomers(firstCustomer, secondCustomer);
					if (speed < Constants.MINIMUM_VEHICLE_AVERAGE_SPEED
							|| speed > Constants.MAXIMUM_VEHICLE_AVERAGE_SPEED) {
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 10));
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
						row = xssfSheet.createRow(rowCount++);
						cell = row.createCell(0);
						cell.setCellStyle(colorCellStyle);
						cell.setCellValue((Integer) route.getId());
						cell = row.createCell(2);
						cell.setCellStyle(colorCellStyle);
						cell.setCellValue((Integer) firstCustomer.getId());
						cell = row.createCell(5);
						cell.setCellStyle(colorCellStyle);
						cell.setCellValue((Integer) secondCustomer.getId());
						cell = row.createCell(8);
						cell.setCellStyle(redColorCellStyle);
						cell.setCellValue((Double) distance);
						cell = row.createCell(11);
						cell.setCellStyle(redColorCellStyle);
						cell.setCellValue((Double) travelTime);
						cell = row.createCell(13);
						cell.setCellStyle(redColorCellStyle);
						cell.setCellValue((Double) speed);
					}
				}
				// depot
				Customer lastCustomer = customers.get(customers.size() - 1);
				distance = VerifyFacade.getDistanceByRoutedCustomers(lastCustomer, depot);
				travelTime = depot.getCustomerRouteDatas().getServedExpedient().getFinish() - depot.getServiceTime()
						- lastCustomer.getCustomerRouteDatas().getServedExpedient().getFinish();
				speed = distance / travelTime;
				if (speed < Constants.MINIMUM_VEHICLE_AVERAGE_SPEED
						|| speed > Constants.MAXIMUM_VEHICLE_AVERAGE_SPEED) {
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 10));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
					row = xssfSheet.createRow(rowCount++);
					cell = row.createCell(0);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) route.getId());
					cell = row.createCell(2);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) firstCustomer.getId());
					cell = row.createCell(5);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) depot.getId());
					cell = row.createCell(8);
					cell.setCellStyle(redColorCellStyle);
					cell.setCellValue((Double) distance);
					cell = row.createCell(11);
					cell.setCellStyle(redColorCellStyle);
					cell.setCellValue((Double) travelTime);
					cell = row.createCell(13);
					cell.setCellStyle(redColorCellStyle);
					cell.setCellValue((Double) speed);
				}
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 10));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 11, 12));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 13, 14));
		row = xssfSheet.createRow(rowCount++);
	}

	protected void fillEuclidianPaths() {
		// invalid paths
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "CAMINHOS EUCLIDIANOS");
		Integer euclidianPathsNumber = 0;
		Boolean[][] euclidianPaths = new Boolean[routes.size()][];
		for (int i = 0; i < routes.size(); i++) {
			Route route = routes.get(i);
			List<Customer> customers = route.getCustomers();
			euclidianPaths[i] = new Boolean[customers.size() + 1];
			if (route.getDepot().getCustomerRouteDatas().getPathToNextCustomer().getLocals().size() == 4) {
				euclidianPaths[i][0] = true;
				euclidianPathsNumber++;
			}else {
				euclidianPaths[i][0] = false;
			}
			for (int j = 0; j < customers.size(); j++) {
				if (customers.get(j).getCustomerRouteDatas().getPathToNextCustomer().getLocals()
						.size() == 4) {
					euclidianPaths[i][j + 1] = true;
					euclidianPathsNumber++;
				}else {
					euclidianPaths[i][j + 1] = false;
				}
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade: ");
		cell = row.createCell(2);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) euclidianPathsNumber);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Rota");
		cell = row.createCell(2);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Cliente Origem");
		cell = row.createCell(5);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Cliente Destino");		
		for (int i = 0; i < routes.size(); i++) {
			Route route = routes.get(i);
			List<Customer> customers = route.getCustomers();
			if (customers.size() > 0) {
				Customer depot = route.getDepot();
				// verify wrong paths
				// depot
				Customer firstCustomer = customers.get(1);
				if (euclidianPaths[i][0]) {
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
					row = xssfSheet.createRow(rowCount++);
					cell = row.createCell(0);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) route.getId());
					cell = row.createCell(2);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) depot.getId());
					cell = row.createCell(5);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) firstCustomer.getId());
				}
				for (int j = 1; j < customers.size() - 1; j++) {
					firstCustomer = customers.get(j);
					Customer secondCustomer = customers.get(j + 1);
					// alerts
					if (euclidianPaths[i][j]) {
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
						xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
						row = xssfSheet.createRow(rowCount++);
						cell = row.createCell(0);
						cell.setCellStyle(colorCellStyle);
						cell.setCellValue((Integer) route.getId());
						cell = row.createCell(2);
						cell.setCellStyle(colorCellStyle);
						cell.setCellValue((Integer) firstCustomer.getId());
						cell = row.createCell(5);
						cell.setCellStyle(colorCellStyle);
						cell.setCellValue((Integer) secondCustomer.getId());
					}
				}
				// depot
				if (euclidianPaths[i][euclidianPaths[i].length - 1]) {
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
					xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
					row = xssfSheet.createRow(rowCount++);
					cell = row.createCell(0);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) route.getId());
					cell = row.createCell(2);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) customers.get(customers.size() - 1).getId());
					cell = row.createCell(5);
					cell.setCellStyle(colorCellStyle);
					cell.setCellValue((Integer) depot.getId());
				}
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 4));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 5, 7));
		row = xssfSheet.createRow(rowCount++);
	}
}
