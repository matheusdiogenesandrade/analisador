/**
 * 
 */
package models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import models.Instance.VelocityPerDistance;

/**
 * @author projeto
 *
 */
public class InstanceFactoryRotas extends InstanceFactory {

	public InstanceFactoryRotas(JsonObject instanceJsonObject) {
		super(instanceJsonObject, InstanceType.ROTAS);		
	}
	
	private InstanceFactoryRotas(JsonObject instanceJsonObject, InstanceType instanceType) {
		super(instanceJsonObject, instanceType);		
	}

	@Override
	public List<Customer> createDepots() {
		return new InstanceFactoryGrupos(super.getInstanceJsonObject()).createDepots();
	}

	@Override
	public List<Customer> createCustomers() {
		List<Customer> customers = new ArrayList<Customer>();
		// getting grupo
		JsonArray grupos = super.getInstanceJsonObject().getAsJsonArray("grupos");
		JsonObject params = super.getInstanceJsonObject().getAsJsonObject("params");
		// getting exped
		JsonObject exped = params.getAsJsonObject("exped");
		Expedient expedient = new Expedient(exped.getAsJsonPrimitive("ini").getAsDouble(),
				exped.getAsJsonPrimitive("fim").getAsDouble());
		for (int i = 0; i < grupos.size(); i++) {	
			JsonObject grupo = grupos.get(i).getAsJsonObject();
			customers.addAll(getCustomersByJSONArray(grupo.getAsJsonArray("clientes"), expedient));
		}	
		return customers;
	}

	@Override
	public List<VelocityPerDistance> createVelocitiesPerDistance() {
		return new InstanceFactoryGrupos(super.getInstanceJsonObject()).createVelocitiesPerDistance();
	}

	@Override
	public List<Vehicle> createVehicles() {
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		// getting grupo
		JsonArray grupos = super.getInstanceJsonObject().getAsJsonArray("grupos");
		for (int i = 0; i < grupos.size(); i++) {	
			JsonObject grupo = grupos.get(i).getAsJsonObject();
			vehicles.add(getVehicleByJSONObject(grupo.getAsJsonObject("veiculo")));
		}	
		return vehicles;
	}

}
