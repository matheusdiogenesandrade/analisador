/**
 *
 */
package models;

import java.util.List;

/**
 * @author matheus
 *
 */
public class Result {

    private List<Customer> notServed;
    private List<Route> routes;
    private Double algorithmTime;

    /**
     * @return the notServed
     */
    public List<Customer> getNotServed() {
        return notServed;
    }

    /**
     * @param notServed the notServed to set
     */
    public void setNotServed(List<Customer> notServed) {
        if (notServed != null) {
            this.notServed = notServed;
        } else {
            throw new IllegalArgumentException("Erro: O parâmetro notServed não pode ser nulo.");
        }
    }

    /**
     * @return the routes
     */
    public List<Route> getRoutes() {
        return routes;
    }

    /**
     * @param routes the routes to set
     */
    public void setRoutes(List<Route> routes) {
        if (routes != null){
            this.routes = routes;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro routes não pode ser nulo.");
        }        
    }

    /**
     * @return the algorithmTime
     */
    public Double getAlgorithmTime() {
        return algorithmTime;
    }

    /**
     * @param algorithmTime the algorithmTime to set
     */
    public void setAlgorithmTime(Double algorithmTime) {
        if (algorithmTime != null){
            this.algorithmTime = algorithmTime;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro algorithmTime não pode ser nulo.");
        }                
    }

}
