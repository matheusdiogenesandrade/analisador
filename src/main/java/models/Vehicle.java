/**
 *
 */
package models;

/**
 * @author matheus
 *
 */
public class Vehicle {

    private String board;
    private VehicleRestrictions vehicleRestrictions;
    private VehicleRouteDatas vehicleRouteDatas;

    public Vehicle() {
        this.setVehicleRestrictions(new VehicleRestrictions());
    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Vehicle vehicle = new Vehicle();
		vehicle.setBoard(this.getBoard());
		if (this.getVehicleRestrictions() != null) {
			vehicle.setVehicleRestrictions((VehicleRestrictions) this.getVehicleRestrictions().clone());
		}
		if (this.getVehicleRouteDatas() != null) {
			vehicle.setVehicleRouteDatas((VehicleRouteDatas) this.getVehicleRouteDatas().clone());
		}
		return vehicle; 
	}

	/**
     * @return the board
     */
    public String getBoard() {
        return board;
    }

    /**
     * @param board the board to set
     */
    public void setBoard(String board) {
        if (board != null && !board.isEmpty()) {
            this.board = board;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro board deve ser diferente de nulo e não vazio.");
        }
    }

    /**
     * @return the visitsLimit
     */
    public Integer getVisitsLimit() {
        return this.getVehicleRestrictions().getVisitsLimit();
    }

    /**
     * @param visitsLimit the visitsLimit to set
     */
    public void setVisitsLimit(Integer visitsLimit) {
        this.getVehicleRestrictions().setVisitsLimit(visitsLimit);
    }

    /**
     * @return the cubingLimit
     */
    public Double getCubingLimit() {
        return this.getVehicleRestrictions().getCubingLimit();
    }

    /**
     * @param cubingLimit the cubingLimit to set
     */
    public void setCubingLimit(Double cubingLimit) {
        this.getVehicleRestrictions().setCubingLimit(cubingLimit);
    }

    /**
     * @return the serviceTimeLimit
     */
    public Double getServiceTimeLimit() {
        return this.getVehicleRestrictions().getServiceTimeLimit();
    }

    /**
     * @param serviceTimeLimit the serviceTimeLimit to set
     */
    public void setServiceTimeLimit(Double serviceTimeLimit) {
        this.getVehicleRestrictions().setServiceTimeLimit(serviceTimeLimit);
    }

    /**
     * @return the weightLimit
     */
    public Double getWeightLimit() {
        return this.getVehicleRestrictions().getWeightLimit();
    }

    /**
     * @param weightLimit the weightLimit to set
     */
    public void setWeightLimit(Double weightLimit) {
        this.getVehicleRestrictions().setWeightLimit(weightLimit);
    }

    /**
     * @return the vehicleRouteDatas
     */
    public VehicleRouteDatas getVehicleRouteDatas() {
        return vehicleRouteDatas;
    }

    /**
     * @param vehicleRouteDatas the vehicleRouteDatas to set
     */
    public void setVehicleRouteDatas(VehicleRouteDatas vehicleRouteDatas) {
        if (vehicleRouteDatas != null) {
            this.vehicleRouteDatas = vehicleRouteDatas;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro weightLimit deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

    /**
     * @return the vehicleRestrictions
     */
    public VehicleRestrictions getVehicleRestrictions() {
        return vehicleRestrictions;
    }

    /**
     * @param vehicleRestrictions the vehicleRestrictions to set
     */
    public void setVehicleRestrictions(VehicleRestrictions vehicleRestrictions) {
        this.vehicleRestrictions = vehicleRestrictions;
    }

}
