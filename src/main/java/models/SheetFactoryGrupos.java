/**
 * 
 */
package models;

import javax.swing.JProgressBar;

import utils.WriteExcel;

/**
 * @author projeto
 *
 */
public class SheetFactoryGrupos extends SheetFactory {

	private SheetFactoryRotas sheetFactoryRotas; 
	
	public SheetFactoryGrupos(Instance instance, Result result, String sheetName, String fileName) {
		super(instance, result, sheetName, fileName);
		this.setSheetFactoryRotas(new SheetFactoryRotas(instance, result, sheetName, fileName));
	}

	/* (non-Javadoc)
	 * @see models.SheetFactory#createSheet()
	 */
	@Override
	public WriteExcel createSheet(JProgressBar progressBar, int qtdInstance) {		
		return this.getSheetFactoryRotas().createSheet(progressBar, qtdInstance);
	}

	/**
	 * @return the sheetFactoryRotas
	 */
	public SheetFactoryRotas getSheetFactoryRotas() {
		return sheetFactoryRotas;
	}

	/**
	 * @param sheetFactoryRotas the sheetFactoryRotas to set
	 */
	public void setSheetFactoryRotas(SheetFactoryRotas sheetFactoryRotas) {
		this.sheetFactoryRotas = sheetFactoryRotas;
	}	
	
}
