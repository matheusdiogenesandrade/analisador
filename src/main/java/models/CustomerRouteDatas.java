/**
 *
 */
package models;

/**
 * @author matheus
 *
 */
public class CustomerRouteDatas {

    private Expedient servedExpedient;
    private Double currentMileage;
    private Path pathToNextCustomer;
    private Route route;

    public CustomerRouteDatas() {
        this.setPathToNextCustomer(new Path());
    }
       
    /**
     * @return the servedExpedient
     */
    public Expedient getServedExpedient() {
        return servedExpedient;
    }

    /**
     * @param servedExpedient the servedExpedient to set
     */
    public void setServedExpedient(Expedient servedExpedient) {
        if (servedExpedient != null){
            this.servedExpedient = servedExpedient;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro servedExpedient não pode ser nulo");
        }           
    }

    /**
     * @return the currentMileage
     */
    public Double getCurrentMileage() {
        return currentMileage;
    }

    /**
     * @param currentMileage the currentMileage to set
     */
    public void setCurrentMileage(Double currentMileage) {
        if (currentMileage != null && currentMileage >= 0){
            this.currentMileage = currentMileage;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro currentMileage não pode ser nulo e deve ser maior ou igual a zero.");
        }         
    }

    /**
     * @return the pathToNextCustomer
     */
    public Path getPathToNextCustomer() {
        return pathToNextCustomer;
    }

    /**
     * @param pathToNextCustomer the pathToNextCustomer to set
     */
    public void setPathToNextCustomer(Path pathToNextCustomer) {
        if (pathToNextCustomer != null){
            this.pathToNextCustomer = pathToNextCustomer;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro pathToNextCustomer não pode ser nulo.");
        }                 
    }

    /**
     * @return the route
     */
    public Route getRoute() {
        return route;
    }

    /**
     * @param route the route to set
     */
    public void setRoute(Route route) {
        if (route != null){
            this.route = route;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro route não pode ser nulo.");
        }           
    }

}
