/**
 * 
 */
package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import models.Instance.VelocityPerDistance;

/**
 * @author projeto
 *
 */
public class InstanceFactoryGrupos extends InstanceFactory {

	public InstanceFactoryGrupos(JsonObject instanceJsonObject) {
		super(instanceJsonObject, InstanceType.GRUPOS);		
	}
	
	private InstanceFactoryGrupos(JsonObject instanceJsonObject, InstanceType instanceType) {
		super(instanceJsonObject, instanceType);		
	}

	@Override
	public List<Customer> createDepots() {
		Customer depot = new Customer();
		// get params
		JsonObject params = super.getInstanceJsonObject().getAsJsonObject("params");
		JsonObject cd = params.getAsJsonObject("cd");
		depot.setId(cd.getAsJsonPrimitive("id").getAsInt());
		depot.setX(cd.getAsJsonPrimitive("x").getAsDouble());
		depot.setY(cd.getAsJsonPrimitive("y").getAsDouble());
		depot.setServiceTime(0d);
		// getting exped
		JsonObject exped = params.getAsJsonObject("exped");
		Expedient expedient = new Expedient(exped.getAsJsonPrimitive("ini").getAsDouble(),
				exped.getAsJsonPrimitive("fim").getAsDouble());
		depot.setExpedient(expedient);
		return Arrays.asList(depot);
	}

	@Override
	public List<Customer> createCustomers() {
		List<Customer> customers = new ArrayList<Customer>();
		JsonArray clientes = super.getInstanceJsonObject().getAsJsonArray("clientes");
		JsonObject params = super.getInstanceJsonObject().getAsJsonObject("params");
		JsonObject exped = params.getAsJsonObject("exped");
		Expedient expedient = new Expedient(exped.getAsJsonPrimitive("ini").getAsDouble(),
				exped.getAsJsonPrimitive("fim").getAsDouble());
		customers = getCustomersByJSONArray(clientes, expedient);
		return customers;
	}

	@Override
	public List<VelocityPerDistance> createVelocitiesPerDistance() {
		List<Instance.VelocityPerDistance> velocitiesPerDistance = new ArrayList<Instance.VelocityPerDistance>();
		// getting velocities
		JsonArray tbVeloc = super.getInstanceJsonObject().getAsJsonObject("params").getAsJsonObject("veloc")
				.getAsJsonArray("tb");
		for (int i = 0; i < tbVeloc.size(); i++) {
			JsonObject item = tbVeloc.get(i).getAsJsonObject();
			velocitiesPerDistance.add(new Instance.VelocityPerDistance(item.getAsJsonPrimitive("d").getAsDouble(),
					item.getAsJsonPrimitive("vm").getAsDouble()));
		}
		return velocitiesPerDistance;
	}

	@Override
	public List<Vehicle> createVehicles() {
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		// getting vehicles
		JsonArray veiculos = super.getInstanceJsonObject().getAsJsonArray("veiculos");
		for (int i = 0; i < veiculos.size(); i++) {
			JsonObject veiculo = veiculos.get(i).getAsJsonObject();
			vehicles.add(getVehicleByJSONObject(veiculo));
		}
		return vehicles;
	}

}
