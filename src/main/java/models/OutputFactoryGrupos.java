/**
 * 
 */
package models;

import java.util.List;
import com.google.gson.JsonObject;

/**
 * @author projeto
 *
 */
public class OutputFactoryGrupos extends OutputFactory {

	public OutputFactoryGrupos(JsonObject outputJsonObject, Instance instance) {
		super(outputJsonObject, instance);
	}

	@Override
	public List<Route> createRoutes() {		
		return super.getRoutesFromJsonObject(super.getOutputJsonObject().getAsJsonObject("resultado"), super.getInstance());
	}

	@Override
	public List<Customer> createNotServed() {		
		return super.getNotServedFromJsonObject(super.getOutputJsonObject().getAsJsonObject("resultado"), super.getInstance());
	}


	@Override
	public Double createAlgorithmTime() {		
		return super.getOutputJsonObject().getAsJsonObject("resultado").getAsJsonObject("resumo")
				.getAsJsonPrimitive("duracaoCalculo").getAsDouble();
	}

	
	
}
