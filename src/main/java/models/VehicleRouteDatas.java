/**
 *
 */
package models;

import java.util.ArrayList;
import java.util.List;

/**
 * @author matheus
 *
 */
public class VehicleRouteDatas {

    private VehicleRestrictions vehicleRestrictions;
    private List<Route> servedRoutes;
    
    public VehicleRouteDatas() {
        this.setServedRoutes(new ArrayList<Route>());
        this.setVehicleRestrictions(new VehicleRestrictions());
    }

    /* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		VehicleRouteDatas vehicleRouteDatas = new VehicleRouteDatas();
		if (this.getServedRoutes() != null) {
			vehicleRouteDatas.setServedRoutes(new ArrayList<Route>(this.getServedRoutes()));
		}
		if (this.getVehicleRestrictions() != null) {
			vehicleRouteDatas.setVehicleRestrictions((VehicleRestrictions) this.getVehicleRestrictions().clone());
		}
		return vehicleRouteDatas;
	}

	/**
     * @return the visitsMade
     */
    public Integer getVisitsMade() {
        return this.getVehicleRestrictions().getVisitsLimit();
    }

    /**
     * @param visitsMade the visitsMade to set
     */
    public void setVisitsMade(Integer visitsMade) {
        this.getVehicleRestrictions().setVisitsLimit(visitsMade);
    }

    /**
     * @return the cubingFilled
     */
    public Double getCubingFilled() {
        return this.getVehicleRestrictions().getCubingLimit();
    }

    /**
     * @param cubingFilled the cubingFilled to set
     */
    public void setCubingFilled(Double cubingFilled) {
        this.getVehicleRestrictions().setCubingLimit(cubingFilled);
    }

    /**
     * @return the serviceTimeFilled
     */
    public Double getServiceTimeFilled() {
        return this.getVehicleRestrictions().getServiceTimeLimit();
    }

    /**
     * @param serviceTimeFilled the serviceTimeFilled to set
     */
    public void setServiceTimeFilled(Double serviceTimeFilled) {
        this.getVehicleRestrictions().setServiceTimeLimit(serviceTimeFilled);
    }

    /**
     * @return the weightFilled
     */
    public Double getWeightFilled() {
        return this.getVehicleRestrictions().getWeightLimit();
    }

    /**
     * @param weightFilled the weightFilled to set
     */
    public void setWeightFilled(Double weightFilled) {
        this.getVehicleRestrictions().setWeightLimit(weightFilled);
    }

    /**
     * @return the servedRoutes
     */
    public List<Route> getServedRoutes() {
        return servedRoutes;
    }

    /**
     * @param servedRoutes the servedRoutes to set
     */
    public void setServedRoutes(List<Route> servedRoutes) {
        this.servedRoutes = servedRoutes;
    }

    /**
     * @return the vehicleRestrictions
     */
    public VehicleRestrictions getVehicleRestrictions() {
        return vehicleRestrictions;
    }

    /**
     * @param vehicleRestrictions the vehicleRestrictions to set
     */
    public void setVehicleRestrictions(VehicleRestrictions vehicleRestrictions) {
        this.vehicleRestrictions = vehicleRestrictions;
    }

}
