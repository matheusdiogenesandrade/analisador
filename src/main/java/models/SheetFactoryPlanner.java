/**
 * 
 */
package models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JProgressBar;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import utils.WriteExcel;

/**
 * @author projeto
 *
 */
public class SheetFactoryPlanner extends SheetFactory {

	private SheetFactoryRotas sheetFactoryRotas;

	public SheetFactoryPlanner(Instance instance, Result result, String sheetName, String fileName) {
		super(instance, result, sheetName, fileName);
		this.setSheetFactoryRotas(new SheetFactoryRotas(instance, result, sheetName, fileName));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.SheetFactory#createSheet()
	 */
	@Override
	public WriteExcel createSheet(JProgressBar progressBar, int qtdInstance) {
		SheetFactoryRotas sheetFactoryRotas = this.getSheetFactoryRotas();
		sheetFactoryRotas.setWriteExcel(this.getWriteExcel());
		sheetFactoryRotas.setXssfSheet(this.getXssfSheet());
		sheetFactoryRotas.setBoldCellStyle(this.getBoldCellStyle());
		sheetFactoryRotas.setRedColorCellStyle(this.getRedColorCellStyle());
		sheetFactoryRotas.setYellowColorCellStyle(this.getYellowColorCellStyle());
		sheetFactoryRotas.setHeaderColorCellStyle(this.getHeaderColorCellStyle());
		sheetFactoryRotas.setColorCellStyle(this.getColorCellStyle());
		sheetFactoryRotas.fillRoute();
		progressBar.setValue(progressBar.getValue()+5/qtdInstance);
		this.setRowCount(sheetFactoryRotas.getRowCount());
		this.fillVehicles();
		progressBar.setValue(progressBar.getValue()+10/qtdInstance);
		this.fillCustomers();
		progressBar.setValue(progressBar.getValue()+20/qtdInstance);
		sheetFactoryRotas.setRowCount(this.getRowCount());
		sheetFactoryRotas.fillNotServedCustomers();
		progressBar.setValue(progressBar.getValue()+20/qtdInstance);
		sheetFactoryRotas.fillLostedCustomers();
		progressBar.setValue(progressBar.getValue()+10/qtdInstance);
		sheetFactoryRotas.fillInvalidPaths();
		progressBar.setValue(progressBar.getValue()+10/qtdInstance);
		sheetFactoryRotas.fillEuclidianPaths();
		progressBar.setValue(progressBar.getValue()+10/qtdInstance);
		return this.getWriteExcel();
	}

	protected void fillVehicles() {
		System.out.println("HEADER from vehicles");
		// fill vehicles
		// header
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "VEÍCULOS");
		int usedVehiclesNumber = VerifyFacade.getUsedVehiclesNumber(vehicles);
		int unusedVehiclesNumber = vehicles.size() - usedVehiclesNumber;
		int totalWorkedDays = VerifyFacade.calculateTotalWorkedDays(routes, instance.getWorksInSaturday());
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade de veí­culos usados: ");
		cell = row.createCell(4);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) usedVehiclesNumber);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 3));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade de veículos ociosos: ");
		cell = row.createCell(4);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) unusedVehiclesNumber);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 3));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 8));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 11));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 22, 23));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 24, 25));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 26, 27));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 28, 29));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 30, 31));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 32, 33));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Grupo");
		cell = row.createCell(1);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Placa");
		cell = row.createCell(2);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Peso limite");
		cell = row.createCell(4);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Peso ocupado");
		cell = row.createCell(6);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo limite de serviço");
		cell = row.createCell(9);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo serviço ocupado");
		cell = row.createCell(12);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Núm. limite de visitas");
		cell = row.createCell(14);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Visitas feitas");
		cell = row.createCell(16);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Cubagem limite");
		cell = row.createCell(18);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Cubagem ocupada");
		cell = row.createCell(20);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Inicio Almoço");
		cell = row.createCell(22);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Fim Almoço");
		cell = row.createCell(24);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Atraso almoço");
		cell = row.createCell(26);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Antecipação almoço");
		cell = row.createCell(28);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Qntd. dias trabalhados");
		cell = row.createCell(30);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Dias trabalhados");
		cell = row.createCell(32);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Dias não trabalhados");
		System.out.println("CONTENT from vehicles");
		// content
		for (Vehicle vehicle : vehicles) {
			VehicleRouteDatas vehicleRouteDatas = vehicle.getVehicleRouteDatas();
			VehicleRestrictions vehicleRestrictions = vehicle.getVehicleRestrictions();
			Expedient lunchExpedient = vehicleRestrictions.getLunchExpedient();
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 3));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 8));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 11));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 22, 23));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 24, 25));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 26, 27));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 28, 29));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 30, 31));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 32, 33));
			row = xssfSheet.createRow(rowCount++);
			if (vehicleRouteDatas != null) {
				cell = row.createCell(0);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) VerifyFacade.formatRoutesIds(vehicleRouteDatas.getServedRoutes()));
				cell = row.createCell(1);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) vehicle.getBoard());
				cell = row.createCell(2);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getWeightLimit());
				cell = row.createCell(4);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRouteDatas.getWeightFilled());
				cell = row.createCell(6);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getServiceTimeLimit());
				cell = row.createCell(9);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRouteDatas.getServiceTimeFilled());
				cell = row.createCell(12);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) vehicle.getVisitsLimit());
				cell = row.createCell(14);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) vehicleRouteDatas.getVisitsMade());
				cell = row.createCell(16);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getCubingLimit());
				cell = row.createCell(18);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRouteDatas.getCubingFilled());
				if (!VerifyFacade.isValidVehicleFilledCubing(vehicle)) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(20);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) lunchExpedient.getStart());
				cell = row.createCell(22);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) lunchExpedient.getFinish());
				cell = row.createCell(24);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRestrictions.getLunchDelay());
				cell = row.createCell(26);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRestrictions.getLunchAntecipate());
				cell = row.createCell(28);
				cell.setCellStyle(colorCellStyle);				
				List<Route> vehicleRoutes = vehicleRouteDatas.getServedRoutes();
				List<Integer> notWorkedDays = VerifyFacade.getNotWorkedDaysFromVehicle(vehicle, routes);
				int routesSize = vehicleRoutes.size();
				cell.setCellValue((Integer) routesSize);				
				if (routesSize > 0 && notWorkedDays.size() != 0) {
					cell.setCellStyle(redColorCellStyle);
				}				
				cell = row.createCell(30);
				cell.setCellStyle(colorCellStyle);
				String routeIds = "";
				for (Route r : vehicleRouteDatas.getServedRoutes()) {
					routeIds += r.getDay() + ", ";
				}
				cell.setCellValue((String) routeIds);
				cell = row.createCell(32);
				cell.setCellStyle(colorCellStyle);				
				String notWorkedDaysString = "";
				for (Integer day : notWorkedDays) {
					notWorkedDaysString += day + ", ";
				}
				cell.setCellValue((String) notWorkedDaysString);
			} else {
				cell = row.createCell(0);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) "");
				cell = row.createCell(1);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) vehicle.getBoard());
				cell = row.createCell(2);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getWeightLimit());
				cell = row.createCell(4);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) 0d);
				cell = row.createCell(6);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getServiceTimeLimit());
				cell = row.createCell(9);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) 0d);
				cell = row.createCell(12);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) vehicle.getVisitsLimit());
				cell = row.createCell(14);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) 0);
				cell = row.createCell(16);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicle.getCubingLimit());
				cell = row.createCell(18);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) 0d);
				cell = row.createCell(20);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) lunchExpedient.getStart());
				cell = row.createCell(22);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) lunchExpedient.getFinish());
				cell = row.createCell(24);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRestrictions.getLunchDelay());
				cell = row.createCell(26);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) vehicleRestrictions.getLunchAntecipate());
				cell = row.createCell(28);
				cell.setCellStyle(colorCellStyle);				
				cell.setCellValue((Integer) 0);
				cell = row.createCell(30);
				cell.setCellStyle(colorCellStyle);				
				cell.setCellValue((String) "");
				cell = row.createCell(32);
				cell.setCellStyle(colorCellStyle);				
				cell.setCellValue((String) "");
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 2, 3));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 8));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 9, 11));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 22, 23));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 24, 25));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 26, 27));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 28, 29));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 30, 31));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 32, 33));
		row = xssfSheet.createRow(rowCount++);
	}

	protected void fillCustomers() {
		System.out.println("HEADER from served customers");
		// fill served customers
		// header
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, Constants.MAX_COLUMNS_SHEET_SIZE));
		Row row = xssfSheet.createRow(rowCount++);
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "CLIENTES ATENDIDOS");
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 0, 1));
		row = xssfSheet.createRow(rowCount++);
		Integer servedCustomersNumber = 0;
		for (Route route : routes) {
			servedCustomersNumber += route.getCustomers().size();
		}
		cell = row.createCell(0);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((String) "Quantidade: ");
		cell = row.createCell(2);
		cell.setCellStyle(boldCellStyle);
		cell.setCellValue((Integer) servedCustomersNumber);
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 7));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 11));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 24, 25));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 26, 27));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 28, 29));
		row = xssfSheet.createRow(rowCount++);
		cell = row.createCell(0);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID Grupo");
		cell = row.createCell(1);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "ID");
		cell = row.createCell(2);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "X");
		cell = row.createCell(3);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Y");
		cell = row.createCell(4);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Demanda de peso");
		cell = row.createCell(6);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Tempo de serviço");
		cell = row.createCell(8);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Núm. visitas limite");
		cell = row.createCell(10);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Kilometragem atual");
		cell = row.createCell(12);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Expediente de início");
		cell = row.createCell(14);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Início atendimento");
		cell = row.createCell(16);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Expediente de fim");
		cell = row.createCell(18);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Fim atendimento");
		cell = row.createCell(20);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Placa veículo");
		cell = row.createCell(22);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Dia");
		cell = row.createCell(23);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Semana");
		cell = row.createCell(24);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Semanas possíveis");
		cell = row.createCell(26);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Dias obrigatórios");
		cell = row.createCell(28);
		cell.setCellStyle(headerColorCellStyle);
		cell.setCellValue((String) "Frequência de visita");
		System.out.println("CONTENT from served customers");
		// content
		for (int i = 0; i < routes.size(); i++) {
			Route route = routes.get(i);
			Vehicle routeVehicle = route.getVehicle();
			// set customers route
			List<Customer> customers = route.getCustomers();
			servedCustomersNumber += customers.size();
			// depot
			Customer depot = route.getDepot();
			CustomerRouteDatas depotRouteDatas = depot.getCustomerRouteDatas();
			Expedient depotExpedient = depot.getExpedient();
			Expedient depotRouteExpedient = depotRouteDatas.getServedExpedient();
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 7));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 11));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 24, 25));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 26, 27));
			xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 28, 29));
			row = xssfSheet.createRow(rowCount++);
			cell = row.createCell(0);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) route.getId());
			cell = row.createCell(1);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) depot.getId());
			cell = row.createCell(2);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depot.getX());
			cell = row.createCell(3);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depot.getY());
			cell = row.createCell(4);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue(depot.getWeight() == null ? 0 : (Double) depot.getWeight());
			cell = row.createCell(6);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue(depot.getServiceTime() == null ? 0 : (Double) depot.getServiceTime());
			cell = row.createCell(8);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue(depot.getVisitsLimit() == null ? 0 : (Integer) depot.getVisitsLimit());
			cell = row.createCell(10);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotRouteDatas.getCurrentMileage());
			cell = row.createCell(12);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotExpedient.getStart());
			cell = row.createCell(14);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotRouteExpedient.getStart());
			if (VerifyFacade.isStartExpedientBroken(depotExpedient.getStart(), depotRouteExpedient.getStart())) {
				cell.setCellStyle(redColorCellStyle);
			}
			cell = row.createCell(16);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotExpedient.getFinish());
			cell = row.createCell(18);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Double) depotRouteExpedient.getFinish());
			if (VerifyFacade.isFinishExpedientBroken(depotExpedient.getFinish(), depotRouteExpedient.getFinish())) {
				cell.setCellStyle(redColorCellStyle);
			}
			cell = row.createCell(20);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((String) routeVehicle.getBoard());
			cell = row.createCell(22);
			cell.setCellStyle(colorCellStyle);
			String routeDay = route.getDay().toString() + " - ";
			Integer routeDayNumber = VerifyFacade.getWeekDayByRouteDay(route.getDay());
			if (routeDayNumber.equals(WeekDay.MONDAY.getWeekDay())) {
				routeDay += "Seg";
			} else if (routeDayNumber.equals(WeekDay.TUESDAY.getWeekDay())) {
				routeDay += "Ter";
			} else if (routeDayNumber.equals(WeekDay.WEDNESDAY.getWeekDay())) {
				routeDay += "Qua";
			} else if (routeDayNumber.equals(WeekDay.THURSDAY.getWeekDay())) {
				routeDay += "Qui";
			} else if (routeDayNumber.equals(WeekDay.FRIDAY.getWeekDay())) {
				routeDay += "Sex";
			} else if (routeDayNumber.equals(WeekDay.SATURDAY.getWeekDay())) {
				routeDay += "Sab";
			}
			cell.setCellValue((String) routeDay);
			cell = row.createCell(23);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((Integer) VerifyFacade.getWeekByRouteDay(route.getDay()));
			cell = row.createCell(24);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((String) "");
			cell = row.createCell(26);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((String) "");
			cell = row.createCell(28);
			cell.setCellStyle(colorCellStyle);
			cell.setCellValue((String) "");
			double [][] lunchInterval = VerifyFacade.getLunchInterval(routeVehicle, customers);
			VehicleRestrictions vehicleRestrictions = routeVehicle.getVehicleRestrictions();
			int quantityLunchs = (int) Math.ceil(vehicleRestrictions.getExpedient().getFinish()/24); 
			boolean[] respectedLunch = VerifyFacade.getRespectedLunch(routeVehicle, customers, lunchInterval);
			// anothers customers
			for (int j = 0; j < customers.size(); j++) {
				Customer customer = customers.get(j);
				CustomerRouteDatas customerRouteDatas = customer.getCustomerRouteDatas();
				Expedient expedient = customer.getExpedient();
				Expedient routeExpedient = customerRouteDatas.getServedExpedient();
				// if customer is repeated in route
				Boolean isCustomerRepeatedRoute = false;
				for (int k = j - 1; k >= 0; k--) {
					if (customers.get(k).getId().equals(customer.getId())) {
						isCustomerRepeatedRoute = true;
						k = 0;
						break;
					}
				}
				// alerts
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 7));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 11));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 24, 25));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 26, 27));
				xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 28, 29));
				row = xssfSheet.createRow(rowCount++);
				cell = row.createCell(0);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) route.getId());
				cell = row.createCell(1);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) customer.getId());
				if (isCustomerRepeatedRoute) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(2);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customer.getX());
				cell = row.createCell(3);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customer.getY());
				cell = row.createCell(4);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue(customer.getWeight() == null ? 0 : (Double) customer.getWeight());
				cell = row.createCell(6);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue(customer.getServiceTime() == null ? 0 : (Double) customer.getServiceTime());
				cell = row.createCell(8);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue(customer.getVisitsLimit() == null ? 0 : (Integer) customer.getVisitsLimit());
				cell = row.createCell(10);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) customerRouteDatas.getCurrentMileage());
				cell = row.createCell(12);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) expedient.getStart());
				cell = row.createCell(14);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) routeExpedient.getStart());
				Expedient expedientCustomer = customer.getCustomerRouteDatas().getServedExpedient();
				if(VerifyFacade.isStartNotRespectedLunch(expedientCustomer, vehicleRestrictions, quantityLunchs, respectedLunch)) {
					cell.setCellStyle(redColorCellStyle);
				}
				if (VerifyFacade.isStartExpedientBroken(expedient.getStart(), routeExpedient.getStart())) {
					cell.setCellStyle(redColorCellStyle);
				}
				if (j == 0 && !instance.getStartInDepot() && routeExpedient.getStart() != expedient.getStart()) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(16);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) expedient.getFinish());
				cell = row.createCell(18);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Double) routeExpedient.getFinish());
				if(VerifyFacade.isFinishNotRespectedLunch(expedientCustomer, vehicleRestrictions, quantityLunchs, respectedLunch)) {
					cell.setCellStyle(redColorCellStyle);
				}
				if (VerifyFacade.isFinishExpedientBroken(expedient.getFinish(), routeExpedient.getFinish())) {
					cell.setCellStyle(yellowColorCellStyle);
				}
				if (!instance.getEndsInDepot() && j == customers.size() - 1 && routeExpedient.getFinish() != depotRouteExpedient.getFinish()) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(20);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) routeVehicle.getBoard());
				cell = row.createCell(22);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) routeDay);
				cell = row.createCell(23);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((Integer) VerifyFacade.getWeekByRouteDay(route.getDay()));
				// dias da semana
				Boolean[] possibleVisitWeeks = customer.getPossibleVisitWeeks();
				List<Integer> possibleWeeks = new ArrayList<Integer>();
				for (int k = 0; k < possibleVisitWeeks.length; k++) {
					if (possibleVisitWeeks[k]) {
						possibleWeeks.add(k + 1);
					}
				}
				cell = row.createCell(24);
				cell.setCellStyle(colorCellStyle);				
				String possibleWeeksString = "";
				for (int k = 0; k < possibleWeeks.size(); k++) {
					possibleWeeksString += possibleWeeks.get(k) + ", ";
				}								
				cell.setCellValue((String) possibleWeeksString);
				// verification
				Integer week = (route.getDay() / 7) + 1;
				if (!possibleWeeks.contains(week)) {
					cell.setCellStyle(redColorCellStyle);
				}
				// dias obrigatorios
				WeekDay[] obrigatoryWeekDays = customer.getRequiredWeekDays();
				String obrigatoryWeekDaysString = "";
				Boolean invalidVisitedDay = false;
				for (int k = 0; k < obrigatoryWeekDays.length; k++) {
					if (obrigatoryWeekDays[k].equals(WeekDay.MONDAY)) {
						if ((route.getDay() % 7) != WeekDay.MONDAY.getWeekDay()) {
							invalidVisitedDay = true;
						}
						obrigatoryWeekDaysString += "Seg, ";
					} else if (obrigatoryWeekDays[k].equals(WeekDay.TUESDAY)) {
						if ((route.getDay() % 7) != WeekDay.TUESDAY.getWeekDay()) {
							invalidVisitedDay = true;
						}
						obrigatoryWeekDaysString += "Ter, ";
					} else if (obrigatoryWeekDays[k].equals(WeekDay.WEDNESDAY)) {
						if ((route.getDay() % 7) != WeekDay.WEDNESDAY.getWeekDay()) {
							invalidVisitedDay = true;
						}
						obrigatoryWeekDaysString += "Qua, ";
					} else if (obrigatoryWeekDays[k].equals(WeekDay.THURSDAY)) {
						if ((route.getDay() % 7) != WeekDay.THURSDAY.getWeekDay()) {
							invalidVisitedDay = true;
						}
						obrigatoryWeekDaysString += "Qui, ";
					} else if (obrigatoryWeekDays[k].equals(WeekDay.FRIDAY)) {
						if ((route.getDay() % 7) != WeekDay.FRIDAY.getWeekDay()) {
							invalidVisitedDay = true;
						}
						obrigatoryWeekDaysString += "Sex, ";
					} else if (obrigatoryWeekDays[k].equals(WeekDay.SATURDAY)) {
						if ((route.getDay() % 7) != WeekDay.SATURDAY.getWeekDay()) {
							invalidVisitedDay = true;
						}
						obrigatoryWeekDaysString += "Sab, ";
					}
				}
				cell = row.createCell(26);
				cell.setCellStyle(colorCellStyle);
				cell.setCellValue((String) obrigatoryWeekDaysString);
				if (invalidVisitedDay) {
					cell.setCellStyle(redColorCellStyle);
				}
				cell = row.createCell(28);
				cell.setCellStyle(colorCellStyle);
				if (customer.getVisitFrequency().equals(VisitFrequency.DAILY)) {
					cell.setCellValue((String) "Diariamente");
					for (int k = 0; k < routes.size(); k++) {
						List<Customer> routeCustomers = routes.get(k).getCustomers();
						Boolean customerFinded = false;
						for (int l = 0; l < routeCustomers.size(); l++) {
							if (customer.getId() == routeCustomers.get(l).getId()) {
								customerFinded = true;
								break;
							}
						}
						if (!customerFinded) {
							cell.setCellStyle(redColorCellStyle);
						}
					}
				} else if (customer.getVisitFrequency().equals(VisitFrequency.TWO_TIMES_PER_WEEK)) {
					Integer[] invalidWeeks = VerifyFacade.weeksNotVisitedByCustomerNTimes(routes, customer, 2);
					if (invalidWeeks.length > 0) {
						String cellString = "Bipasse (";
						for (int k = 0; k < invalidWeeks.length; cellString += invalidWeeks[k++] + ", ")
							;
						cell.setCellValue((String) cellString + ")");
						cell.setCellStyle(redColorCellStyle);
					} else {
						cell.setCellValue((String) "Bipasse");
					}
				} else if (customer.getVisitFrequency().equals(VisitFrequency.WEEKLY)) {
					Integer[] invalidWeeks = VerifyFacade.weeksNotVisitedByCustomerNTimes(routes, customer, 1);
					if (invalidWeeks.length > 0) {
						String cellString = "Semanal (";
						for (int k = 0; k < invalidWeeks.length; cellString += invalidWeeks[k++] + ", ")
							;
						cell.setCellValue((String) cellString + ")");
						cell.setCellStyle(redColorCellStyle);
					} else {
						cell.setCellValue((String) "Semanal");
					}
				} else if (customer.getVisitFrequency().equals(VisitFrequency.BIWEEKLY)) {
					List<Integer[]> invalidWeeksPair = new ArrayList<Integer[]>();
					Integer weekNumber = 1;
					do {
						// get routes from weekNumber
						List<Route> weekRoute = VerifyFacade.getRouteByWeekNumber(routes, weekNumber);
						weekRoute.addAll(VerifyFacade.getRouteByWeekNumber(routes, weekNumber + 1));
						// if there is routes from weekNumber
						if (weekRoute.size() > 0) {
							// if the customer is visited in weekRoutes
							Integer visits = 0;
							for (int k = 0; k < weekRoute.size(); k++) {
								List<Customer> routeCustomers = weekRoute.get(k).getCustomers();
								for (int l = 0; l < routeCustomers.size(); l++) {
									if (customer.getId() == routeCustomers.get(l).getId()) {
										visits++;
										break;
									}
								}
							}
							if (visits != 1) {
								invalidWeeksPair.add(new Integer[] { weekNumber, weekNumber + 1 });
							}
							weekNumber += 2;
						} else {
							break;
						}
					} while (true);
					// set txt
					if (invalidWeeksPair.size() > 0) {
						String cellString = "Quinzenal (";
						for (int k = 0; k < invalidWeeksPair.size(); k++) {
							Integer[] pair = invalidWeeksPair.get(k);
							cellString += "[" + pair[0] + ", " + pair[1] + "], ";
						}
						cell.setCellValue((String) cellString + ")");
						cell.setCellStyle(redColorCellStyle);
					} else {
						cell.setCellValue((String) "Quinzenal");
					}
				} else if (customer.getVisitFrequency().equals(VisitFrequency.MONTHLY)) {
					List<Integer> invalidMonths = new ArrayList<Integer>();
					Integer monthNumber = 1;
					do {
						// get routes from monthNumber
						List<Route> monthRoute = VerifyFacade.getRouteByMonthNumber(routes, monthNumber);
						// if there is routes from monthNumber
						if (monthRoute.size() > 0) {
							// if the customer is visited in monthRoutes
							Integer visits = 0;
							for (int k = 0; k < monthRoute.size(); k++) {
								List<Customer> routeCustomers = monthRoute.get(k).getCustomers();
								for (int l = 0; l < routeCustomers.size(); l++) {
									if (customer.getId() == routeCustomers.get(l).getId()) {
										visits++;
										break;
									}
								}
							}
							if (visits != 1) {
								invalidMonths.add(monthNumber);
							}
							monthNumber += 1;
						} else {
							break;
						}
					} while (true);
					// set txt
					if (invalidMonths.size() > 0) {
						String cellString = "Mensal (";
						for (int k = 0; k < invalidMonths.size(); k++) {
							cellString += invalidMonths.get(k) + ", ";
						}
						cell.setCellValue((String) cellString + ")");
						cell.setCellStyle(redColorCellStyle);
					} else {
						cell.setCellValue((String) "Mensal");
					}
				}
			}
		}
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 4, 5));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 6, 7));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 8, 9));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 10, 11));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 12, 13));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 14, 15));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 16, 17));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 18, 19));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 20, 21));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 24, 25));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 26, 27));
		xssfSheet.addMergedRegion(new CellRangeAddress(rowCount, rowCount, 28, 29));
		row = xssfSheet.createRow(rowCount++);
	}
	

	/**
	 * @return the sheetFactoryRotas
	 */
	public SheetFactoryRotas getSheetFactoryRotas() {
		return sheetFactoryRotas;
	}

	/**
	 * @param sheetFactoryRotas
	 *            the sheetFactoryRotas to set
	 */
	public void setSheetFactoryRotas(SheetFactoryRotas sheetFactoryRotas) {
		this.sheetFactoryRotas = sheetFactoryRotas;
	}

}
