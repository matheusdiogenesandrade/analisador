/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author matheus
 */
public class VehicleRestrictions {

    private Integer visitsLimit;
    private Double cubingLimit;
    private Double serviceTimeLimit;
    private Double weightLimit;
    private Expedient expedient;
    private Expedient lunchExpedient;
    private Double lunchDelay;
    private Double lunchAntecipate;
    
    /* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		VehicleRestrictions vehicleRestrictions = new VehicleRestrictions();
		if (this.getVisitsLimit() != null) {
			vehicleRestrictions.setVisitsLimit(this.getVisitsLimit());
		}
		if (this.getCubingLimit() != null) {
			vehicleRestrictions.setCubingLimit(this.getCubingLimit());
		}
		if (this.getServiceTimeLimit() != null) {
			vehicleRestrictions.setServiceTimeLimit(this.getServiceTimeLimit());
		}
		if (this.getWeightLimit() != null) {
			vehicleRestrictions.setWeightLimit(this.getWeightLimit());
		}
		if (this.getLunchDelay() != null) {
			vehicleRestrictions.setLunchDelay(this.getLunchDelay());
		}
		if (this.getLunchAntecipate() != null) {
			vehicleRestrictions.setLunchAntecipate(this.getLunchAntecipate());
		}
		if (this.getExpedient() != null) {
			vehicleRestrictions.setExpedient((Expedient) this.getExpedient().clone());
		}
		if (this.getLunchExpedient() != null) {
			vehicleRestrictions.setLunchExpedient((Expedient) this.getLunchExpedient().clone());
		}		
		return vehicleRestrictions; 
	}

	/**
     * @return the visitsLimit
     */
    public Integer getVisitsLimit() {
        return visitsLimit;
    }

    /**
     * @param visitsLimit the visitsLimit to set
     */
    public void setVisitsLimit(Integer visitsLimit) {
        if (visitsLimit != null && visitsLimit > 0) {
            this.visitsLimit = visitsLimit;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro visitsLimit deve ser diferente de nulo e maior que zero.");
        }
    }

    /**
     * @return the cubingLimit
     */
    public Double getCubingLimit() {
        return cubingLimit;
    }

    /**
     * @param cubingLimit the cubingLimit to set
     */
    public void setCubingLimit(Double cubingLimit) {
        if (cubingLimit != null && cubingLimit >= 0) {
            this.cubingLimit = cubingLimit;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro cubingLimit deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

    /**
     * @return the serviceTimeLimit
     */
    public Double getServiceTimeLimit() {
        return serviceTimeLimit;
    }

    /**
     * @param serviceTimeLimit the serviceTimeLimit to set
     */
    public void setServiceTimeLimit(Double serviceTimeLimit) {
        if (serviceTimeLimit != null && serviceTimeLimit > 0) {
            this.serviceTimeLimit = serviceTimeLimit;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro serviceTimeLimit deve ser diferente de nulo e maior que zero.");
        }
    }

    /**
     * @return the weightLimit
     */
    public Double getWeightLimit() {
        return weightLimit;
    }

    /**
     * @param weightLimit the weightLimit to set
     */
    public void setWeightLimit(Double weightLimit) {
        if (weightLimit != null && weightLimit >= 0) {
            this.weightLimit = weightLimit;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro weightLimit deve ser diferente de nulo e maior ou igual a zero.");
        }
    }

	/**
	 * @return the expedient
	 */
	public Expedient getExpedient() {
		return expedient;
	}

	/**
	 * @param expedient the expedient to set
	 */
	public void setExpedient(Expedient expedient) {		
		if (expedient != null) {
			this.expedient = expedient;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro expedient deve ser diferente de nulo.");
        }
	}

	/**
	 * @return the lunchExpedient
	 */
	public Expedient getLunchExpedient() {
		return lunchExpedient;
	}

	/**
	 * @param lunchExpedient the lunchExpedient to set
	 */
	public void setLunchExpedient(Expedient lunchExpedient) {
		if (lunchExpedient != null) {
			this.lunchExpedient = lunchExpedient;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro lunchExpedient deve ser diferente de nulo.");
        }		
	}

	/**
	 * @return the lunchDelay
	 */
	public Double getLunchDelay() {
		return lunchDelay;
	}

	/**
	 * @param lunchDelay the lunchDelay to set
	 */
	public void setLunchDelay(Double lunchDelay) {
		if (lunchDelay != null) {
			this.lunchDelay = lunchDelay;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro lunchDelay deve ser diferente de nulo.");
        }	
	}

	/**
	 * @return the lunchAntecipate
	 */
	public Double getLunchAntecipate() {
		return lunchAntecipate;
	}

	/**
	 * @param lunchAntecipate the lunchAntecipate to set
	 */
	public void setLunchAntecipate(Double lunchAntecipate) {
		if (lunchAntecipate != null) {
			this.lunchAntecipate = lunchAntecipate;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro lunchAntecipate deve ser diferente de nulo.");
        }
	}
	
}
