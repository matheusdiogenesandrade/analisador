/**
 * 
 */
package models;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * @author projeto
 *
 */
public class OutputFactoryPlanner extends OutputFactory {

	public OutputFactoryPlanner(JsonObject outputJsonObject, Instance instance) {
		super(outputJsonObject, instance);
	}

	@Override
	public List<Route> createRoutes() {
		Instance instance = super.getInstance();
		List<Vehicle> instanceVehicles = instance.getVehicles();
		List<Customer> instanceDepots = instance.getDepots();
		List<Customer> instanceCustomers = instance.getCustomers();
		// getting routes
		JsonArray grupos = super.getOutputJsonObject().getAsJsonArray("grupos");
		List<Route> routes = new ArrayList<Route>(grupos.size());
		for (int i = 0; i < grupos.size(); i++) {
			JsonObject grupo = grupos.get(i).getAsJsonObject();
			Route route = new Route();
			//getting route day
			JsonObject resumo = grupo.getAsJsonObject("resumo");
			route.setDay(resumo.getAsJsonPrimitive("dia").getAsInt());
			// getting vehicle
			JsonObject veiculo = grupo.getAsJsonObject("veiculo");
			// search vehicle in instance
			String board = veiculo.getAsJsonPrimitive("id").getAsString();
			Boolean find = false;
			for (Vehicle instanceVehicle : instanceVehicles) {
				if (board.equals(instanceVehicle.getBoard())) {					
					JsonArray totais = grupo.getAsJsonArray("totais");
					// vehicle already used
					VehicleRouteDatas vehicleRouteDatas;
					if (instanceVehicle.getVehicleRouteDatas() != null) {
						vehicleRouteDatas = instanceVehicle.getVehicleRouteDatas();
						vehicleRouteDatas
								.setWeightFilled(vehicleRouteDatas.getWeightFilled() + totais.get(0).getAsDouble());
						vehicleRouteDatas.setServiceTimeFilled(
								vehicleRouteDatas.getServiceTimeFilled() + totais.get(1).getAsDouble());
						vehicleRouteDatas.setVisitsMade(vehicleRouteDatas.getVisitsMade() + totais.get(2).getAsInt());
						vehicleRouteDatas
								.setCubingFilled(vehicleRouteDatas.getCubingFilled() + totais.get(3).getAsDouble());
					} else {
						vehicleRouteDatas = new VehicleRouteDatas();
						vehicleRouteDatas.setWeightFilled(totais.get(0).getAsDouble());
						vehicleRouteDatas.setServiceTimeFilled(totais.get(1).getAsDouble());
						vehicleRouteDatas.setVisitsMade(totais.get(2).getAsInt());
						vehicleRouteDatas.setCubingFilled(totais.get(3).getAsDouble());
					}
					vehicleRouteDatas.getServedRoutes().add(route);
					instanceVehicle.setVehicleRouteDatas(vehicleRouteDatas);
					// copy new vehicle
					Vehicle routeVehicle = null;
					try {
						routeVehicle = (Vehicle) instanceVehicle.clone();
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					}
					VehicleRouteDatas vehicleRouteDatasRoute = new VehicleRouteDatas();
					vehicleRouteDatasRoute.setWeightFilled(totais.get(0).getAsDouble());
					vehicleRouteDatasRoute.setServiceTimeFilled(totais.get(1).getAsDouble());
					vehicleRouteDatasRoute.setVisitsMade(totais.get(2).getAsInt());
					vehicleRouteDatasRoute.setCubingFilled(totais.get(3).getAsDouble());
					routeVehicle.setVehicleRouteDatas(vehicleRouteDatasRoute);
					route.setVehicle(routeVehicle);
					find = true;
					break;
				}
			}
			// if vehicle in route does not exists in instance
			if (!find) {
				JsonArray totais = grupo.getAsJsonArray("totais");
				JsonArray limites = veiculo.getAsJsonArray("limites");
				// getting vehicle
				Vehicle vehicle = new Vehicle();
				vehicle.setBoard(board);
				vehicle.setWeightLimit(limites.get(0).getAsDouble());
				vehicle.setServiceTimeLimit(limites.get(1).getAsDouble());
				vehicle.setVisitsLimit(limites.get(2).getAsInt());
				vehicle.setCubingLimit(limites.get(3).getAsDouble());
				// getting vehicle route datas
				VehicleRouteDatas vehicleRouteDatas = new VehicleRouteDatas();
				vehicleRouteDatas.setWeightFilled(totais.get(0).getAsDouble());
				vehicleRouteDatas.setServiceTimeFilled(totais.get(1).getAsDouble());
				vehicleRouteDatas.setVisitsMade(totais.get(2).getAsInt());
				vehicleRouteDatas.setCubingFilled(totais.get(3).getAsDouble());

				vehicle.setVehicleRouteDatas(vehicleRouteDatas);
				route.setVehicle(vehicle);
			}
			// getting customers
			JsonArray clientes = grupo.getAsJsonArray("clientes");
			// getting depot
			JsonObject cd = clientes.get(0).getAsJsonObject();
			Integer depotId = cd.getAsJsonPrimitive("id").getAsInt();			
			Customer depot = null;
			// searching depot in instance
			try {
				Customer findedDepot = null;
				for (Customer instanceDepot : instanceDepots) {
					if (depotId.equals(instanceDepot.getId())) {
						findedDepot = instanceDepot;
						break;
					}
				}
				if (findedDepot != null) {
					depot = (Customer) findedDepot.clone();
				} else {
					for (Customer instanceCustomer : instanceCustomers) {
						if (depotId.equals(instanceCustomer.getId())) {
							depot = (Customer) instanceCustomer.clone();
						}
					}
				}
			} catch (CloneNotSupportedException ex) {
				Logger.getLogger(VerifyFacade.class.getName()).log(Level.SEVERE, null, ex);
			}
			JsonObject jtrCd = cd.getAsJsonObject("jtr");
			JsonObject trajetoCd = cd.getAsJsonObject("trajeto");
			JsonArray locaisCd = trajetoCd.getAsJsonArray("locais");
			// getting route datas from depot
			CustomerRouteDatas depotCustomerRouteDatas = new CustomerRouteDatas();
			depotCustomerRouteDatas.setCurrentMileage(cd.getAsJsonPrimitive("km").getAsDouble());
			depotCustomerRouteDatas.setServedExpedient(new Expedient(jtrCd.getAsJsonPrimitive("ini").getAsDouble(),
					jtrCd.getAsJsonPrimitive("fim").getAsDouble()));
			depotCustomerRouteDatas.setRoute(route);
			Path depotPathToNextCustomer = depotCustomerRouteDatas.getPathToNextCustomer();
			depotPathToNextCustomer.setExitId(trajetoCd.getAsJsonPrimitive("id0").getAsInt());
			depotPathToNextCustomer.setArrivalId(trajetoCd.getAsJsonPrimitive("id1").getAsInt());
			depotPathToNextCustomer.setExitCoordinates(new Coordinates(trajetoCd.getAsJsonPrimitive("x0").getAsDouble(),
					trajetoCd.getAsJsonPrimitive("y0").getAsDouble()));
			depotPathToNextCustomer
					.setArrivalCoordinates(new Coordinates(trajetoCd.getAsJsonPrimitive("x1").getAsDouble(),
							trajetoCd.getAsJsonPrimitive("y1").getAsDouble()));
			for (int j = 0; j < locaisCd.size(); j++) {
				JsonObject localCd = locaisCd.get(j).getAsJsonObject();
				depotPathToNextCustomer.getLocals().add(new Coordinates(localCd.getAsJsonPrimitive("x").getAsDouble(),
						localCd.getAsJsonPrimitive("y").getAsDouble()));
			}
			depot.setCustomerRouteDatas(depotCustomerRouteDatas);
			route.setDepot(depot);
			// getting route datas from anothers customers
			for (int j = 1; j < clientes.size(); j++) {
				JsonObject cliente = clientes.get(j).getAsJsonObject();
				Customer customer = null;
				Integer customerId = Integer.valueOf(cliente.getAsJsonPrimitive("id").getAsString());
				// searching customer in instance
				try {
//					Customer findedDepot = null;
//					for (Customer instanceDepot : instanceDepots) {
//						if (depotId.equals(instanceDepot.getId())) {
//							findedDepot = instanceDepot;
//							break;
//						}
//					}
//					if (findedDepot != null) {
//						depot = (Customer) findedDepot.clone();
//					} else {
						for (Customer instanceCustomer : instanceCustomers) {
							if (customerId.equals(instanceCustomer.getId())) {
								customer = (Customer) instanceCustomer.clone();
							}
						}
//					}
				} catch (CloneNotSupportedException ex) {
					Logger.getLogger(VerifyFacade.class.getName()).log(Level.SEVERE, null, ex);
				}
				JsonObject jtr = cliente.getAsJsonObject("jtr");
				JsonObject trajeto = cliente.getAsJsonObject("trajeto");
				JsonArray locais = trajeto.getAsJsonArray("locais");
				// getting route datas from customer
				CustomerRouteDatas customerRouteDatas = new CustomerRouteDatas();
				customerRouteDatas.setCurrentMileage(cliente.getAsJsonPrimitive("km").getAsDouble());
				customerRouteDatas.setServedExpedient(new Expedient(jtr.getAsJsonPrimitive("ini").getAsDouble(),
						jtr.getAsJsonPrimitive("fim").getAsDouble()));
				customerRouteDatas.setRoute(route);
				Path pathToNextCustomer = customerRouteDatas.getPathToNextCustomer();
				pathToNextCustomer.setExitId(trajeto.getAsJsonPrimitive("id0").getAsInt());
				pathToNextCustomer.setArrivalId(trajeto.getAsJsonPrimitive("id1").getAsInt());
				pathToNextCustomer.setExitCoordinates(new Coordinates(trajeto.getAsJsonPrimitive("x0").getAsDouble(),
						trajeto.getAsJsonPrimitive("y0").getAsDouble()));
				pathToNextCustomer.setArrivalCoordinates(new Coordinates(trajeto.getAsJsonPrimitive("x1").getAsDouble(),
						trajeto.getAsJsonPrimitive("y1").getAsDouble()));
				for (int k = 0; k < locais.size(); k++) {
					JsonObject local = locais.get(k).getAsJsonObject();
					pathToNextCustomer.getLocals().add(new Coordinates(local.getAsJsonPrimitive("x").getAsDouble(),
							local.getAsJsonPrimitive("y").getAsDouble()));
				}
				customer.setCustomerRouteDatas(customerRouteDatas);
				route.getCustomers().add(customer);
			}
			routes.add(route);
		}
		//sort routes by vehicle
		for (int i = 0; i < routes.size(); i++) {
			for (int j = i + 1; j < routes.size(); j++) {
				if (routes.get(i).getDay() > routes.get(j).getDay()) {
					routes.set(i, routes.set(j, routes.get(i)));
				}
			}
		}		
		//set ids
		for (int i = 0; i < routes.size(); i++) {
			routes.get(i).setId(i);
		}
		return routes;
	}

	@Override
	public List<Customer> createNotServed() {
		return super.getNotServedFromJsonObject(super.getOutputJsonObject(), super.getInstance());
	}

	@Override
	public Double createAlgorithmTime() {		
		return Double.NaN;
	}

}
