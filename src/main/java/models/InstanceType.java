/**
 * 
 */
package models;

/**
 * @author projeto
 *
 */
public enum InstanceType {
	GRUPOS(1), ROTAS(2), PLANNER(3), UNKNOWN(4);
	
	private int type;	
	
	/**
	 * @param type
	 */
	private InstanceType(int type) {
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	
}
