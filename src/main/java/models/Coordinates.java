/**
 *
 */
package models;

/**
 * @author matheus
 *
 */
public class Coordinates {

    private Double x;
    private Double y;

    /**
     *
     */
    public Coordinates() {
        super();
    }

    /**
     * @param x
     * @param y
     */
    public Coordinates(Double x, Double y) {
        super();
        this.x = x;
        this.y = y;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Coordinates(this.getX(), this.getY());
    }

    /**
     * @return the x
     */
    public Double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(Double x) {
        if (x != null) {
            this.x = x;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro x deve ser diferente de nulo.");
        }
    }

    /**
     * @return the y
     */
    public Double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(Double y) {
        if (y != null) {
            this.y = y;
        } else {
            throw new IllegalArgumentException("Erro: O paramêtro y deve ser diferente de nulo.");
        }
    }

}
