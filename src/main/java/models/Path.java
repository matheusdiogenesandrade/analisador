/**
 *
 */
package models;

import java.util.ArrayList;
import java.util.List;

/**
 * @author matheus
 *
 */
public class Path {

    private Integer exitId;
    private Integer arrivalId;
    private Coordinates exitCoordinates;
    private Coordinates arrivalCoordinates;
    private List<Coordinates> locals;

    public Path() {
        this.setLocals(new ArrayList<Coordinates>());
    }
    
    /**
     * @return the exitId
     */
    public Integer getExitId() {
        return exitId;
    }

    /**
     * @param exitId the exitId to set
     */
    public void setExitId(Integer exitId) {
        if (exitId != null){
            this.exitId = exitId;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro exitId não pode ser nulo.");
        }                   
    }

    /**
     * @return the arrivalId
     */
    public Integer getArrivalId() {
        return arrivalId;
    }

    /**
     * @param arrivalId the arrivalId to set
     */
    public void setArrivalId(Integer arrivalId) {        
        if (arrivalId != null){
            this.arrivalId = arrivalId;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro arrivalId não pode ser nulo.");
        }                   
    }

    /**
     * @return the exitCoordinates
     */
    public Coordinates getExitCoordinates() {
        return exitCoordinates;
    }

    /**
     * @param exitCoordinates the exitCoordinates to set
     */
    public void setExitCoordinates(Coordinates exitCoordinates) {
        if (exitCoordinates != null){
            this.exitCoordinates = exitCoordinates;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro exitCoordinates não pode ser nulo.");
        }                           
    }

    /**
     * @return the arrivalCoordinates
     */
    public Coordinates getArrivalCoordinates() {
        return arrivalCoordinates;
    }

    /**
     * @param arrivalCoordinates the arrivalCoordinates to set
     */
    public void setArrivalCoordinates(Coordinates arrivalCoordinates) {        
        if (arrivalCoordinates != null){
            this.arrivalCoordinates = arrivalCoordinates;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro arrivalCoordinates não pode ser nulo.");
        }                           
    }

    /**
     * @return the locals
     */
    public List<Coordinates> getLocals() {
        return locals;
    }

    /**
     * @param locals the locals to set
     */
    public void setLocals(List<Coordinates> locals) {
        if (locals != null){
            this.locals = locals;
        }else{
            throw new IllegalArgumentException("Erro: O parâmetro locals não pode ser nulo.");
        }               
        this.locals = locals;
    }

}
