/**
 * 
 */
package models;

/**
 * @author projeto
 *
 */
public enum VisitFrequency {
	MONTHLY(1), BIWEEKLY(2), WEEKLY(3), TWO_TIMES_PER_WEEK(4), DAILY(5);
	
	private int visitFrequency;

	/**
	 * @param visitFrequency
	 */
	private VisitFrequency(int visitFrequency) {
		this.visitFrequency = visitFrequency;
	}

	/**
	 * @return the visitFrequency
	 */
	public int getVisitFrequency() {
		return visitFrequency;
	}

	/**
	 * @param visitFrequency the visitFrequency to set
	 */
	public void setVisitFrequency(int visitFrequency) {
		this.visitFrequency = visitFrequency;
	}
		
}
