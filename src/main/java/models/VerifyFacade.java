package models;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import utils.WriteExcel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.JProgressBar;

import java.util.regex.Pattern;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import utils.Utils;

public class VerifyFacade {

	private VerifyFacade() {
		//
	}

	public static String executeHTTPRequestToWS(String url, String jsonInstanceContent) throws IOException {
		// prepare json
		StringEntity input = new StringEntity(jsonInstanceContent, "UTF-8");
		input.setContentType("application/json");	
		
		// prepare http request
		HttpPost postRequest = new HttpPost(url);
		postRequest.setEntity(input);
		HttpClient httpClient = HttpClientBuilder.create().build();
		// do request and prepare response
		HttpResponse response = httpClient.execute(postRequest);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		}
		// prepare output
		StringBuilder totalOutput = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
		String output;
		while ((output = br.readLine()) != null) {
			totalOutput.append(output);
		}
		return totalOutput.toString();
	}

	public static String[] searchConfFileWithoutInstances(String[] originDirs) {
		List<String> confFileWithoutInstances = new ArrayList<String>();
		Integer size = originDirs.length;
		for (int i = 0; i < size; i++) {
			// get intanceName
			String part = originDirs[i].toLowerCase();
			if (part.endsWith(Constants.INSTANCE_CONF_FILE_EXTENSION)) {
				part = part.replace(Constants.INSTANCE_CONF_FILE_EXTENSION, "");
				boolean find = false;
				// get instance
				for (int j = 0; j < size; j++) {
					if (i != j) {
						String otherPart = originDirs[j].toLowerCase().replace(Constants.INSTANCE_EXTENSION, "");
						if (otherPart.equals(part)) {
							find = true;
							break;
						}
					}
				}
				if (!find) {
					confFileWithoutInstances.add(originDirs[i]);
				}
			}
		}
		return confFileWithoutInstances.toArray(new String[confFileWithoutInstances.size()]);
	}

	public static String[] searchInstancesWithoutConfFile(String[] originDirs) {
		List<String> instancesWithoutConfFile = new ArrayList<String>();
		Integer size = originDirs.length;
		for (int i = 0; i < size; i++) {
			// get intanceName
			String part = originDirs[i].toLowerCase();
			if (!part.endsWith(Constants.INSTANCE_CONF_FILE_EXTENSION)) {
				part = part.replace(Constants.INSTANCE_EXTENSION, "");
				boolean find = false;
				// get instance conf file
				for (int j = 0; j < size; j++) {
					if (i != j) {
						String anotherPart = originDirs[j].toLowerCase().replace(Constants.INSTANCE_CONF_FILE_EXTENSION,
								"");
						if (anotherPart.equals(part)) {
							find = true;
							break;
						}
					}
				}
				if (!find) {
					instancesWithoutConfFile.add(originDirs[i]);
				}
			}
		}
		return instancesWithoutConfFile.toArray(new String[instancesWithoutConfFile.size()]);
	}

	public static void testInstancesByDirs(String[] originDirs, String url, String destinyDir, JProgressBar progressBar)
			throws Exception {
		Integer size = originDirs.length;
		int validInstancesNumber = size - (VerifyFacade.searchConfFileWithoutInstances(originDirs).length
				+ searchInstancesWithoutConfFile(originDirs).length);
		if (validInstancesNumber == 0) {
			validInstancesNumber = 1;
		}
		//int incrementValue = Math.round(100 / validInstancesNumber) - 1;
		for (int i = 0; i < size; i++) {			
			// get intanceName
			String[] parts = originDirs[i].toLowerCase().split(Pattern.quote(Constants.INSTANCE_CONF_FILE_EXTENSION));
			if (parts.length > 0) {				
				// get instance
				String instaceDirPart = parts[parts.length - 1];
				for (int j = 0; j < size; j++) {					
					if (i != j) {						
						parts = originDirs[j].toLowerCase().split(Pattern.quote(Constants.INSTANCE_EXTENSION));						
						if (parts.length > 0 && instaceDirPart.equals(parts[parts.length - 1])) {
							// get contents
							// read instance
							String instanceContent = Utils.readFileByDir(originDirs[j]);
							Instance instance = VerifyFacade.mountInstanceByString(instanceContent);
							// read conf
							InstanceParams instanceParams = VerifyFacade
									.mountInstanceParamsByConfFile(Utils.readFileByDir(originDirs[i]), instance);
							// request WS and mount output
							String output = null;
							if (instance.getInstanceType().equals(InstanceType.PLANNER)) {
								output = VerifyFacade.executeHTTPRequestToWS(url, instanceContent);
							}else {
								output = VerifyFacade.executeHTTPRequestToWS(
										VerifyFacade.apllyInstanceParamsOnURL(url, instanceParams), instanceContent);
							}
							Result result = VerifyFacade.mountResultByOutputString(output, instance);
							parts = instaceDirPart.split(Pattern.quote(File.separator));
							// create instance folder
							String instanceName = parts[parts.length - 1];
							String folderName = destinyDir + File.separator + instanceName;
							String baseFileName = instanceName + "-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyyhhmmss"));
							File folder = new File(folderName);
							if (!folder.exists() && !folder.mkdir()) {
								throw new IllegalStateException("Erro: Não foi possí­vel criar a pasta " + folderName);
							}
							// save sheet
							VerifyFacade.writeSheet(instance, result, Constants.SHEET_NAME, folderName + File.separator + baseFileName + Constants.RESULT_EXTENSION, progressBar, validInstancesNumber);
							// save output
							Files.write(Paths.get(folderName + File.separator + baseFileName + Constants.INSTANCE_EXTENSION), output.getBytes());
							//progressBar.setValue(progressBar.getValue() + incrementValue);
						}
					}
				}
			}
		}
		progressBar.setValue(100);
	}
        
        public static void testInstance(String instanceOriginDir, String instanceOutputDir, String destinyDir, JProgressBar progressBar) throws Exception {   					
            String[] parts = instanceOriginDir.toLowerCase().split(Pattern.quote(Constants.INSTANCE_EXTENSION));
            parts = parts[parts.length - 1].split(Pattern.quote(File.separator));
            // read instance
            String instanceContent = Utils.readFileByDir(instanceOriginDir);
            Instance instance = VerifyFacade.mountInstanceByString(instanceContent);
            // read instance output
            String output = Utils.readFileByDir(instanceOutputDir);
            Result result = VerifyFacade.mountResultByOutputString(output, instance);
            // create instance folder
            String instanceName = parts[parts.length - 1];
            System.out.println(instanceName);
            String folderName = destinyDir + File.separator + instanceName;
            String baseFileName = instanceName + "-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyyhhmmss"));
            File folder = new File(folderName);
            if (!folder.exists() && !folder.mkdir()) {throw new IllegalStateException("Erro: Não foi possível criar a pasta " + folderName);
            }
            progressBar.setValue(5);
            // save sheet
            VerifyFacade.writeSheet(instance, result, Constants.SHEET_NAME,folderName + File.separator + baseFileName + Constants.RESULT_EXTENSION, progressBar, 1);
            // save output
            progressBar.setValue(95);
            Files.write(Paths.get(folderName + File.separator + baseFileName + Constants.INSTANCE_EXTENSION), output.getBytes());
            progressBar.setValue(100);
	}

	public static void saveMap(Result result, String path) throws MalformedURLException, IOException {
		String endereco = "https://maps.googleapis.com/maps/api/staticmap?size=" + Constants.MAP_IMAGE_SIZE
				+ "&scale=2";
		String currentColor;
		List<Route> routes = result.getRoutes();
		// definir marcador para CD
		for (Route route : routes) {
			// definir marcador para todos os clientes com exceÃ§Ã£o do CD
			// definir cor da rota
			// mudar cor dinamicamente
		}
		ImageIO.write(ImageIO.read(((HttpURLConnection) new URL(endereco).openConnection()).getInputStream()),
				Constants.MAP_IMAGE_FORMAT, new File(path));
	}

	public static InstanceParams mountInstanceParamsByConfFile(String confFileContent, Instance instance) {
		InstanceParams instanceParams = new InstanceParams();
		instanceParams.setPrmMap(new Gson().fromJson(confFileContent, JsonObject.class)
				.getAsJsonPrimitive(Constants.INSTANCE_PARAMS_NAME[0]).getAsInt());
		instanceParams.setInstance(instance);
		return instanceParams;
	}

	public static String apllyInstanceParamsOnURL(String url, InstanceParams instanceParams) {
		return url.charAt(url.length() - 1) == '/' ? url + instanceParams.getPrmMap()
				: url + "/" + instanceParams.getPrmMap();
	}

	public static Result mountResultByOutputString(String output, Instance instance) throws Exception {
		Result result = new Result();		
		System.out.println(output);
		JsonObject jsonOutput = new Gson().fromJson(output, JsonObject.class);
		OutputFactory outputFactory = null; 
		if (instance.getInstanceType().equals(InstanceType.GRUPOS)) {
			outputFactory = new OutputFactoryGrupos(jsonOutput, instance);				
		} else if (instance.getInstanceType().equals(InstanceType.ROTAS)){
			outputFactory = new OutputFactoryRotas(jsonOutput, instance);
		} else if (instance.getInstanceType().equals(InstanceType.PLANNER)){
			outputFactory = new OutputFactoryPlanner(jsonOutput, instance);
		} else if (instance.getInstanceType().equals(InstanceType.UNKNOWN)){
			throw new IllegalStateException("Formato de instância desconhecido");
		}
		result.setRoutes(outputFactory.createRoutes());
		// getting not served customers
		result.setNotServed(outputFactory.createNotServed());
		// getting resumo			
		result.setAlgorithmTime(outputFactory.createAlgorithmTime());	
		return result;
	}

	public static Instance mountInstanceByString(String instanceString) throws Exception {
		JsonObject jsonInstance = new Gson().fromJson(instanceString, JsonObject.class);
		InstanceFactory instanceFactory = null;
		InstanceType instanceType = getInstanceTypeByJSONObject(jsonInstance);
		if (instanceType.equals(InstanceType.GRUPOS)) {
			instanceFactory = new InstanceFactoryGrupos(jsonInstance);				
		} else if (instanceType.equals(InstanceType.ROTAS)){
			instanceFactory = new InstanceFactoryRotas(jsonInstance);
		} else if (instanceType.equals(InstanceType.PLANNER)){
			instanceFactory = new InstanceFactoryPlanner(jsonInstance);
		} else if (instanceType.equals(InstanceType.UNKNOWN)){
			throw new IllegalStateException("Formato de instância desconhecido");
		}
		return instanceFactory.createInstance();
	}	
	
	public static InstanceType getInstanceTypeByJSONObject(JsonObject instanceJsonObject) {
		boolean hasClientes = instanceJsonObject.getAsJsonArray("clientes") != null,
				hasVendedores = instanceJsonObject.getAsJsonArray("vendedores") != null,
				hasParametros = instanceJsonObject.getAsJsonArray("parametros") != null,
				hasParams = instanceJsonObject.getAsJsonObject("params") != null,
				hasGrupos = instanceJsonObject.getAsJsonArray("grupos") != null,
				hasVeiculos = instanceJsonObject.getAsJsonArray("veiculos") != null;
		if (hasClientes && hasVendedores && hasParametros) {
			return InstanceType.PLANNER;
		}
		if (hasClientes && hasParams && hasVeiculos) {
			return InstanceType.GRUPOS;
		}
		if (hasParams && hasGrupos) {
			return InstanceType.ROTAS;
		}
		return InstanceType.UNKNOWN;
	}
		
	public static List<Customer> getLostCustomers(Instance instance, Result result) {
		List<Customer> customers = instance.getCustomers();
		List<Customer> notServedCustomers = result.getNotServed();
		List<Route> routes = result.getRoutes();
		List<Customer> lostCustomers = new ArrayList<Customer>();
		for (Customer customer : customers) {
			boolean find = false;
			// searching in not served customers
			for (Customer notServedCustomer : notServedCustomers) {
				if (notServedCustomer.getId().equals(customer.getId())) {
					find = true;
					break;
				}
			}
			// searching in served customers
			for (int i = 0; !find && i < routes.size(); i++) {
				List<Customer> routeCustomers = routes.get(i).getCustomers();
				for (Customer routeCustomer : routeCustomers) {
					if (routeCustomer.getId().equals(customer.getId())) {
						find = true;
						break;
					}
				}
			}
			if (!find) {
				lostCustomers.add(customer);
			}
		}
		return lostCustomers;
	}

	public static Integer getUsedVehiclesNumber(List<Vehicle> vehicles) {
		Integer usedVehiclesNumber = 0;
		for (Vehicle vehicle : vehicles) {
			if (vehicle.getVehicleRouteDatas() != null) {
				usedVehiclesNumber++;
			}
		}
		return usedVehiclesNumber;
	}

	public static List<Route> getRoutesFromCustomer(Customer customer, List<Route> routes) {
		List<Route> routesFromCustomer = new ArrayList<Route>();
		for (Route route : routes) {
			List<Customer> customers = route.getCustomers();
			for (Customer routeCustomer : customers) {
				if (routeCustomer.getId().equals(customer.getId())) {
					routesFromCustomer.add(route);
					break;
				}
			}
		}
		return routesFromCustomer;
	}

	public static String formatRoutesIds(List<Route> routes) {
		Integer routesFromCustomerSize = routes.size();
		String ids = String.valueOf(routes.get(0).getId());
		// format string id's
		for (int i = 1; i < routesFromCustomerSize; i++) {
			ids += ", " + routes.get(i).getId();
		}
		return ids;
	}

	public static Boolean isValidVehicleFilledWeight(Vehicle vehicle) {
		VehicleRouteDatas vehicleRouteDatas = vehicle.getVehicleRouteDatas();
		if (vehicleRouteDatas != null) {
			return vehicleRouteDatas.getWeightFilled() <= vehicle.getWeightLimit();
		}
		return true;
	}

	public static Boolean vehicleExistsInInstace(Vehicle vehicle, Instance instance) {
		List<Vehicle> vehicles = instance.getVehicles();
		for (Vehicle vehicleFromInstance : vehicles) {
			if (vehicleFromInstance.getBoard().equals(vehicle.getBoard())) {
				return true;
			}
		}
		return false;
	}

	public static Boolean isValidVehicleFilledServiceTime(Vehicle vehicle) {
		VehicleRouteDatas vehicleRouteDatas = vehicle.getVehicleRouteDatas();
		if (vehicleRouteDatas != null) {
			return vehicleRouteDatas.getServiceTimeFilled() <= vehicle.getServiceTimeLimit();
		}
		return true;
	}

	public static Boolean isValidMadeVisitsNumberByVehicle(Vehicle vehicle) {
		VehicleRouteDatas vehicleRouteDatas = vehicle.getVehicleRouteDatas();
		if (vehicleRouteDatas != null) {
			return vehicleRouteDatas.getVisitsMade() <= vehicle.getVisitsLimit();
		}
		return true;
	}

	public static Boolean isValidVehicleFilledCubing(Vehicle vehicle) {
		VehicleRouteDatas vehicleRouteDatas = vehicle.getVehicleRouteDatas();
		if (vehicleRouteDatas != null) {
			return vehicleRouteDatas.getCubingFilled() <= vehicle.getCubingLimit();
		}
		return true;
	}

	public static Boolean isBrokenExpedient(Customer customer) {
		Expedient expedient = customer.getExpedient();
		if (expedient != null) {
			CustomerRouteDatas customerRouteDatas = customer.getCustomerRouteDatas();
			if (customerRouteDatas != null) {
				Expedient servedExpedient = customerRouteDatas.getServedExpedient();
				if (isStartExpedientBroken(expedient.getStart(), servedExpedient.getStart())
						|| isFinishExpedientBroken(expedient.getFinish(), servedExpedient.getFinish())) {
					return true;
				}
			}
		}
		return false;
	}

	public static Boolean isStartExpedientBroken(Double start, Double startServe) {
		return startServe < start;
	}

	public static Boolean isFinishExpedientBroken(Double finish, Double finishServe) {
		return finishServe > finish;
	}

	public static Integer getBrokenExpedientsNumber(List<Customer> customers) {
		Integer brokenExpedientsNumber = 0;
		for (Customer customer : customers) {
			if (isBrokenExpedient(customer)) {
				brokenExpedientsNumber++;
			}
		}
		return brokenExpedientsNumber;
	}

	public static Boolean isValidRouteTimeAndKilometers(Route route) {
		Double averageSpeed = route.getDepot().getCustomerRouteDatas().getCurrentMileage()
				/ route.getVehicle().getVehicleRouteDatas().getServiceTimeFilled();
		return averageSpeed >= Constants.MINIMUM_VEHICLE_AVERAGE_SPEED
				&& averageSpeed <= Constants.MAXIMUM_VEHICLE_AVERAGE_SPEED;
	}

	public static Boolean isCustomerRepeatedInRoute(Customer customer, List<Customer> customers) {
		Integer qtt = 0;
		for (Customer customerFromRoute : customers) {
			if (customer.getId().equals(customerFromRoute.getId())) {
				qtt++;
			}
		}
		return qtt > 1;
	}

	public static Double getDistanceByRoutedCustomers(Customer firstCustomer, Customer secondCustomer) {
		CustomerRouteDatas firstCustomerRouteDatas = firstCustomer.getCustomerRouteDatas();
		CustomerRouteDatas secondCustomerRouteDatas = secondCustomer.getCustomerRouteDatas();
		if (firstCustomer != null && secondCustomer != null && firstCustomerRouteDatas != null
				&& secondCustomerRouteDatas != null) {
			return (secondCustomerRouteDatas.getCurrentMileage() - firstCustomerRouteDatas.getCurrentMileage());
		}
		throw new IllegalArgumentException(
				"Erro: O parÃ¢metro firstCustomer e secondCustomer nÃ£o podem ser nulos e devem ter CustomerRouteDatas nÃ£o nulo.");
	}

	public static Double getTravelTimeByRoutedCustomers(Customer firstCustomer, Customer secondCustomer) {
		CustomerRouteDatas firstCustomerRouteDatas = firstCustomer.getCustomerRouteDatas();
		CustomerRouteDatas secondCustomerRouteDatas = secondCustomer.getCustomerRouteDatas();
		if (firstCustomer != null && secondCustomer != null && firstCustomerRouteDatas != null
				&& secondCustomerRouteDatas != null) {
			return (secondCustomerRouteDatas.getServedExpedient().getStart()
					- firstCustomerRouteDatas.getServedExpedient().getFinish());
		}
		throw new IllegalArgumentException(
				"Erro: O parÃ¢metro firstCustomer e secondCustomer nÃ£o podem ser nulos e devem ter CustomerRouteDatas nÃ£o nulo.");
	}

	public static Double getSpeedByCustomers(Customer firstCustomer, Customer secondCustomer) {
		CustomerRouteDatas firstCustomerRouteDatas = firstCustomer.getCustomerRouteDatas();
		CustomerRouteDatas secondCustomerRouteDatas = secondCustomer.getCustomerRouteDatas();
		if (firstCustomer != null && secondCustomer != null && firstCustomerRouteDatas != null
				&& secondCustomerRouteDatas != null) {
			return (secondCustomerRouteDatas.getCurrentMileage() - firstCustomerRouteDatas.getCurrentMileage())
					/ (secondCustomerRouteDatas.getServedExpedient().getStart()
							- firstCustomerRouteDatas.getServedExpedient().getFinish());
		}
		throw new IllegalArgumentException(
				"Erro: O parÃ¢metro firstCustomer e secondCustomer nÃ£o podem ser nulos e devem ter CustomerRouteDatas nÃ£o nulo.");
	}

	public static void writeSheet(Instance instance, Result result, String sheetName, String fileName, JProgressBar progressBar, int qtdInstance)
			throws IOException {
		SheetFactory sheetFactory = null;
		if (instance.getInstanceType().equals(InstanceType.GRUPOS)) {
			sheetFactory = new SheetFactoryGrupos(instance, result, sheetName, fileName);
		}else if (instance.getInstanceType().equals(InstanceType.ROTAS)) {
			sheetFactory = new SheetFactoryRotas(instance, result, sheetName, fileName);
		}else if (instance.getInstanceType().equals(InstanceType.PLANNER)) {
			sheetFactory = new SheetFactoryPlanner(instance, result, sheetName, fileName);
		}else {
			throw new IllegalStateException("Erro: Tipo de instância inválida");
		}
		sheetFactory.createSheet(progressBar, qtdInstance).write(fileName);
	}
	
	public static Integer calculateTotalWorkedDays(List<Route> routes, Boolean workInSaturday) {
		Integer lastWorkedDay = getLastWorkedDays(routes); 
		Integer weeksNumber = lastWorkedDay/7;
		Integer totalWorkedDays = workInSaturday ? weeksNumber * 6 : weeksNumber * 5;
		return totalWorkedDays + (lastWorkedDay%7);
	}
	
	public static List<Integer> getNotWorkedDaysFromVehicle(Vehicle vehicle, List<Route> routes){
		Set<Integer> days = new HashSet<Integer>();
		for (Route route : routes) {
			if (!route.getVehicle().getBoard().equals(vehicle.getBoard())) {
				days.add(route.getDay());
			}
		}
		for (Route route : vehicle.getVehicleRouteDatas().getServedRoutes()) {
			if (days.contains(route.getDay())) {
				days.remove(route.getDay());
			}
		}
		return new ArrayList<Integer> (days);
	}
	
	public static Integer getLastWorkedDays(List<Route> routes) {
		Integer lastWorkedDay = null;
		for (Route route : routes) {			
			if (lastWorkedDay == null || lastWorkedDay < route.getDay()) {
				lastWorkedDay = route.getDay();
			}
		}
		return lastWorkedDay;
	}
	
	public static Integer[] weeksNotVisitedByCustomerNTimes(List<Route> routes, Customer customer, Integer n) {
		Integer weekNumber = 1;
		List<Integer> weeksNotVisitedNTimes = new ArrayList<Integer>();
		do {
			// get routes from weekNumber
			List<Route> weekRoute = getRouteByWeekNumber(routes, weekNumber);
			// if there is routes from weekNumber
			if (weekRoute.size() > 0) {
				// if the customer is visited in weekRoutes
				Integer visits = 0;
				for (int k = 0; k < weekRoute.size(); k++) {
					List<Customer> routeCustomers = weekRoute.get(k).getCustomers();
					for (int l = 0; l < routeCustomers.size(); l++) {
						if (customer.getId() == routeCustomers.get(l).getId()) {
							visits++;
							break;
						}
					}
				}
				if (visits != n) {
					weeksNotVisitedNTimes.add(weekNumber);
				}
				weekNumber++;
			} else {
				break;
			}
		} while (true);
		return weeksNotVisitedNTimes.toArray(new Integer[weeksNotVisitedNTimes.size()]);
	}

	public static List<Route> getRouteByWeekNumber(List<Route> routes, Integer weekNumber) {
		List<Route> weekRoute = new ArrayList<Route>();
		for (int k = 0; k < routes.size(); k++) {
			if (getWeekByRouteDay(routes.get(k).getDay()) == weekNumber) {
				weekRoute.add(routes.get(k));
			}
		}
		return weekRoute;
	}

	public static List<Route> getRouteByMonthNumber(List<Route> routes, Integer monthNumber) {
		List<Route> monthRoute = new ArrayList<Route>();
		for (int k = 0; k < routes.size(); k++) {
			if (getMonthByRouteDay(routes.get(k).getDay()) == monthNumber) {
				monthRoute.add(routes.get(k));
			}
		}
		return monthRoute;
	}
	
	public static Integer getWeekDayByRouteDay(Integer routeDay) {
		return routeDay % 7;
	}

	public static Integer getWeekByRouteDay(Integer routeDay) {
		return (routeDay / 7) + 1;
	}

	public static Integer getMonthByRouteDay(Integer routeDay) {
		return (routeDay / Constants.MONTH_DAYS_NUM) + 1;
	}
	
	public static double[][] getLunchInterval(Vehicle routeVehicle, List<Customer> customers) {
		double [][] lunchInterval = new double[customers.size()][2];
		VehicleRestrictions vehicleRestrictions = routeVehicle.getVehicleRestrictions();
		for (int j = 0; j+1 < customers.size(); j++) {
			Expedient expedientCustomer = customers.get(j).getCustomerRouteDatas().getServedExpedient();
			Expedient nextExpedientCustomer = customers.get(j+1).getCustomerRouteDatas().getServedExpedient();
			if(expedientCustomer.getFinish()%24 >= vehicleRestrictions.getLunchExpedient().getStart()%24-vehicleRestrictions.getLunchAntecipate()%24 && expedientCustomer.getFinish()%24 <= vehicleRestrictions.getLunchExpedient().getStart()%24+vehicleRestrictions.getLunchDelay()%24) {
				lunchInterval[j][0]= nextExpedientCustomer.getStart()%24 - expedientCustomer.getFinish()%24;
			}
		}
		for (int j = 1; j < customers.size(); j++) {
			Expedient expedientCustomer = customers.get(j).getCustomerRouteDatas().getServedExpedient();
			Expedient antecedentExpedientCustomer = customers.get(j-1).getCustomerRouteDatas().getServedExpedient();
			if(expedientCustomer.getStart()%24 >= vehicleRestrictions.getLunchExpedient().getStart()%24+vehicleRestrictions.getLunchDelay()%24 && expedientCustomer.getStart()%24 <= vehicleRestrictions.getLunchExpedient().getFinish()%24+vehicleRestrictions.getLunchDelay()%24) {
				lunchInterval[j][1]= expedientCustomer.getStart()%24 - antecedentExpedientCustomer.getFinish()%24;
			}
		}
		return lunchInterval;
	}
	
	public static boolean[] getRespectedLunch(Vehicle routeVehicle, List<Customer> customers, double[][] lunchInterval) {
		VehicleRestrictions vehicleRestrictions = routeVehicle.getVehicleRestrictions();
		int quantityLunchs = (int) Math.ceil(vehicleRestrictions.getExpedient().getFinish()/24); 
		boolean[] respectedLunch = new boolean[quantityLunchs];
		for (int j = 0; j < customers.size(); j++) {
			Expedient expedientCustomer = customers.get(j).getCustomerRouteDatas().getServedExpedient();
			for (int k = 0; k < quantityLunchs; k++) {
				if(expedientCustomer.getStart()>24*k && expedientCustomer.getStart()<24*(k+1) || expedientCustomer.getFinish()>24*k && expedientCustomer.getFinish()<24*(k+1)) {
					if(lunchInterval[j][0]>=vehicleRestrictions.getLunchExpedient().getFinish()-vehicleRestrictions.getLunchExpedient().getStart() || lunchInterval[j][1]>=vehicleRestrictions.getLunchExpedient().getFinish()-vehicleRestrictions.getLunchExpedient().getStart()) {
						respectedLunch[k] = true;
					}
				}
			}
		}
		return respectedLunch;
	}
	
	public static Boolean isStartNotRespectedLunch(Expedient expedientCustomer, VehicleRestrictions vehicleRestrictions, int quantityLunchs, boolean[] respectedLunch) {
		if(expedientCustomer.getStart()%24>=vehicleRestrictions.getLunchExpedient().getStart()%24-vehicleRestrictions.getLunchAntecipate()%24 && expedientCustomer.getStart()%24<=vehicleRestrictions.getLunchExpedient().getFinish()%24+vehicleRestrictions.getLunchDelay()%24) {
			for (int k = 0; k < quantityLunchs; k++) {
				if(expedientCustomer.getStart()>24*k && expedientCustomer.getStart()<24*(k+1) || expedientCustomer.getFinish()>24*k && expedientCustomer.getFinish()<24*(k+1)) {
					if (!respectedLunch[k]) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static Boolean isFinishNotRespectedLunch(Expedient expedientCustomer, VehicleRestrictions vehicleRestrictions, int quantityLunchs, boolean[] respectedLunch) {
		if(expedientCustomer.getFinish()%24>=vehicleRestrictions.getLunchExpedient().getStart()%24-vehicleRestrictions.getLunchAntecipate()%24 && expedientCustomer.getFinish()%24<=vehicleRestrictions.getLunchExpedient().getFinish()+vehicleRestrictions.getLunchDelay()%24) {
			for (int k = 0; k < quantityLunchs; k++) {
				if(expedientCustomer.getStart()>24*k && expedientCustomer.getStart()<24*(k+1) || expedientCustomer.getFinish()>24*k && expedientCustomer.getFinish()<24*(k+1)) {
					if (!respectedLunch[k]) {
						return true;
					}
				}
			}
		}
		return false;
	}

	
}