/**
 * 
 */
package models;

/**
 * @author projeto
 *
 */
public enum WeekDay {
	SUNDAY(1), MONDAY(2), TUESDAY(3), WEDNESDAY(4), THURSDAY(5), FRIDAY(6), SATURDAY(7);
	
	private int weekDay;
	
	/**
	 * @param weekDay
	 */
	private WeekDay(int weekDay) {
		this.weekDay = weekDay;
	}

	/**
	 * @return the weekDay
	 */
	public int getWeekDay() {
		return weekDay;
	}

	/**
	 * @param weekDay the weekDay to set
	 */
	public void setWeekDay(int weekDay) {
		this.weekDay = weekDay;
	}
	
}
