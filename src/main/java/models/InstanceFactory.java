/**
 * 
 */
package models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * @author projeto
 *
 */
public abstract class InstanceFactory {
	
	private JsonObject instanceJsonObject;
	private Instance instance;
	
	public InstanceFactory(JsonObject instanceJsonObject, InstanceType instanceType) {
		this.setInstanceJsonObject(instanceJsonObject);
		this.setInstance(new Instance());
		this.getInstance().setInstanceType(instanceType);
	}	
		
	public abstract List<Customer> createDepots();
	public abstract List<Customer> createCustomers();
	public abstract List<Instance.VelocityPerDistance> createVelocitiesPerDistance();
	public abstract List<Vehicle> createVehicles();
			
	public Instance createInstance() {
		Instance instance = this.getInstance();
		// setting clientes
		instance.setCustomers(this.createCustomers());
		// setting vehicles
		instance.setVehicles(this.createVehicles());
		// setting velocities
		instance.setVelocitiesPerDistance(this.createVelocitiesPerDistance());
		// setting values
		instance.setDepots(this.createDepots());
		return instance;
	}
	
	protected Vehicle getVehicleByJSONObject(JsonObject jsonObject) {		
		Vehicle vehicle = new Vehicle();
		vehicle.setBoard(jsonObject.getAsJsonPrimitive("id").getAsString());
		// limits
		JsonArray limites = jsonObject.getAsJsonArray("limites");
		vehicle.setWeightLimit(limites.get(0).getAsDouble());
		vehicle.setServiceTimeLimit(limites.get(1).getAsDouble());
		vehicle.setVisitsLimit(limites.get(2).getAsInt());
		vehicle.setCubingLimit(limites.get(3).getAsDouble());
		return vehicle;
	}
	
	protected List<Customer> getCustomersByJSONArray(JsonArray jsonArray, Expedient expedient) {
		List<Customer> customers = new ArrayList<Customer>(jsonArray.size());
		for (int i = 0; i < jsonArray.size(); i++) {
			Customer customer = new Customer();
			JsonObject cliente = jsonArray.get(i).getAsJsonObject();
			customer.setId(cliente.getAsJsonPrimitive("id").getAsInt());
			customer.setX(cliente.getAsJsonPrimitive("x").getAsDouble());
			customer.setY(cliente.getAsJsonPrimitive("y").getAsDouble());
			// restrictions
			JsonArray totais = cliente.getAsJsonArray("totais");
			customer.setWeight(totais.get(0).getAsDouble());
			customer.setServiceTime(totais.get(1).getAsDouble());
			customer.setVisitsLimit(totais.get(2).getAsInt());
			customer.setCubing(totais.get(3).getAsDouble());
			// time windows
			JsonObject jt = cliente.getAsJsonObject("jt");
			if (jt != null) {
				customer.setExpedient(new Expedient(jt.getAsJsonPrimitive("ini").getAsDouble(),
						jt.getAsJsonPrimitive("fim").getAsDouble()));
			} else {
				customer.setExpedient(expedient);
			}
			customers.add(customer);
		}
		return customers;
	}
	
	/**
	 * @return the instanceJsonObject
	 */
	public JsonObject getInstanceJsonObject() {
		return instanceJsonObject;
	}

	/**
	 * @param instanceJsonObject the instanceJsonObject to set
	 */
	public void setInstanceJsonObject(JsonObject instanceJsonObject) {
		if (instanceJsonObject != null) {
			this.instanceJsonObject = instanceJsonObject;
		}else {
			throw new IllegalArgumentException("Erro: O parâmetro instanceJsonObject não pode ser nulo");
		}
	}

	/**
	 * @return the instance
	 */
	public Instance getInstance() {
		return instance;
	}

	/**
	 * @param instance the instance to set
	 */
	public void setInstance(Instance instance) {		
		if (instance != null) {
			this.instance = instance;
		}else {
			throw new IllegalArgumentException("Erro: O parâmetro instance não pode ser nulo");
		}
	}
	
}
