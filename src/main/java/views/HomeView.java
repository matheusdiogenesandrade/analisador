/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import controllers.HomeController;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;

/**
 *
 * @author matheus
 */
public class HomeView extends javax.swing.JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private HomeController homeController;
    
    /**
     * Creates new form HomeView
     * @param homeController
     */
    public HomeView(HomeController homeController) {
        this.setHomeController(homeController);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        btnTest = new javax.swing.JButton();
        btnMoveUnselected = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        btnMoveSelected = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        lblMenssagens = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listInstSelected = new javax.swing.JList<>();
        btnSelectDirDestiny = new javax.swing.JButton();
        txtDirOrigin = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtUrl = new javax.swing.JComboBox<>();
        txtDirDestiny = new javax.swing.JComboBox<>();
        btnMoveAll = new javax.swing.JButton();
        btnSelectDirOrigin = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listInstAvailable = new javax.swing.JList<>();
        btnRemoveAll = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtInstance = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        btnSelectInstance = new javax.swing.JButton();
        txtInstanceOutput = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        btnSelectInstanceOutput = new javax.swing.JButton();
        txtDirDestinyTestU = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        btnSelectDirDestinyTestU = new javax.swing.JButton();
        btnTestU = new javax.swing.JButton();
        progressBarU = new javax.swing.JProgressBar();
        btnCancelU = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Analisador Pathfind");
        setName("frmHome"); // NOI18N
        setResizable(false);

        jLabel3.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel3.setText("Analisador");

        btnTest.setText("Testar");
        btnTest.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestActionPerformed(evt);
            }
        });

        btnMoveUnselected.setText("<");
        btnMoveUnselected.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMoveUnselectedActionPerformed(evt);
            }
        });

        progressBar.setStringPainted(true);

        btnMoveSelected.setText(">");
        btnMoveSelected.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMoveSelectedActionPerformed(evt);
            }
        });

        jLabel4.setText("Diretório de origem:");

        jLabel1.setText("URL:");

        listInstSelected.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        listInstSelected.setToolTipText("");
        jScrollPane1.setViewportView(listInstSelected);

        btnSelectDirDestiny.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dir.png"))); // NOI18N
        btnSelectDirDestiny.setName(""); // NOI18N
        btnSelectDirDestiny.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectDirDestinyActionPerformed(evt);
            }
        });

        txtDirOrigin.setEditable(true);
        txtDirOrigin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDirOriginActionPerformed(evt);
            }
        });

        jLabel5.setText("Exemplo: http://localhost:8080/plannerws/rs/PathfindJSON/tam/1/cli/1/map/");

        txtUrl.setEditable(true);

        txtDirDestiny.setEditable(true);

        btnMoveAll.setText(">>>");
        btnMoveAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMoveAllActionPerformed(evt);
            }
        });

        btnSelectDirOrigin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dir.png"))); // NOI18N
        btnSelectDirOrigin.setName(""); // NOI18N
        btnSelectDirOrigin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectDirOriginActionPerformed(evt);
            }
        });

        jLabel2.setText("Diretório de destino:");

        listInstAvailable.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jScrollPane2.setViewportView(listInstAvailable);

        btnRemoveAll.setText("<<<");
        btnRemoveAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveAllActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancelar");
        btnCancel.setEnabled(false);
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(408, 408, 408)
                        .addComponent(lblMenssagens, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(143, 143, 143))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel5)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtUrl, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 892, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnTest, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnCancel))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(btnMoveUnselected, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnMoveSelected, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(btnMoveAll, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(btnRemoveAll, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 486, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtDirOrigin, javax.swing.GroupLayout.PREFERRED_SIZE, 1021, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(921, 921, 921))
                                    .addComponent(txtDirDestiny, javax.swing.GroupLayout.PREFERRED_SIZE, 1019, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnSelectDirDestiny, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnSelectDirOrigin, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtUrl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblMenssagens, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDirOrigin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSelectDirOrigin, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDirDestiny, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSelectDirDestiny, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(btnMoveAll, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnMoveSelected, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnMoveUnselected, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRemoveAll, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTest, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Executar e testar", jPanel1);

        txtInstance.setEditable(true);
        txtInstance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtInstanceActionPerformed(evt);
            }
        });

        jLabel6.setText("Instância:");

        btnSelectInstance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dir.png"))); // NOI18N
        btnSelectInstance.setName(""); // NOI18N
        btnSelectInstance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectInstanceActionPerformed(evt);
            }
        });

        txtInstanceOutput.setEditable(true);
        txtInstanceOutput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtInstanceOutputActionPerformed(evt);
            }
        });

        jLabel7.setText("Saída:");

        btnSelectInstanceOutput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dir.png"))); // NOI18N
        btnSelectInstanceOutput.setName(""); // NOI18N
        btnSelectInstanceOutput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectInstanceOutputActionPerformed(evt);
            }
        });

        txtDirDestinyTestU.setEditable(true);
        txtDirDestinyTestU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDirDestinyTestUActionPerformed(evt);
            }
        });

        jLabel8.setText("Diretório de origem:");

        btnSelectDirDestinyTestU.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dir.png"))); // NOI18N
        btnSelectDirDestinyTestU.setName(""); // NOI18N
        btnSelectDirDestinyTestU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectDirDestinyTestUActionPerformed(evt);
            }
        });

        btnTestU.setText("Testar");
        btnTestU.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTestU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestUActionPerformed(evt);
            }
        });

        progressBarU.setStringPainted(true);

        btnCancelU.setText("Cancelar");
        btnCancelU.setEnabled(false);
        btnCancelU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelUActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtInstance, javax.swing.GroupLayout.PREFERRED_SIZE, 1021, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSelectInstance, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel7)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtInstanceOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 1021, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSelectInstanceOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel8)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtDirDestinyTestU, javax.swing.GroupLayout.PREFERRED_SIZE, 1021, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSelectDirDestinyTestU, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(progressBarU, javax.swing.GroupLayout.PREFERRED_SIZE, 892, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnTestU, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelU)))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtInstance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSelectInstance, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(47, 47, 47)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtInstanceOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSelectInstanceOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDirDestinyTestU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSelectDirDestinyTestU, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(189, 189, 189)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTestU, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(progressBarU, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancelU, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Testar", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(498, 498, 498))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(21, 21, 21)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestActionPerformed
        this.getHomeController().testDatas();
    }//GEN-LAST:event_btnTestActionPerformed

    private void btnMoveSelectedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMoveSelectedActionPerformed
        this.getHomeController().multipleSelection(this.getListInstAvailable().getSelectedIndices(), this.getListInstAvailable(), this.getListInstSelected());        
    }//GEN-LAST:event_btnMoveSelectedActionPerformed

    private void btnMoveUnselectedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMoveUnselectedActionPerformed
        this.getHomeController().multipleSelection(this.getListInstSelected().getSelectedIndices(), this.getListInstSelected(), this.getListInstAvailable());        
    }//GEN-LAST:event_btnMoveUnselectedActionPerformed

    private void btnSelectDirDestinyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectDirDestinyActionPerformed
        this.getHomeController().selectDestinyFolder();
    }//GEN-LAST:event_btnSelectDirDestinyActionPerformed

    private void btnSelectDirOriginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectDirOriginActionPerformed
        this.getHomeController().selectOriginFolder();
        this.getHomeController().loadFilesFromOriginFolder();
    }//GEN-LAST:event_btnSelectDirOriginActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        this.getHomeController().getTestDatasManager().finish();
        this.homeController.enableComponents();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void txtDirOriginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDirOriginActionPerformed
        this.homeController.loadFilesFromOriginFolder();
    }//GEN-LAST:event_txtDirOriginActionPerformed

    private void btnMoveAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMoveAllActionPerformed
        int[] indexes = new int[this.getListInstAvailable().getModel().getSize()];
        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = i;
        }
        this.getHomeController().multipleSelection(indexes, this.getListInstAvailable(), this.getListInstSelected());        
    }//GEN-LAST:event_btnMoveAllActionPerformed

    private void btnRemoveAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveAllActionPerformed
        int[] indexes = new int[this.getListInstSelected().getModel().getSize()];
        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = i;
        }
        this.getHomeController().multipleSelection(indexes, this.getListInstSelected(), this.getListInstAvailable());        
    }//GEN-LAST:event_btnRemoveAllActionPerformed

    private void txtInstanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtInstanceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtInstanceActionPerformed

    private void btnSelectInstanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectInstanceActionPerformed
        homeController.selectInstance();
    }//GEN-LAST:event_btnSelectInstanceActionPerformed

    private void txtInstanceOutputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtInstanceOutputActionPerformed
        
    }//GEN-LAST:event_txtInstanceOutputActionPerformed

    private void btnSelectInstanceOutputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectInstanceOutputActionPerformed
        homeController.selectInstanceOutput();
    }//GEN-LAST:event_btnSelectInstanceOutputActionPerformed

    private void txtDirDestinyTestUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDirDestinyTestUActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDirDestinyTestUActionPerformed

    private void btnSelectDirDestinyTestUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectDirDestinyTestUActionPerformed
        homeController.selectDestinyTestUFolder();
    }//GEN-LAST:event_btnSelectDirDestinyTestUActionPerformed

    private void btnTestUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestUActionPerformed
        this.getHomeController().testData();
    }//GEN-LAST:event_btnTestUActionPerformed

    private void btnCancelUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelUActionPerformed
        this.getHomeController().getTestDatasManager().finish();
        this.homeController.enableComponents();
    }//GEN-LAST:event_btnCancelUActionPerformed
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCancelU;
    private javax.swing.JButton btnMoveAll;
    private javax.swing.JButton btnMoveSelected;
    private javax.swing.JButton btnMoveUnselected;
    private javax.swing.JButton btnRemoveAll;
    private javax.swing.JButton btnSelectDirDestiny;
    private javax.swing.JButton btnSelectDirDestinyTestU;
    private javax.swing.JButton btnSelectDirOrigin;
    private javax.swing.JButton btnSelectInstance;
    private javax.swing.JButton btnSelectInstanceOutput;
    private javax.swing.JButton btnTest;
    private javax.swing.JButton btnTestU;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblMenssagens;
    private javax.swing.JList<String> listInstAvailable;
    private javax.swing.JList<String> listInstSelected;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JProgressBar progressBarU;
    private javax.swing.JComboBox<String> txtDirDestiny;
    private javax.swing.JComboBox<String> txtDirDestinyTestU;
    private javax.swing.JComboBox<String> txtDirOrigin;
    private javax.swing.JComboBox<String> txtInstance;
    private javax.swing.JComboBox<String> txtInstanceOutput;
    private javax.swing.JComboBox<String> txtUrl;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the homeController
     */
    public HomeController getHomeController() {
        return homeController;
    }

    /**
     * @param homeController the homeController to set
     */
    public void setHomeController(HomeController homeController) {
        this.homeController = homeController;
    }

    /**
     * @return the btnTestar
     */
    public javax.swing.JButton getBtnTestar() {
        return btnTest;
    }

    /**
     * @param btnTestar the btnTestar to set
     */
    public void setBtnTestar(javax.swing.JButton btnTestar) {
        this.btnTest = btnTestar;
    }

    /**
     * @return the lblMenssagens
     */
    public javax.swing.JLabel getLblMenssagens() {
        return lblMenssagens;
    }

    /**
     * @param lblMenssagens the lblMenssagens to set
     */
    public void setLblMenssagens(javax.swing.JLabel lblMenssagens) {
        this.lblMenssagens = lblMenssagens;
    }

    /**
     * @return the txtUrl
     */
    public javax.swing.JComboBox getTxtDirDestiny() {
        return txtDirDestiny;
    }

    /**
     * @param txtDirDestiny the txtDirDestiny
     */
    public void setTxtDirDestiny(javax.swing.JComboBox txtDirDestiny) {
        this.txtDirDestiny = txtDirDestiny;
    }

    /**
     * @return the jButtonMoveSelected
     */
    public javax.swing.JButton getBtnMoveSelected() {
        return btnMoveSelected;
    }

    /**
     * @return the jButtonMoveUnselected
     */
    public javax.swing.JButton getBtnMoveUnselected() {
        return btnMoveUnselected;
    }

    /**
     * @return the jListInstAvailable
     */
    public javax.swing.JList<String> getListInstAvailable() {
        return listInstAvailable;
    }

    /**
     * @return the jListInstSelected
     */
    public javax.swing.JList<String> getListInstSelected() {
        return listInstSelected;
    }

    /**
     * @return the txtDirOrigem
     */
    public javax.swing.JComboBox getTxtDirOrigin() {
        return txtDirOrigin;
    }

    /**
     * @param txtDirOrigin the txtDirOrigin
     */
    public void setTxtDirOrigin(javax.swing.JComboBox txtDirOrigin) {
        this.txtDirOrigin = txtDirOrigin;
    }

    /**
     * @return the txtUrl
     */
    public javax.swing.JComboBox getTxtUrl() {
        return txtUrl;
    }

    /**
     * @param txtUrl the txtUrl to set
     */
    public void setTxtUrl(javax.swing.JComboBox txtUrl) {
        this.txtUrl = txtUrl;
    }

    /**
     * @return the btnCancel
     */
    public javax.swing.JButton getBtnCancel() {
        return btnCancel;
    }

    /**
     * @param btnCancel the btnCancel to set
     */
    public void setBtnCancel(javax.swing.JButton btnCancel) {
        this.btnCancel = btnCancel;
    }

    /**
     * @return the progressBar
     */
    public javax.swing.JProgressBar getProgressBar() {
        return progressBar;
    }

    /**
     * @param progressBar the progressBar to set
     */
    public void setProgressBar(javax.swing.JProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    /**
     * @return the btnSelectDirDestiny
     */
    public javax.swing.JButton getBtnSelectDirDestiny() {
        return btnSelectDirDestiny;
    }

    /**
     * @return the btnSelectDirOrigin
     */
    public javax.swing.JButton getBtnSelectDirOrigin() {
        return btnSelectDirOrigin;
    }

    public JButton getBtnCancelU() {
        return btnCancelU;
    }

    public void setBtnCancelU(JButton btnCancelU) {
        this.btnCancelU = btnCancelU;
    }

    public JButton getBtnSelectDirDestinyTestU() {
        return btnSelectDirDestinyTestU;
    }

    public void setBtnSelectDirDestinyTestU(JButton btnSelectDirDestinyTestU) {
        this.btnSelectDirDestinyTestU = btnSelectDirDestinyTestU;
    }

    public JButton getBtnSelectInstanceOutput() {
        return btnSelectInstanceOutput;
    }

    public void setBtnSelectInstanceOutput(JButton btnSelectInstanceOutput) {
        this.btnSelectInstanceOutput = btnSelectInstanceOutput;
    }

    public JButton getBtnTestU() {
        return btnTestU;
    }

    public void setBtnTestU(JButton btnTestU) {
        this.btnTestU = btnTestU;
    }

    public JProgressBar getProgressBarU() {
        return progressBarU;
    }

    public void setProgressBarU(JProgressBar progressBarU) {
        this.progressBarU = progressBarU;
    }

    public JComboBox<String> getTxtDirDestinyTestU() {
        return txtDirDestinyTestU;
    }

    public void setTxtDirDestinyTestU(JComboBox<String> txtDirDestinyTestU) {
        this.txtDirDestinyTestU = txtDirDestinyTestU;
    }

    public JComboBox<String> getTxtInstance() {
        return txtInstance;
    }

    public void setTxtInstance(JComboBox<String> txtInstance) {
        this.txtInstance = txtInstance;
    }

    public JComboBox<String> getTxtInstanceOutput() {
        return txtInstanceOutput;
    }

    public void setTxtInstanceOutput(JComboBox<String> txtInstanceOutput) {
        this.txtInstanceOutput = txtInstanceOutput;
    }

    public JButton getBtnSelectInstance() {
        return btnSelectInstance;
    }

    public void setBtnSelectInstance(JButton btnSelectInstance) {
        this.btnSelectInstance = btnSelectInstance;
    }
    
    
    
}
