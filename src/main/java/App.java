
import controllers.HomeController;
import utils.LookAndFeelManager;

/**
 *
 */
/**
 * @author matheus
 *
 */
public class App {

    /**
     * @param args
     * @author matheus
     */
    public static void main(String[] args) {
        LookAndFeelManager.changeThemeToNimbus();
        HomeController homeController = new HomeController();
        homeController.showView();
    }

}
