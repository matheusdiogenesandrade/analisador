import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JProgressBar;

import models.Instance;
import models.Result;
import models.VerifyFacade;

/**
 * 
 */

/**
 * @author projeto
 *
 */
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {				
		BufferedReader br = null;
		FileReader fr = null;
		try {
			br = new BufferedReader(new FileReader("C:/Users/Dmontier/plannerJSON/Request1.json"));
			//br = new BufferedReader(new FileReader("/home/projeto/Documents/Instâncias/plannerJSON/Request1.json"));
			String stringInstance = "";
			for (String sCurrentLine; (sCurrentLine = br.readLine()) != null; stringInstance += sCurrentLine) ;
			Instance instance = VerifyFacade.mountInstanceByString(stringInstance);
			br = new BufferedReader(new FileReader("C:/Users/Dmontier/plannerJSON/request1-25042018011226.json"));
			//br = new BufferedReader(new FileReader("/home/projeto/Documents/Instâncias/plannerJSON/output.json"));
			String outputString = "";
			for (String sCurrentLine; (sCurrentLine = br.readLine()) != null; outputString += sCurrentLine) ;			
			Result result =  VerifyFacade.mountResultByOutputString(outputString, instance);
			VerifyFacade.writeSheet(instance, result, "file", "fileName.xlsx", new JProgressBar(), 1);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}		
	}

}
