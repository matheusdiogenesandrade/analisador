/**
 *
 */
package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import java.io.File;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.ListModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import models.Constants;
import utils.ProgressBarManager;
import utils.TestDatasManager;
import utils.Utils;
import views.HomeView;

/**
 * @author matheus
 *
 */
public class HomeController {

    private HomeView homeView;
    private TestDatasManager testDatasManager;
    
    /**
     * 
     */
    public HomeController() {
        this.setHomeView(new HomeView(this));
        this.setTxtURLValues();
        this.setOriginDirValues();
        this.setDestinyDirValues();
    }

    /**
     * 
     */
    public void setTxtURLValues() {
        String[] values = this.getURLHistoryValues();
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(values);
        this.getHomeView().getTxtUrl().setModel(comboBoxModel);
        this.getHomeView().getTxtUrl().setSelectedIndex(-1);
    }

    /**
     * @return
     */
    public String[] getURLHistoryValues() {
        return this.getSavedHistoryValues(Constants.URL_TXT_HISTORY_DIR, Constants.URL_TXT_HISTORY_NUMBER);
    }

    /**
     * 
     */
    public void saveTxtURLHistoryValues() {
        this.saveHistoryValues(Constants.URL_TXT_HISTORY_DIR, this.getHomeView().getTxtUrl().getSelectedItem().toString().trim(), this.getURLHistoryValues(), Constants.URL_TXT_HISTORY_NUMBER);
    }

    /**
     * 
     */
    public void setOriginDirValues() {
        String[] values = this.getSavedOriginDirHistoryValues();
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(values);
        this.getHomeView().getTxtDirOrigin().setModel(comboBoxModel);
        this.getHomeView().getTxtDirOrigin().setSelectedIndex(-1);
    }

    /**
     * @return
     */
    public String[] getSavedOriginDirHistoryValues() {
        return this.getSavedHistoryValues(Constants.ORIGIN_DIR_HISTORY_DIR, Constants.ORIGIN_DIR_HISTORY_NUMBER);
    }

    /**
     * 
     */
    public void saveOriginDirHistoryValues() {
        this.saveHistoryValues(Constants.ORIGIN_DIR_HISTORY_DIR, this.getHomeView().getTxtDirOrigin().getSelectedItem().toString().trim(), this.getSavedOriginDirHistoryValues(), Constants.ORIGIN_DIR_HISTORY_NUMBER);
    }

    /**
     * 
     */
    public void setDestinyDirValues() {
        String[] values = this.getSavedDestinyDirHistoryValues();
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(values);
        this.getHomeView().getTxtDirDestiny().setModel(comboBoxModel);
        this.getHomeView().getTxtDirDestiny().setSelectedIndex(-1);
    }

    /**
     * @return
     */
    public String[] getSavedDestinyDirHistoryValues() {
        return this.getSavedHistoryValues(Constants.DESTINY_DIR_HISTORY_DIR, Constants.DESTINY_DIR_HISTORY_NUMBER);
    }

    /**
     * 
     */
    public void saveDestinyDirHistoryValues() {
        this.saveHistoryValues(Constants.DESTINY_DIR_HISTORY_DIR, this.getHomeView().getTxtDirDestiny().getSelectedItem().toString().trim(), this.getSavedDestinyDirHistoryValues(), Constants.DESTINY_DIR_HISTORY_NUMBER);
    }

    /**
     * @param dir
     * @param limit
     * @return
     */
    private String[] getSavedHistoryValues(String dir, Integer limit) {
        try {
            JsonArray history = new Gson().fromJson(Utils.readFileByDir(dir), JsonArray.class);
            Integer size = history.size() < limit ? history.size() : limit;
            String[] values = new String[size];
            for (int i = 0; i < size; i++) {
                values[i] = history.get(i).getAsString();
            }
            return values;
        } catch (Exception ex) {
            return new String[0];
        }
    }

    /**
     * @param dir
     * @param newValue
     * @param oldHistoryValues
     * @param limit
     */
    private void saveHistoryValues(String dir, String newValue, String[] oldHistoryValues, Integer limit) {
        if (newValue != null && !newValue.isEmpty()) {
            final String[] newHistoryValues = new String[oldHistoryValues.length + 1 > limit ? limit : oldHistoryValues.length + 1];
            System.arraycopy(oldHistoryValues, 0, newHistoryValues, 1, newHistoryValues.length - 1);            
            for(int i=0; i<newHistoryValues.length;i++){
                if(newValue.equals(newHistoryValues[i])){
                    return;
                }
            }
            newHistoryValues[0] = newValue;
            try {
                Gson gson = new Gson();
                String json = gson.toJson(newHistoryValues);
                Utils.saveContentInDir(dir, json);
            } catch (Exception ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * 
     */
    public void showView() {
        this.getHomeView().setVisible(true);
    }

    /**
     * 
     */
    public void disableComponents() {        
        HomeView homeView = this.getHomeView();
        homeView.getTxtDirDestiny().setEnabled(false);
        homeView.getTxtDirOrigin().setEnabled(false);
        homeView.getBtnSelectDirDestiny().setEnabled(false);
        homeView.getBtnSelectDirOrigin().setEnabled(false);
        homeView.getTxtUrl().setEnabled(false);
        homeView.getBtnMoveSelected().setEnabled(false);
        homeView.getBtnMoveUnselected().setEnabled(false);
        homeView.getListInstSelected().setEnabled(false);
        homeView.getListInstAvailable().setEnabled(false);
        homeView.getBtnTestar().setEnabled(false);
        homeView.getBtnSelectDirDestinyTestU().setEnabled(false);
        homeView.getBtnSelectInstanceOutput().setEnabled(false);
        homeView.getBtnSelectInstance().setEnabled(false);
        homeView.getTxtDirDestinyTestU().setEnabled(false);
        homeView.getTxtInstance().setEnabled(false);
        homeView.getTxtInstanceOutput().setEnabled(false);
        homeView.getBtnTestU().setEnabled(false);
        homeView.getBtnCancelU().setEnabled(true);
        homeView.getBtnCancel().setEnabled(true);
    }

    /**
     * 
     */
    public void enableComponents() {
        HomeView homeView = this.getHomeView();
        homeView.getTxtDirDestiny().setEnabled(true);
        homeView.getTxtDirOrigin().setEnabled(true);
        homeView.getBtnSelectDirDestiny().setEnabled(true);
        homeView.getBtnSelectDirOrigin().setEnabled(true);
        homeView.getTxtUrl().setEnabled(true);
        homeView.getBtnMoveSelected().setEnabled(true);
        homeView.getBtnMoveUnselected().setEnabled(true);
        homeView.getListInstSelected().setEnabled(true);
        homeView.getListInstAvailable().setEnabled(true);
        homeView.getBtnTestar().setEnabled(true);
        homeView.getBtnSelectDirDestinyTestU().setEnabled(true);
        homeView.getBtnSelectInstanceOutput().setEnabled(true);
        homeView.getBtnSelectInstance().setEnabled(true);
        homeView.getTxtDirDestinyTestU().setEnabled(true);
        homeView.getTxtInstance().setEnabled(true);
        homeView.getTxtInstanceOutput().setEnabled(true);
        homeView.getBtnTestU().setEnabled(true);
        homeView.getBtnCancelU().setEnabled(false);
        homeView.getBtnCancel().setEnabled(false);
        
    }

    /**
     * 
     */
    public void testDatas() {
        this.testDatasManager = new TestDatasManager(this, new ProgressBarManager(homeView.getProgressBar()),false);
        this.testDatasManager.start();
    }
    
    /**
     * 
     */
    public void testData() {
        this.testDatasManager = new TestDatasManager(this, new ProgressBarManager(homeView.getProgressBarU()),true);
        this.testDatasManager.start();
    }
    
    /**
     * 
     */
    public void selectOriginFolder() {
        JFileChooser f = new JFileChooser();
        f.setApproveButtonMnemonic('S');
        f.setApproveButtonText("Selecionar");
        f.setApproveButtonToolTipText("Selecionar");
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        String[] savedOriginDirHistory = getSavedOriginDirHistoryValues();
        if( savedOriginDirHistory.length > 0){
            f.setCurrentDirectory(new File(savedOriginDirHistory[0]));
        }
        int returnVal = f.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (f.getSelectedFile() != null) {
                this.getHomeView().getTxtDirOrigin().setSelectedItem(f.getSelectedFile().toString());
            }
        }
    }

    /**
     * 
     */
    public void selectDestinyFolder() {
        JFileChooser f = new JFileChooser();
        f.setApproveButtonMnemonic('S');
        f.setApproveButtonText("Selecionar");
        f.setApproveButtonToolTipText("Selecionar");
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        String[] savedDestinyDirHistory = getSavedDestinyDirHistoryValues();
        if( savedDestinyDirHistory.length > 0){
            f.setCurrentDirectory(new File(savedDestinyDirHistory[0]));
        }
        int returnVal = f.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (f.getSelectedFile() != null) {
                this.getHomeView().getTxtDirDestiny().setSelectedItem(f.getSelectedFile().toString());
            }
        }
    }
    
     /**
     * 
     */
    public void selectInstance() {
        JFileChooser f = new JFileChooser();
        f.setFileFilter(new FileNameExtensionFilter("Arquivo JSON (*.json)", "json"));
        f.setApproveButtonMnemonic('S');
        f.setApproveButtonText("Selecionar");
        f.setApproveButtonToolTipText("Selecionar");
        f.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int returnVal = f.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (f.getSelectedFile() != null) {
                this.getHomeView().getTxtInstance().setSelectedItem(f.getSelectedFile().toString());
            }
        }
    }
    
    public void selectInstanceOutput() {
        JFileChooser f = new JFileChooser();
        f.setFileFilter(new FileNameExtensionFilter("Arquivo JSON (*.json)", "json"));
        f.setApproveButtonMnemonic('S');
        f.setApproveButtonText("Selecionar");
        f.setApproveButtonToolTipText("Selecionar");
        f.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int returnVal = f.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (f.getSelectedFile() != null) {
                this.getHomeView().getTxtInstanceOutput().setSelectedItem(f.getSelectedFile().toString());
            }
        }
    }
    
    public void selectDestinyTestUFolder() {
        JFileChooser f = new JFileChooser();
        f.setApproveButtonMnemonic('S');
        f.setApproveButtonText("Selecionar");
        f.setApproveButtonToolTipText("Selecionar");
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = f.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (f.getSelectedFile() != null) {
                this.getHomeView().getTxtDirDestinyTestU().setSelectedItem(f.getSelectedFile().toString());
            }
        }
    }
    
    /**
     * 
     */
    public void loadFilesFromOriginFolder() {
        if(this.getHomeView().getTxtDirOrigin().getSelectedItem()!=null){
            List<String> filesName = Utils.getListJSONFromDir(this.getHomeView().getTxtDirOrigin().getSelectedItem().toString().trim());
            DefaultListModel<String> newList1Model = new DefaultListModel<String>();
            //copy list 1
            for (int i = 0; i < filesName.size(); i++) {
                newList1Model.addElement(filesName.get(i));
            }
            this.getHomeView().getListInstAvailable().setModel(newList1Model);
            this.getHomeView().getListInstSelected().setModel(new DefaultListModel<String>());
        }
    }
    
    /**
     * @param indexes
     * @param list1
     * @param list2
     */
    public void multipleSelection(int[] indexes, JList<String> list1, JList<String> list2) {
        DefaultListModel<String> newList2Model = new DefaultListModel<String>();
        DefaultListModel<String> newList1Model = new DefaultListModel<String>();
        ListModel<String> list2Model = list2.getModel();
        ListModel<String> list1Model = list1.getModel();
        //copy list 2
        for (int i = 0; i < list2Model.getSize(); i++) {
            newList2Model.addElement((String) list2Model.getElementAt(i));
        }
        //copy list 1 to list 2
        for (Integer index : indexes) {
            newList2Model.addElement((String) list1Model.getElementAt(index));
        }
        //remove from list 1        
        for (int i = 0; i < list1Model.getSize(); i++) {
            boolean find = false;
            for (int index : indexes) {
                if (i == index) {
                    find = true;
                    break;
                }
            }
            if (!find) {
                newList1Model.addElement((String) list1Model.getElementAt(i));
            }
        }
        list2.setModel(newList2Model);
        list1.setModel(newList1Model);
    }

    /**
     * @return
     */
    public HomeView getHomeView() {
        return homeView;
    }

    /**
     * @param homeView
     */
    public void setHomeView(HomeView homeView) {
        this.homeView = homeView;
    }

    /**
     * @return
     */
    public TestDatasManager getTestDatasManager() {
        return testDatasManager;
    }

    /**
     * @param testDatasManager
     */
    public void setTestDatasManager(TestDatasManager testDatasManager) {
        this.testDatasManager = testDatasManager;
    }

}
