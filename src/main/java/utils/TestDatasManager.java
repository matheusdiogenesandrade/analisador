/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import controllers.HomeController;

import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.ListModel;
import models.VerifyFacade;
import views.HomeView;

/**
 *
 * @author mathe
 */
public class TestDatasManager extends Thread {

	private HomeController homeController;
	private ProgressBarManager progressBarManager;
        private boolean isTestWithOutput;       

	public TestDatasManager(HomeController homeController, ProgressBarManager progressBarManager, boolean isTestWithOutput) {
		this.homeController = homeController;
		this.setProgressBarManager(progressBarManager);
                this.isTestWithOutput = isTestWithOutput;
	}

	public void run() {
		HomeController homeController = this.getHomeController();
		HomeView homeView = homeController.getHomeView();
                this.getProgressBarManager().start();
                if(!isTestWithOutput){
                    // get fields values
                    if (homeView.getTxtDirOrigin().getSelectedItem() != null
                                    && homeView.getTxtDirDestiny().getSelectedItem() != null
                                    && homeView.getTxtUrl().getSelectedItem() != null) {
                            String dirOrigin = homeView.getTxtDirOrigin().getSelectedItem().toString().trim();
                            String dirDestiny = homeView.getTxtDirDestiny().getSelectedItem().toString().trim();
                            String url = homeView.getTxtUrl().getSelectedItem().toString().trim();
                            ListModel<String> listInstSelectedModel = homeView.getListInstSelected().getModel();
                            Integer instancesToTestNumber = listInstSelectedModel.getSize();
                            // if not empty
                            if (!dirOrigin.isEmpty() && !dirDestiny.isEmpty() && !url.isEmpty() && instancesToTestNumber > 0) {
                                    String[] originsDir = new String[instancesToTestNumber];
                                    for (int i = 0; i < instancesToTestNumber; i++) {
                                            originsDir[i] = dirOrigin + File.separator + (String) listInstSelectedModel.getElementAt(i);
                                    }
                                    try {
                                            VerifyFacade.testInstancesByDirs(originsDir, url, dirDestiny,
                                                            this.getProgressBarManager().getProgressBar());
                                            // save fields history
                                            this.homeController.saveTxtURLHistoryValues();
                                            this.homeController.saveOriginDirHistoryValues();
                                            this.homeController.saveDestinyDirHistoryValues();
                                            this.homeController.setTxtURLValues();
                                            this.homeController.setOriginDirValues();
                                            this.homeController.setDestinyDirValues();
                                            // show missing files
                                            String[] confFilesWithoutInstances = VerifyFacade.searchConfFileWithoutInstances(originsDir);
                                            String[] instanceFilesWithoutConf = VerifyFacade.searchInstancesWithoutConfFile(originsDir);
                                            String missingFilesMessage = "";
                                            if (confFilesWithoutInstances.length > 0 || instanceFilesWithoutConf.length > 0) {
                                                    missingFilesMessage = "\nPorém, ";
                                                    // config files without instances
                                                    if (confFilesWithoutInstances.length > 0) {
                                                            missingFilesMessage += "\nOs arquivos de configuração abaixo estão sem instâncias: ";
                                                            for (String confFile : confFilesWithoutInstances) {
                                                                    missingFilesMessage += "\n" + confFile;
                                                            }
                                                    }
                                                    // instances without config files
                                                    if (instanceFilesWithoutConf.length > 0) {
                                                            missingFilesMessage += "\nAs instâncias abaixo estão sem arquivos de configuração: ";
                                                            for (String confFile : instanceFilesWithoutConf) {
                                                                    missingFilesMessage += "\n" + confFile;
                                                            }
                                                    }
                                            }
                                            JOptionPane.showMessageDialog(null, "Teste finalizado com sucesso. " + missingFilesMessage);
                                    } catch (Exception ex) {
                                            StackTraceElement st[] = ex.getStackTrace();
                                            String erro = "";
                                            for(int i = 0; i < st.length; i++){
                                                    erro += st[i].toString() + '\n';
                                            }
                                            JOptionPane.showMessageDialog(null, erro + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                                    }
                            } else {
                                    JOptionPane.showMessageDialog(null,
                                                    "Erro: Os campos URL, diretório de origem e destino não podem ser vazios e deve haver pelo menos uma instância selecionada.");
                            }
                    } else {
                            JOptionPane.showMessageDialog(null,
                                            "Erro: Os campos URL, diretório de origem e destino não podem ser vazios e deve haver pelo menos uma instância selecionada.");
                    }
                }else{
                    if (homeView.getTxtInstance().getSelectedItem() != null && homeView.getTxtInstanceOutput().getSelectedItem() != null && homeView.getTxtDirDestinyTestU().getSelectedItem() != null) {
                        String instance = homeView.getTxtInstance().getSelectedItem().toString().trim();
                        String instanceOutput = homeView.getTxtInstanceOutput().getSelectedItem().toString().trim();
                        String dirDestinyTestU = homeView.getTxtDirDestinyTestU().getSelectedItem().toString().trim();
                        try {
                            VerifyFacade.testInstance(instance, instanceOutput, dirDestinyTestU, this.getProgressBarManager().getProgressBar());
                            JOptionPane.showMessageDialog(null, "Teste finalizado com sucesso.");
                        } catch (Exception ex) {
                                StackTraceElement st[] = ex.getStackTrace();
                                String erro = "";
                                for(int i = 0; i < st.length; i++){
                                        erro += st[i].toString() + '\n';
                                }
                                JOptionPane.showMessageDialog(null, erro + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                        }

                    } else {
                            JOptionPane.showMessageDialog(null, "Erro: Os campos instância, saída e destino não podem ser vazios.");
                    }
                }
		homeController.enableComponents();
		this.finish();
	}

	@Override
	public synchronized void start() {
		this.homeController.disableComponents();
		super.start();
	}

	@SuppressWarnings("deprecation")
	public void finish() {
		this.getProgressBarManager().interrupt();
		this.stop();
	}

	/**
	 * @return the homeController
	 */
	public HomeController getHomeController() {
		return homeController;
	}

	/**
	 * @param homeController
	 *            the homeController to set
	 */
	public void setHomeController(HomeController homeController) {
		this.homeController = homeController;
	}

	/**
	 * @return the progressBarManager
	 */
	private ProgressBarManager getProgressBarManager() {
		return progressBarManager;
	}

	/**
	 * @param progressBarManager
	 *            the progressBarManager to set
	 */
	private void setProgressBarManager(ProgressBarManager progressBarManager) {
		this.progressBarManager = progressBarManager;
	}

}
