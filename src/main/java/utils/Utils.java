/**
 *
 */
package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author matheus
 *
 */
public class Utils {

    private Utils() {
        //
    }

    public static List<String> getListFilesFromDir(String dir) {
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        List<String> filesName = new ArrayList<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                filesName.add(listOfFiles[i].getName());
            }
        }
        return filesName;
    }

    public static List<String> getListJSONFromDir(String dir) {
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        List<String> filesName = new ArrayList<String>();
        if (listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    String fileName = listOfFiles[i].getName();
                    int index = fileName.lastIndexOf(".");
                    if (index > 0 && fileName.substring(index + 1).toLowerCase().equals("json")) {
                        filesName.add(fileName);
                    }
                }
            }
        }
        return filesName;
    }

    public static String readFileByDir(String originDir) throws Exception {
        BufferedReader br = null;
        String content;
        try {
            br = new BufferedReader(new FileReader(originDir));
            content = "";
            for (String currentLine; (currentLine = br.readLine()) != null; content += currentLine)
				;
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                throw new Exception(ex.getMessage());
            }
        }
        return content;
    }

    public static void saveContentInDir(String destinyDir, String object) throws Exception {
        byte data[] = object.getBytes();
        FileOutputStream out = new FileOutputStream(destinyDir);
        out.write(data);
        out.close();
        
    }
}
