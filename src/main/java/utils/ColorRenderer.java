package utils;

import java.awt.Component;
import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ColorRenderer extends DefaultTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map<Point, Color> _colors = new HashMap<Point, Color>();
	private Point _tempCell = new Point(0, 0);
	private Color _defaultBG;
	public ColorRenderer() {
		this._defaultBG = getBackground();
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		this._tempCell.x = row;
		this._tempCell.y = column;
		if (this._colors.containsKey(this._tempCell)) {
			c.setBackground((Color) this._colors.get(this._tempCell));
		} else {
			c.setBackground(this._defaultBG);
		}
		return c;
	}

	public void toColorCell(int row, int col, Color cor) {
		this._colors.put(new Point(row, col), cor);
	}
}
