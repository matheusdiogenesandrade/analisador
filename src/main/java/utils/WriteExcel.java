/**
 * 
 */
package utils;

import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author matheus
 *
 */
public class WriteExcel {	
	
	private XSSFWorkbook xssfWorkbook;
	private XSSFSheet xssfSheet;			
	
	/**
	 * 
	 */
	public WriteExcel(String sheetName) {
		super();		
		this.setXssfWorkbook(new XSSFWorkbook());
		this.setXssfSheet(this.getXssfWorkbook().createSheet(sheetName));
	}

	public void write(String fileName) throws IOException{
		System.out.println("Write");
        this.getXssfWorkbook().write(new FileOutputStream(fileName));        
	}
	
	public CellStyle createCellStyle(short color, FillPatternType fillPatternType){
		CellStyle style = this.getXssfWorkbook().createCellStyle();
    	style.setFillBackgroundColor(color);
    	style.setFillPattern(fillPatternType);
    	return style;
	}
	
	/**
	 * @return the xssfWorkbook
	 */
	public XSSFWorkbook getXssfWorkbook() {
		return xssfWorkbook;
	}

	/**
	 * @param xssfWorkbook the xssfWorkbook to set
	 */
	public void setXssfWorkbook(XSSFWorkbook xssfWorkbook) {
		this.xssfWorkbook = xssfWorkbook;
	}

	/**
	 * @return the xssfSheet
	 */
	public XSSFSheet getXssfSheet() {
		return xssfSheet;
	}

	/**
	 * @param xssfSheet the xssfSheet to set
	 */
	public void setXssfSheet(XSSFSheet xssfSheet) {
		this.xssfSheet = xssfSheet;
	}
	
}
