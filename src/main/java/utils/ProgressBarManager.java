/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import javax.swing.JProgressBar;

/**
 *
 * @author mathe
 */
public class ProgressBarManager extends Thread{
    private JProgressBar progressBar;

    public ProgressBarManager(JProgressBar progressBar) {
        this.progressBar = progressBar;
    }
    
    @Override
    public void run(){
        int value = this.progressBar.getValue();
        while(!this.isInterrupted()){
            if(this.progressBar.getValue()!= value){
                this.progressBar.paint(this.progressBar.getGraphics());
                value = this.progressBar.getValue();
            }
        }
        this.progressBar.setValue(0);
    }

    /**
     * @return the progressBar
     */
    public JProgressBar getProgressBar() {
        return progressBar;
    }

    /**
     * @param progressBar the progressBar to set
     */
    public void setProgressBar(JProgressBar progressBar) {
        this.progressBar = progressBar;
    }
}
