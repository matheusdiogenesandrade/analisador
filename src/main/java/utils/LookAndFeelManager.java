/**
 * 
 */
package utils;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * @author matheus
 *
 */
public class LookAndFeelManager {
	private LookAndFeelManager(){
		//
	}
	
	public static void changeThemeToNimbus(){
		boolean find = false;
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
	        if ("Nimbus".equals(info.getName())) {
	            try {
					UIManager.setLookAndFeel(info.getClassName());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (UnsupportedLookAndFeelException e) {
					e.printStackTrace();
				}
	            find = true;
	            break;
	        }	        
	    }
		if (!find){
			throw new IllegalAccessError("Erro: Nimbus not found.");
		}
	}
}
